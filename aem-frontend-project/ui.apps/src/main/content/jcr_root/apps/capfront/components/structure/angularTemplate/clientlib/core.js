var userManagement = angular.module('userManagement', []);

function mainController($scope, $http) {
    $scope.formData = {};

    // when landing on the page, get all todos and show them
    $http.get('http://localhost:3000/users').then(function (success) {

        $scope.todos = success.data.message;
        console.log(success.data);
        
    }, function (error) {
      console.log('error!');  
    });


}

userManagement.controller("mainController", mainController);