angular.module('Stores')
.service('storeService', ['$http', function($http) {



        this.getStores = function () {
            return $http.get('/bin/fridays-commerce/locations')

        }

        this.getStore = function (locationId) {
            return $http.get('/bin/fridays-commerce/locations?' + 'storeId=' + locationId)
        }

        this.setStoreId = function (data) {
            return localStorage.setItem("storeId", data); // needs to be dynamically set by User Selection.
           }

        this.getStoreName = function (store) {
            return store.locations["0"].address_town;
        }


        this.getStoreId = function () {
            return localStorage.getItem("storeId"); // needs to be dynamically set by User Selection.
        }
}]);



