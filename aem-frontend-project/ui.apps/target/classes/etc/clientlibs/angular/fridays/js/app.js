var app = angular.module('Stores', [
    'ui.router',
    'app.controllers',
    'app.controllers.store'
])

app.config(function($stateProvider) {

        $stateProvider

            .state('home', {
                url: '/',
                resolve: {
                    store: function (storeService) {
                        return storeService.getStore(storeService.getStoreId());
                    }
                },
                views: {
                    'header': {
                        templateUrl: '/apps/fridays-demo/components/content/header/location.html',
                        controller: 'HeaderCtrl',
                        controllerAs: 'header'
                    },
                    'main': {
                        templateUrl: '/content/fridays/en/index/home.content.html'

                    }

                }
            })
            .state('home.locations', {
                url: '/locations',
                resolve: {
                    stores: ['storeService',
                        function (storeService) {
                            return storeService.getStores();
                        }],
                    store: function (storeService) {
                        return storeService.getStore(storeService.getStoreId());
                    }

                },
                views: {
                    'main@': {
                        templateUrl: '/content/fridays/en/index/home/locations.content.html',
                        controller: 'StoreCtrl',
                        controllerAs: 'locations'

                    }
                }

            })
            .state('home.menu', {
                url: '/menu',
                views: {
                    'main@': {
                        templateUrl: '/content/fridays/en/index/home/category.content.html'

                    }
                }
            })
            .state('storeMenu', {
                url: '/menu/:storeId',
                resolve: {
                    stores: function () {
                        return [];
                    },
                    store: ['$stateParams', 'storeService',
                        function ($stateParams, storeService) {
                            return storeService.getStore($stateParams.id);
                        }]
                },
                templateUrl: '/content/fridays/en/index/home/category.content.html',
                controller: 'StoreCtrl'

            })
    })


