/*! jQuery v1.12.0 | (c) jQuery Foundation | jquery.org/license */
;
!function(d,c){"object"==typeof module&&"object"==typeof module.exports?module.exports=d.document?c(d,!0):function(b){if(!b.document){throw new Error("jQuery requires a window with a document")
}return c(b)
}:c(d)
}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=a.document,e=c.slice,f=c.concat,g=c.push,h=c.indexOf,i={},j=i.toString,k=i.hasOwnProperty,l={},m="1.12.0",n=function(a,b){return new n.fn.init(a,b)
},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()
};
    n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return e.call(this)
    },get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:e.call(this)
    },pushStack:function(a){var b=n.merge(this.constructor(),a);
        return b.prevObject=this,b.context=this.context,b
    },each:function(a){return n.each(this,a)
    },map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)
    }))
    },slice:function(){return this.pushStack(e.apply(this,arguments))
    },first:function(){return this.eq(0)
    },last:function(){return this.eq(-1)
    },eq:function(a){var b=this.length,c=+a+(0>a?b:0);
        return this.pushStack(c>=0&&b>c?[this[c]]:[])
    },end:function(){return this.prevObject||this.constructor()
    },push:g,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;
        for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);
            i>h;
            h++){if(null!=(e=arguments[h])){for(d in e){a=g[d],c=e[d],g!==c&&(j&&c&&(n.isPlainObject(c)||(b=n.isArray(c)))?(b?(b=!1,f=a&&n.isArray(a)?a:[]):f=a&&n.isPlainObject(a)?a:{},g[d]=n.extend(j,f,c)):void 0!==c&&(g[d]=c))
        }}}return g
    },n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)
    },noop:function(){},isFunction:function(a){return"function"===n.type(a)
    },isArray:Array.isArray||function(a){return"array"===n.type(a)
    },isWindow:function(a){return null!=a&&a==a.window
    },isNumeric:function(a){var b=a&&a.toString();
        return !n.isArray(a)&&b-parseFloat(b)+1>=0
    },isEmptyObject:function(a){var b;
        for(b in a){return !1
        }return !0
    },isPlainObject:function(a){var b;
        if(!a||"object"!==n.type(a)||a.nodeType||n.isWindow(a)){return !1
        }try{if(a.constructor&&!k.call(a,"constructor")&&!k.call(a.constructor.prototype,"isPrototypeOf")){return !1
        }}catch(c){return !1
        }if(!l.ownFirst){for(b in a){return k.call(a,b)
        }}for(b in a){}return void 0===b||k.call(a,b)
    },type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?i[j.call(a)]||"object":typeof a
    },globalEval:function(b){b&&n.trim(b)&&(a.execScript||function(b){a.eval.call(a,b)
    })(b)
    },camelCase:function(a){return a.replace(p,"ms-").replace(q,r)
    },nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()
    },each:function(a,b){var c,d=0;
        if(s(a)){for(c=a.length;
                     c>d;
                     d++){if(b.call(a[d],d,a[d])===!1){break
        }}}else{for(d in a){if(b.call(a[d],d,a[d])===!1){break
        }}}return a
    },trim:function(a){return null==a?"":(a+"").replace(o,"")
    },makeArray:function(a,b){var c=b||[];
        return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):g.call(c,a)),c
    },inArray:function(a,b,c){var d;
        if(b){if(h){return h.call(b,a,c)
        }for(d=b.length,c=c?0>c?Math.max(0,d+c):c:0;
             d>c;
             c++){if(c in b&&b[c]===a){return c
        }}}return -1
    },merge:function(a,b){var c=+b.length,d=0,e=a.length;
        while(c>d){a[e++]=b[d++]
        }if(c!==c){while(void 0!==b[d]){a[e++]=b[d++]
        }}return a.length=e,a
    },grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;
                               g>f;
                               f++){d=!b(a[f],f),d!==h&&e.push(a[f])
    }return e
    },map:function(a,b,c){var d,e,g=0,h=[];
        if(s(a)){for(d=a.length;
                     d>g;
                     g++){e=b(a[g],g,c),null!=e&&h.push(e)
        }}else{for(g in a){e=b(a[g],g,c),null!=e&&h.push(e)
        }}return f.apply([],h)
    },guid:1,proxy:function(a,b){var c,d,f;
        return"string"==typeof b&&(f=a[b],b=a,a=f),n.isFunction(a)?(c=e.call(arguments,2),d=function(){return a.apply(b||this,c.concat(e.call(arguments)))
        },d.guid=a.guid=a.guid||n.guid++,d):void 0
    },now:function(){return +new Date
    },support:l}),"function"==typeof Symbol&&(n.fn[Symbol.iterator]=c[Symbol.iterator]),n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){i["[object "+b+"]"]=b.toLowerCase()
    });
    function s(a){var b=!!a&&"length" in a&&a.length,c=n.type(a);
        return"function"===c||n.isWindow(a)?!1:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a
    }var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ga(),z=ga(),A=ga(),B=function(a,b){return a===b&&(l=!0),0
    },C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;
                                                                                               d>c;
                                                                                               c++){if(a[c]===b){return c
    }}return -1
    },K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+M+"))|)"+L+"*\\]",O=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+N+")*)|.*)\\)|)",P=new RegExp(L+"+","g"),Q=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),R=new RegExp("^"+L+"*,"+L+"*"),S=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),T=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),U=new RegExp(O),V=new RegExp("^"+M+"$"),W={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M+"|[*])"),ATTR:new RegExp("^"+N),PSEUDO:new RegExp("^"+O),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},X=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Z=/^[^{]+\{\s*\[native \w/,$=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,_=/[+~]/,aa=/'|\\/g,ba=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),ca=function(a,b,c){var d="0x"+b-65536;
        return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)
    },da=function(){m()
    };
        try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType
        }catch(ea){H={apply:E.length?function(a,b){G.apply(a,I.call(b))
        }:function(a,b){var c=a.length,d=0;
            while(a[c++]=b[d++]){}a.length=c-1
        }}
        }function fa(a,b,d,e){var f,h,j,k,l,o,r,s,w=b&&b.ownerDocument,x=b?b.nodeType:9;
            if(d=d||[],"string"!=typeof a||!a||1!==x&&9!==x&&11!==x){return d
            }if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==x&&(o=$.exec(a))){if(f=o[1]){if(9===x){if(!(j=b.getElementById(f))){return d
            }if(j.id===f){return d.push(j),d
            }}else{if(w&&(j=w.getElementById(f))&&t(b,j)&&j.id===f){return d.push(j),d
            }}}else{if(o[2]){return H.apply(d,b.getElementsByTagName(a)),d
            }if((f=o[3])&&c.getElementsByClassName&&b.getElementsByClassName){return H.apply(d,b.getElementsByClassName(f)),d
            }}}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==x){w=b,s=a
            }else{if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(aa,"\\$&"):b.setAttribute("id",k=u),r=g(a),h=r.length,l=V.test(k)?"#"+k:"[id='"+k+"']";
                while(h--){r[h]=l+" "+qa(r[h])
                }s=r.join(","),w=_.test(a)&&oa(b.parentNode)||b
            }}if(s){try{return H.apply(d,w.querySelectorAll(s)),d
            }catch(y){}finally{k===u&&b.removeAttribute("id")
            }}}}return i(a.replace(Q,"$1"),b,d,e)
        }function ga(){var a=[];
            function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e
            }return b
        }function ha(a){return a[u]=!0,a
        }function ia(a){var b=n.createElement("div");
            try{return !!a(b)
            }catch(c){return !1
            }finally{b.parentNode&&b.parentNode.removeChild(b),b=null
            }}function ja(a,b){var c=a.split("|"),e=c.length;
            while(e--){d.attrHandle[c[e]]=b
            }}function ka(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);
            if(d){return d
            }if(c){while(c=c.nextSibling){if(c===b){return -1
            }}}return a?1:-1
        }function la(a){return function(b){var c=b.nodeName.toLowerCase();
            return"input"===c&&b.type===a
        }
        }function ma(a){return function(b){var c=b.nodeName.toLowerCase();
            return("input"===c||"button"===c)&&b.type===a
        }
        }function na(a){return ha(function(b){return b=+b,ha(function(c,d){var e,f=a([],c.length,b),g=f.length;
            while(g--){c[e=f[g]]&&(c[e]=!(d[e]=c[e]))
            }})
        })
        }function oa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a
        }c=fa.support={},f=fa.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;
            return b?"HTML"!==b.nodeName:!1
        },m=fa.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;
            return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ia(function(a){return a.className="i",!a.getAttribute("className")
            }),c.getElementsByTagName=ia(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length
            }),c.getElementsByClassName=Z.test(n.getElementsByClassName),c.getById=ia(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length
            }),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);
                return c?[c]:[]
            }},d.filter.ID=function(a){var b=a.replace(ba,ca);
                return function(a){return a.getAttribute("id")===b
                }
            }):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ba,ca);
                return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");
                    return c&&c.value===b
                }
            }),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0
            }:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);
                if("*"===a){while(c=f[e++]){1===c.nodeType&&d.push(c)
                }return d
                }return f
            },d.find.CLASS=c.getElementsByClassName&&function(a,b){return"undefined"!=typeof b.getElementsByClassName&&p?b.getElementsByClassName(a):void 0
            },r=[],q=[],(c.qsa=Z.test(n.querySelectorAll))&&(ia(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")
            }),ia(function(a){var b=n.createElement("input");
                b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")
            })),(c.matchesSelector=Z.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ia(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",O)
            }),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Z.test(o.compareDocumentPosition),t=b||Z.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;
                return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))
            }:function(a,b){if(b){while(b=b.parentNode){if(b===a){return !0
            }}}return !1
            },B=b?function(a,b){if(a===b){return l=!0,0
            }var d=!a.compareDocumentPosition-!b.compareDocumentPosition;
                return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)
            }:function(a,b){if(a===b){return l=!0,0
            }var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];
                if(!e||!f){return a===n?-1:b===n?1:e?-1:f?1:k?J(k,a)-J(k,b):0
                }if(e===f){return ka(a,b)
                }c=a;
                while(c=c.parentNode){g.unshift(c)
                }c=b;
                while(c=c.parentNode){h.unshift(c)
                }while(g[d]===h[d]){d++
                }return d?ka(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0
            },n):n
        },fa.matches=function(a,b){return fa(a,null,null,b)
        },fa.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(T,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b))){try{var d=s.call(a,b);
            if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType){return d
            }}catch(e){}}return fa(b,n,null,[a]).length>0
        },fa.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)
        },fa.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);
            var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;
            return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null
        },fa.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)
        },fa.uniqueSort=function(a){var b,d=[],e=0,f=0;
            if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++]){b===a[f]&&(e=d.push(f))
            }while(e--){a.splice(d[e],1)
            }}return k=null,a
        },e=fa.getText=function(a){var b,c="",d=0,f=a.nodeType;
            if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent){return a.textContent
            }for(a=a.firstChild;
                 a;
                 a=a.nextSibling){c+=e(a)
            }}else{if(3===f||4===f){return a.nodeValue
            }}}else{while(b=a[d++]){c+=e(b)
            }}return c
        },d=fa.selectors={cacheLength:50,createPseudo:ha,match:W,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ba,ca),a[3]=(a[3]||a[4]||a[5]||"").replace(ba,ca),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)
        },CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fa.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fa.error(a[0]),a
        },PSEUDO:function(a){var b,c=!a[6]&&a[2];
            return W.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&U.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))
        }},filter:{TAG:function(a){var b=a.replace(ba,ca).toLowerCase();
            return"*"===a?function(){return !0
            }:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b
            }
        },CLASS:function(a){var b=y[a+" "];
            return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")
                })
        },ATTR:function(a,b,c){return function(d){var e=fa.attr(d,a);
            return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(P," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0
        }
        },CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;
            return 1===d&&0===e?function(a){return !!a.parentNode
            }:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;
                if(q){if(f){while(p){m=b;
                    while(m=m[p]){if(h?m.nodeName.toLowerCase()===r:1===m.nodeType){return !1
                    }}o=p="only"===a&&!o&&"nextSibling"
                }return !0
                }if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];
                    while(m=++n&&m&&m[p]||(t=n=0)||o.pop()){if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];
                        break
                    }}}else{if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1){while(m=++n&&m&&m[p]||(t=n=0)||o.pop()){if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b)){break
                }}}}return t-=e,t===d||t%d===0&&t/d>=0
                }}
        },PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fa.error("unsupported pseudo: "+a);
            return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ha(function(a,c){var d,f=e(a,b),g=f.length;
                while(g--){d=J(a,f[g]),a[d]=!(c[d]=f[g])
                }}):function(a){return e(a,0,c)
            }):e
        }},pseudos:{not:ha(function(a){var b=[],c=[],d=h(a.replace(Q,"$1"));
            return d[u]?ha(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;
                while(h--){(f=g[h])&&(a[h]=!(b[h]=f))
                }}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()
            }
        }),has:ha(function(a){return function(b){return fa(a,b).length>0
        }
        }),contains:ha(function(a){return a=a.replace(ba,ca),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1
        }
        }),lang:ha(function(a){return V.test(a||"")||fa.error("unsupported lang: "+a),a=a.replace(ba,ca).toLowerCase(),function(b){var c;
            do{if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang")){return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-")
            }}while((b=b.parentNode)&&1===b.nodeType);
            return !1
        }
        }),target:function(b){var c=a.location&&a.location.hash;
            return c&&c.slice(1)===b.id
        },root:function(a){return a===o
        },focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)
        },enabled:function(a){return a.disabled===!1
        },disabled:function(a){return a.disabled===!0
        },checked:function(a){var b=a.nodeName.toLowerCase();
            return"input"===b&&!!a.checked||"option"===b&&!!a.selected
        },selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0
        },empty:function(a){for(a=a.firstChild;
                                a;
                                a=a.nextSibling){if(a.nodeType<6){return !1
        }}return !0
        },parent:function(a){return !d.pseudos.empty(a)
        },header:function(a){return Y.test(a.nodeName)
        },input:function(a){return X.test(a.nodeName)
        },button:function(a){var b=a.nodeName.toLowerCase();
            return"input"===b&&"button"===a.type||"button"===b
        },text:function(a){var b;
            return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())
        },first:na(function(){return[0]
        }),last:na(function(a,b){return[b-1]
        }),eq:na(function(a,b,c){return[0>c?c+b:c]
        }),even:na(function(a,b){for(var c=0;
                                     b>c;
                                     c+=2){a.push(c)
        }return a
        }),odd:na(function(a,b){for(var c=1;
                                    b>c;
                                    c+=2){a.push(c)
        }return a
        }),lt:na(function(a,b,c){for(var d=0>c?c+b:c;
                                     --d>=0;
        ){a.push(d)
        }return a
        }),gt:na(function(a,b,c){for(var d=0>c?c+b:c;
                                     ++d<b;
        ){a.push(d)
        }return a
        })}},d.pseudos.nth=d.pseudos.eq;
        for(b in {radio:!0,checkbox:!0,file:!0,password:!0,image:!0}){d.pseudos[b]=la(b)
        }for(b in {submit:!0,reset:!0}){d.pseudos[b]=ma(b)
        }function pa(){}pa.prototype=d.filters=d.pseudos,d.setFilters=new pa,g=fa.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];
            if(k){return b?0:k.slice(0)
            }h=a,i=[],j=d.preFilter;
            while(h){(!c||(e=R.exec(h)))&&(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=S.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(Q," ")}),h=h.slice(c.length));
                for(g in d.filter){!(e=W[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length))
                }if(!c){break
                }}return b?h.length:h?fa.error(a):z(a,i).slice(0)
        };
        function qa(a){for(var b=0,c=a.length,d="";
                           c>b;
                           b++){d+=a[b].value
        }return d
        }function ra(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;
            return b.first?function(b,c,f){while(b=b[d]){if(1===b.nodeType||e){return a(b,c,f)
            }}}:function(b,c,g){var h,i,j,k=[w,f];
                if(g){while(b=b[d]){if((1===b.nodeType||e)&&a(b,c,g)){return !0
                }}}else{while(b=b[d]){if(1===b.nodeType||e){if(j=b[u]||(b[u]={}),i=j[b.uniqueID]||(j[b.uniqueID]={}),(h=i[d])&&h[0]===w&&h[1]===f){return k[2]=h[2]
                }if(i[d]=k,k[2]=a(b,c,g)){return !0
                }}}}}
        }function sa(a){return a.length>1?function(b,c,d){var e=a.length;
            while(e--){if(!a[e](b,c,d)){return !1
            }}return !0
        }:a[0]
        }function ta(a,b,c){for(var d=0,e=b.length;
                                e>d;
                                d++){fa(a,b[d],c)
        }return c
        }function ua(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;
                                    i>h;
                                    h++){(f=a[h])&&(!c||c(f,d,e))&&(g.push(f),j&&b.push(h))
        }return g
        }function va(a,b,c,d,e,f){return d&&!d[u]&&(d=va(d)),e&&!e[u]&&(e=va(e,f)),ha(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ta(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ua(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;
            if(c&&c(q,r,h,i),d){j=ua(r,n),d(j,[],h,i),k=j.length;
                while(k--){(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))
                }}if(f){if(e||a){if(e){j=[],k=r.length;
                while(k--){(l=r[k])&&j.push(q[k]=l)
                }e(null,r=[],j,i)
            }k=r.length;
                while(k--){(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))
                }}}else{r=ua(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)
            }})
        }function wa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ra(function(a){return a===b
        },h,!0),l=ra(function(a){return J(b,a)>-1
        },h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));
            return b=null,e
        }];
                            f>i;
                            i++){if(c=d.relative[a[i].type]){m=[ra(sa(m),c)]
        }else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;
                                                                          f>e;
                                                                          e++){if(d.relative[a[e].type]){break
        }}return va(i>1&&sa(m),i>1&&qa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(Q,"$1"),c,e>i&&wa(a.slice(i,e)),f>e&&wa(a=a.slice(e)),f>e&&qa(a))
        }m.push(c)
        }}return sa(m)
        }function xa(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||0.1,z=x.length;
            for(k&&(j=g===n||g||k);
                s!==z&&null!=(l=x[s]);
                s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);
                while(q=a[o++]){if(q(l,g||n,h)){i.push(l);
                    break
                }}k&&(w=y)
            }c&&((l=!q&&l)&&r--,f&&t.push(l))
            }if(r+=s,c&&s!==r){o=0;
                while(q=b[o++]){q(t,u,g,h)
                }if(f){if(r>0){while(s--){t[s]||u[s]||(u[s]=F.call(i))
                }}u=ua(u)
                }H.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&fa.uniqueSort(i)
            }return k&&(w=y,j=v),t
        };
            return c?ha(f):f
        }return h=fa.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];
            if(!f){b||(b=g(a)),c=b.length;
                while(c--){f=wa(b[c]),f[u]?d.push(f):e.push(f)
                }f=A(a,xa(e,d)),f.selector=a
            }return f
        },i=fa.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);
            if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ba,ca),b)||[])[0],!b){return e
            }n&&(b=b.parentNode),a=a.slice(j.shift().value.length)
            }i=W.needsContext.test(a)?0:j.length;
                while(i--){if(k=j[i],d.relative[l=k.type]){break
                }if((m=d.find[l])&&(f=m(k.matches[0].replace(ba,ca),_.test(j[0].type)&&oa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qa(j),!a){return H.apply(e,f),e
                }break
                }}}return(n||h(a,o))(f,b,!p,e,!b||_.test(a)&&oa(b.parentNode)||b),e
        },c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ia(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))
        }),ia(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")
        })||ja("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)
        }),c.attributes&&ia(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")
        })||ja("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue
        }),ia(function(a){return null==a.getAttribute("disabled")
        })||ja(K,function(a,b,c){var d;
            return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null
        }),fa
    }(a);
    n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.uniqueSort=n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;
    var u=function(a,b,c){var d=[],e=void 0!==c;
        while((a=a[b])&&9!==a.nodeType){if(1===a.nodeType){if(e&&n(a).is(c)){break
        }d.push(a)
        }}return d
    },v=function(a,b){for(var c=[];
                          a;
                          a=a.nextSibling){1===a.nodeType&&a!==b&&c.push(a)
    }return c
    },w=n.expr.match.needsContext,x=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,y=/^.[^:#\[\.,]*$/;
    function z(a,b,c){if(n.isFunction(b)){return n.grep(a,function(a,d){return !!b.call(a,d,a)!==c
    })
    }if(b.nodeType){return n.grep(a,function(a){return a===b!==c
    })
    }if("string"==typeof b){if(y.test(b)){return n.filter(b,a,c)
    }b=n.filter(b,a)
    }return n.grep(a,function(a){return n.inArray(a,b)>-1!==c
    })
    }n.filter=function(a,b,c){var d=b[0];
        return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType
        }))
    },n.fn.extend({find:function(a){var b,c=[],d=this,e=d.length;
        if("string"!=typeof a){return this.pushStack(n(a).filter(function(){for(b=0;
                                                                                e>b;
                                                                                b++){if(n.contains(d[b],this)){return !0
        }}}))
        }for(b=0;
             e>b;
             b++){n.find(a,d[b],c)
        }return c=this.pushStack(e>1?n.unique(c):c),c.selector=this.selector?this.selector+" "+a:a,c
    },filter:function(a){return this.pushStack(z(this,a||[],!1))
    },not:function(a){return this.pushStack(z(this,a||[],!0))
    },is:function(a){return !!z(this,"string"==typeof a&&w.test(a)?n(a):a||[],!1).length
    }});
    var A,B=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,C=n.fn.init=function(a,b,c){var e,f;
        if(!a){return this
        }if(c=c||A,"string"==typeof a){if(e="<"===a.charAt(0)&&">"===a.charAt(a.length-1)&&a.length>=3?[null,a,null]:B.exec(a),!e||!e[1]&&b){return !b||b.jquery?(b||c).find(a):this.constructor(b).find(a)
        }if(e[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),x.test(e[1])&&n.isPlainObject(b)){for(e in b){n.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e])
        }}return this
        }if(f=d.getElementById(e[2]),f&&f.parentNode){if(f.id!==e[2]){return A.find(a)
        }this.length=1,this[0]=f
        }return this.context=d,this.selector=a,this
        }return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?"undefined"!=typeof c.ready?c.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))
    };
    C.prototype=n.fn,A=n(d);
    var D=/^(?:parents|prev(?:Until|All))/,E={children:!0,contents:!0,next:!0,prev:!0};
    n.fn.extend({has:function(a){var b,c=n(a,this),d=c.length;
        return this.filter(function(){for(b=0;
                                          d>b;
                                          b++){if(n.contains(this,c[b])){return !0
        }}})
    },closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=w.test(a)||"string"!=typeof a?n(a,b||this.context):0;
                                e>d;
                                d++){for(c=this[d];
                                         c&&c!==b;
                                         c=c.parentNode){if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);
        break
    }}}return this.pushStack(f.length>1?n.uniqueSort(f):f)
    },index:function(a){return a?"string"==typeof a?n.inArray(this[0],n(a)):n.inArray(a.jquery?a[0]:a,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1
    },add:function(a,b){return this.pushStack(n.uniqueSort(n.merge(this.get(),n(a,b))))
    },addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))
    }});
    function F(a,b){do{a=a[b]
    }while(a&&1!==a.nodeType);
        return a
    }n.each({parent:function(a){var b=a.parentNode;
        return b&&11!==b.nodeType?b:null
    },parents:function(a){return u(a,"parentNode")
    },parentsUntil:function(a,b,c){return u(a,"parentNode",c)
    },next:function(a){return F(a,"nextSibling")
    },prev:function(a){return F(a,"previousSibling")
    },nextAll:function(a){return u(a,"nextSibling")
    },prevAll:function(a){return u(a,"previousSibling")
    },nextUntil:function(a,b,c){return u(a,"nextSibling",c)
    },prevUntil:function(a,b,c){return u(a,"previousSibling",c)
    },siblings:function(a){return v((a.parentNode||{}).firstChild,a)
    },children:function(a){return v(a.firstChild)
    },contents:function(a){return n.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:n.merge([],a.childNodes)
    }},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);
        return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(E[a]||(e=n.uniqueSort(e)),D.test(a)&&(e=e.reverse())),this.pushStack(e)
    }
    });
    var G=/\S+/g;
    function H(a){var b={};
        return n.each(a.match(G)||[],function(a,c){b[c]=!0
        }),b
    }n.Callbacks=function(a){a="string"==typeof a?H(a):n.extend({},a);
        var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=a.once,d=b=!0;
                                                    g.length;
                                                    h=-1){c=g.shift();
            while(++h<f.length){f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)
            }}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")
        },j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){n.each(b,function(b,c){n.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==n.type(c)&&d(c)
        })
        }(arguments),c&&!b&&i()),this
        },remove:function(){return n.each(arguments,function(a,b){var c;
            while((c=n.inArray(b,f,c))>-1){f.splice(c,1),h>=c&&h--
            }}),this
        },has:function(a){return a?n.inArray(a,f)>-1:f.length>0
        },empty:function(){return f&&(f=[]),this
        },disable:function(){return e=g=[],f=c="",this
        },disabled:function(){return !f
        },lock:function(){return e=!0,c||j.disable(),this
        },locked:function(){return !!e
        },fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this
        },fire:function(){return j.fireWith(this,arguments),this
        },fired:function(){return !!d
        }};
        return j
    },n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c
    },always:function(){return e.done(arguments).fail(arguments),this
    },then:function(){var a=arguments;
        return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];
            e[f[1]](function(){var a=g&&g.apply(this,arguments);
                a&&n.isFunction(a.promise)?a.promise().progress(c.notify).done(c.resolve).fail(c.reject):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)
            })
        }),a=null
        }).promise()
    },promise:function(a){return null!=a?n.extend(a,d):d
    }},e={};
        return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];
            d[f[1]]=g.add,h&&g.add(function(){c=h
            },b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this
            },e[f[0]+"With"]=g.fireWith
        }),d.promise(e),a&&a.call(e,e),e
    },when:function(a){var b=0,c=e.call(arguments),d=c.length,f=1!==d||a&&n.isFunction(a.promise)?d:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(d){b[a]=this,c[a]=arguments.length>1?e.call(arguments):d,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)
    }
    },i,j,k;
        if(d>1){for(i=new Array(d),j=new Array(d),k=new Array(d);
                    d>b;
                    b++){c[b]&&n.isFunction(c[b].promise)?c[b].promise().progress(h(b,j,i)).done(h(b,k,c)).fail(g.reject):--f
        }}return f||g.resolveWith(k,c),g.promise()
    }});
    var I;
    n.fn.ready=function(a){return n.ready.promise().done(a),this
    },n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)
    },ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(I.resolveWith(d,[n]),n.fn.triggerHandler&&(n(d).triggerHandler("ready"),n(d).off("ready"))))
    }});
    function J(){d.addEventListener?(d.removeEventListener("DOMContentLoaded",K),a.removeEventListener("load",K)):(d.detachEvent("onreadystatechange",K),a.detachEvent("onload",K))
    }function K(){(d.addEventListener||"load"===a.event.type||"complete"===d.readyState)&&(J(),n.ready())
    }n.ready.promise=function(b){if(!I){if(I=n.Deferred(),"complete"===d.readyState){a.setTimeout(n.ready)
    }else{if(d.addEventListener){d.addEventListener("DOMContentLoaded",K),a.addEventListener("load",K)
    }else{d.attachEvent("onreadystatechange",K),a.attachEvent("onload",K);
        var c=!1;
        try{c=null==a.frameElement&&d.documentElement
        }catch(e){}c&&c.doScroll&&!function f(){if(!n.isReady){try{c.doScroll("left")
        }catch(b){return a.setTimeout(f,50)
        }J(),n.ready()
        }}()
    }}}return I.promise(b)
    },n.ready.promise();
    var L;
    for(L in n(l)){break
    }l.ownFirst="0"===L,l.inlineBlockNeedsLayout=!1,n(function(){var a,b,c,e;
        c=d.getElementsByTagName("body")[0],c&&c.style&&(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1",l.inlineBlockNeedsLayout=a=3===b.offsetWidth,a&&(c.style.zoom=1)),c.removeChild(e))
    }),function(){var a=d.createElement("div");
        l.deleteExpando=!0;
        try{delete a.test
        }catch(b){l.deleteExpando=!1
        }a=null
    }();
    var M=function(a){var b=n.noData[(a.nodeName+" ").toLowerCase()],c=+a.nodeType||1;
        return 1!==c&&9!==c?!1:!b||b!==!0&&a.getAttribute("classid")===b
    },N=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,O=/([A-Z])/g;
    function P(a,b,c){if(void 0===c&&1===a.nodeType){var d="data-"+b.replace(O,"-$1").toLowerCase();
        if(c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:N.test(c)?n.parseJSON(c):c
        }catch(e){}n.data(a,b,c)
        }else{c=void 0
        }}return c
    }function Q(a){var b;
        for(b in a){if(("data"!==b||!n.isEmptyObject(a[b]))&&"toJSON"!==b){return !1
        }}return !0
    }function R(a,b,d,e){if(M(a)){var f,g,h=n.expando,i=a.nodeType,j=i?n.cache:a,k=i?a[h]:a[h]&&h;
        if(k&&j[k]&&(e||j[k].data)||void 0!==d||"string"!=typeof b){return k||(k=i?a[h]=c.pop()||n.guid++:h),j[k]||(j[k]=i?{}:{toJSON:n.noop}),("object"==typeof b||"function"==typeof b)&&(e?j[k]=n.extend(j[k],b):j[k].data=n.extend(j[k].data,b)),g=j[k],e||(g.data||(g.data={}),g=g.data),void 0!==d&&(g[n.camelCase(b)]=d),"string"==typeof b?(f=g[b],null==f&&(f=g[n.camelCase(b)])):f=g,f
        }}}function S(a,b,c){if(M(a)){var d,e,f=a.nodeType,g=f?n.cache:a,h=f?a[n.expando]:n.expando;
        if(g[h]){if(b&&(d=c?g[h]:g[h].data)){n.isArray(b)?b=b.concat(n.map(b,n.camelCase)):b in d?b=[b]:(b=n.camelCase(b),b=b in d?[b]:b.split(" ")),e=b.length;
            while(e--){delete d[b[e]]
            }if(c?!Q(d):!n.isEmptyObject(d)){return
            }}(c||(delete g[h].data,Q(g[h])))&&(f?n.cleanData([a],!0):l.deleteExpando||g!=g.window?delete g[h]:g[h]=void 0)
        }}}n.extend({cache:{},noData:{"applet ":!0,"embed ":!0,"object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},hasData:function(a){return a=a.nodeType?n.cache[a[n.expando]]:a[n.expando],!!a&&!Q(a)
    },data:function(a,b,c){return R(a,b,c)
    },removeData:function(a,b){return S(a,b)
    },_data:function(a,b,c){return R(a,b,c,!0)
    },_removeData:function(a,b){return S(a,b,!0)
    }}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;
        if(void 0===a){if(this.length&&(e=n.data(f),1===f.nodeType&&!n._data(f,"parsedAttrs"))){c=g.length;
            while(c--){g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),P(f,d,e[d])))
            }n._data(f,"parsedAttrs",!0)
        }return e
        }return"object"==typeof a?this.each(function(){n.data(this,a)
        }):arguments.length>1?this.each(function(){n.data(this,a,b)
        }):f?P(f,a,n.data(f,a)):void 0
    },removeData:function(a){return this.each(function(){n.removeData(this,a)
    })
    }}),n.extend({queue:function(a,b,c){var d;
        return a?(b=(b||"fx")+"queue",d=n._data(a,b),c&&(!d||n.isArray(c)?d=n._data(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0
    },dequeue:function(a,b){b=b||"fx";
        var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)
        };
        "inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()
    },_queueHooks:function(a,b){var c=b+"queueHooks";
        return n._data(a,c)||n._data(a,c,{empty:n.Callbacks("once memory").add(function(){n._removeData(a,b+"queue"),n._removeData(a,c)
            })})
    }}),n.fn.extend({queue:function(a,b){var c=2;
        return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);
            n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)
        })
    },dequeue:function(a){return this.each(function(){n.dequeue(this,a)
    })
    },clearQueue:function(a){return this.queue(a||"fx",[])
    },promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])
    };
        "string"!=typeof a&&(b=a,a=void 0),a=a||"fx";
        while(g--){c=n._data(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h))
        }return h(),e.promise(b)
    }}),function(){var a;
        l.shrinkWrapBlocks=function(){if(null!=a){return a
        }a=!1;
            var b,c,e;
            return c=d.getElementsByTagName("body")[0],c&&c.style?(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1",b.appendChild(d.createElement("div")).style.width="5px",a=3!==b.offsetWidth),c.removeChild(e),a):void 0
        }
    }();
    var T=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,U=new RegExp("^(?:([+-])=|)("+T+")([a-z%]*)$","i"),V=["Top","Right","Bottom","Left"],W=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)
    };
    function X(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()
    }:function(){return n.css(a,b,"")
    },i=h(),j=c&&c[3]||(n.cssNumber[b]?"":"px"),k=(n.cssNumber[b]||"px"!==j&&+i)&&U.exec(n.css(a,b));
        if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;
            do{f=f||".5",k/=f,n.style(a,b,k+j)
            }while(f!==(f=h()/i)&&1!==f&&--g)
        }return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e
    }var Y=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;
        if("object"===n.type(c)){e=!0;
            for(h in c){Y(a,b,h,c[h],!0,f,g)
            }}else{if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)
            })),b)){for(;
            i>h;
            h++){b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)))
        }}}return e?a:j?b.call(a):i?b(a[0],c):f
    },Z=/^(?:checkbox|radio)$/i,$=/<([\w:-]+)/,_=/^$|\/(?:java|ecma)script/i,aa=/^\s+/,ba="abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";
    function ca(a){var b=ba.split("|"),c=a.createDocumentFragment();
        if(c.createElement){while(b.length){c.createElement(b.pop())
        }}return c
    }!function(){var a=d.createElement("div"),b=d.createDocumentFragment(),c=d.createElement("input");
        a.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",l.leadingWhitespace=3===a.firstChild.nodeType,l.tbody=!a.getElementsByTagName("tbody").length,l.htmlSerialize=!!a.getElementsByTagName("link").length,l.html5Clone="<:nav></:nav>"!==d.createElement("nav").cloneNode(!0).outerHTML,c.type="checkbox",c.checked=!0,b.appendChild(c),l.appendChecked=c.checked,a.innerHTML="<textarea>x</textarea>",l.noCloneChecked=!!a.cloneNode(!0).lastChild.defaultValue,b.appendChild(a),c=d.createElement("input"),c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),a.appendChild(c),l.checkClone=a.cloneNode(!0).cloneNode(!0).lastChild.checked,l.noCloneEvent=!!a.addEventListener,a[n.expando]=1,l.attributes=!a.getAttribute(n.expando)
    }();
    var da={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:l.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]};
    da.optgroup=da.option,da.tbody=da.tfoot=da.colgroup=da.caption=da.thead,da.th=da.td;
    function ea(a,b){var c,d,e=0,f="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):void 0;
        if(!f){for(f=[],c=a.childNodes||a;
                   null!=(d=c[e]);
                   e++){!b||n.nodeName(d,b)?f.push(d):n.merge(f,ea(d,b))
        }}return void 0===b||b&&n.nodeName(a,b)?n.merge([a],f):f
    }function fa(a,b){for(var c,d=0;
                          null!=(c=a[d]);
                          d++){n._data(c,"globalEval",!b||n._data(b[d],"globalEval"))
    }}var ga=/<|&#?\w+;/,ha=/<tbody/i;
    function ia(a){Z.test(a.type)&&(a.defaultChecked=a.checked)
    }function ja(a,b,c,d,e){for(var f,g,h,i,j,k,m,o=a.length,p=ca(b),q=[],r=0;
                                o>r;
                                r++){if(g=a[r],g||0===g){if("object"===n.type(g)){n.merge(q,g.nodeType?[g]:g)
    }else{if(ga.test(g)){i=i||p.appendChild(b.createElement("div")),j=($.exec(g)||["",""])[1].toLowerCase(),m=da[j]||da._default,i.innerHTML=m[1]+n.htmlPrefilter(g)+m[2],f=m[0];
        while(f--){i=i.lastChild
        }if(!l.leadingWhitespace&&aa.test(g)&&q.push(b.createTextNode(aa.exec(g)[0])),!l.tbody){g="table"!==j||ha.test(g)?"<table>"!==m[1]||ha.test(g)?0:i:i.firstChild,f=g&&g.childNodes.length;
            while(f--){n.nodeName(k=g.childNodes[f],"tbody")&&!k.childNodes.length&&g.removeChild(k)
            }}n.merge(q,i.childNodes),i.textContent="";
        while(i.firstChild){i.removeChild(i.firstChild)
        }i=p.lastChild
    }else{q.push(b.createTextNode(g))
    }}}}i&&p.removeChild(i),l.appendChecked||n.grep(ea(q,"input"),ia),r=0;
        while(g=q[r++]){if(d&&n.inArray(g,d)>-1){e&&e.push(g)
        }else{if(h=n.contains(g.ownerDocument,g),i=ea(p.appendChild(g),"script"),h&&fa(i),c){f=0;
            while(g=i[f++]){_.test(g.type||"")&&c.push(g)
            }}}}return i=null,p
    }!function(){var b,c,e=d.createElement("div");
        for(b in {submit:!0,change:!0,focusin:!0}){c="on"+b,(l[b]=c in a)||(e.setAttribute(c,"t"),l[b]=e.attributes[c].expando===!1)
        }e=null
    }();
    var ka=/^(?:input|select|textarea)$/i,la=/^key/,ma=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,na=/^(?:focusinfocus|focusoutblur)$/,oa=/^([^.]*)(?:\.(.+)|)/;
    function pa(){return !0
    }function qa(){return !1
    }function ra(){try{return d.activeElement
    }catch(a){}}function sa(a,b,c,d,e,f){var g,h;
        if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);
            for(h in b){sa(a,h,c,d,b[h],f)
            }return a
        }if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1){e=qa
        }else{if(!e){return a
        }}return 1===f&&(g=e,e=function(a){return n().off(a),g.apply(this,arguments)
        },e.guid=g.guid||(g.guid=n.guid++)),a.each(function(){n.event.add(this,b,e,d,c)
        })
    }n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n._data(a);
        if(r){c.handler&&(i=c,c=i.handler,e=i.selector),c.guid||(c.guid=n.guid++),(g=r.events)||(g=r.events={}),(k=r.handle)||(k=r.handle=function(a){return"undefined"==typeof n||a&&n.event.triggered===a.type?void 0:n.event.dispatch.apply(k.elem,arguments)
        },k.elem=a),b=(b||"").match(G)||[""],h=b.length;
            while(h--){f=oa.exec(b[h])||[],o=q=f[1],p=(f[2]||"").split(".").sort(),o&&(j=n.event.special[o]||{},o=(e?j.delegateType:j.bindType)||o,j=n.event.special[o]||{},l=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},i),(m=g[o])||(m=g[o]=[],m.delegateCount=0,j.setup&&j.setup.call(a,d,p,k)!==!1||(a.addEventListener?a.addEventListener(o,k,!1):a.attachEvent&&a.attachEvent("on"+o,k))),j.add&&(j.add.call(a,l),l.handler.guid||(l.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,l):m.push(l),n.event.global[o]=!0)
            }a=null
        }},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n.hasData(a)&&n._data(a);
        if(r&&(k=r.events)){b=(b||"").match(G)||[""],j=b.length;
            while(j--){if(h=oa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=k[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),i=f=m.length;
                while(f--){g=m[f],!e&&q!==g.origType||c&&c.guid!==g.guid||h&&!h.test(g.namespace)||d&&d!==g.selector&&("**"!==d||!g.selector)||(m.splice(f,1),g.selector&&m.delegateCount--,l.remove&&l.remove.call(a,g))
                }i&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete k[o])
            }else{for(o in k){n.event.remove(a,o+b[j],c,d,!0)
            }}}n.isEmptyObject(k)&&(delete r.handle,n._removeData(a,"events"))
        }},trigger:function(b,c,e,f){var g,h,i,j,l,m,o,p=[e||d],q=k.call(b,"type")?b.type:b,r=k.call(b,"namespace")?b.namespace.split("."):[];
        if(i=m=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!na.test(q+n.event.triggered)&&(q.indexOf(".")>-1&&(r=q.split("."),q=r.shift(),r.sort()),h=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=r.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:n.makeArray(c,[b]),l=n.event.special[q]||{},f||!l.trigger||l.trigger.apply(e,c)!==!1)){if(!f&&!l.noBubble&&!n.isWindow(e)){for(j=l.delegateType||q,na.test(j+q)||(i=i.parentNode);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    i;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    i=i.parentNode){p.push(i),m=i
        }m===(e.ownerDocument||d)&&p.push(m.defaultView||m.parentWindow||a)
        }o=0;
            while((i=p[o++])&&!b.isPropagationStopped()){b.type=o>1?j:l.bindType||q,g=(n._data(i,"events")||{})[b.type]&&n._data(i,"handle"),g&&g.apply(i,c),g=h&&i[h],g&&g.apply&&M(i)&&(b.result=g.apply(i,c),b.result===!1&&b.preventDefault())
            }if(b.type=q,!f&&!b.isDefaultPrevented()&&(!l._default||l._default.apply(p.pop(),c)===!1)&&M(e)&&h&&e[q]&&!n.isWindow(e)){m=e[h],m&&(e[h]=null),n.event.triggered=q;
                try{e[q]()
                }catch(s){}n.event.triggered=void 0,m&&(e[h]=m)
            }return b.result
        }},dispatch:function(a){a=n.event.fix(a);
        var b,c,d,f,g,h=[],i=e.call(arguments),j=(n._data(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};
        if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;
            while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;
                while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped()){(!a.rnamespace||a.rnamespace.test(g.namespace))&&(a.handleObj=g,a.data=g.data,d=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==d&&(a.result=d)===!1&&(a.preventDefault(),a.stopPropagation()))
                }}return k.postDispatch&&k.postDispatch.call(this,a),a.result
        }},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;
        if(h&&i.nodeType&&("click"!==a.type||isNaN(a.button)||a.button<1)){for(;
            i!=this;
            i=i.parentNode||this){if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(d=[],c=0;
                                                                                              h>c;
                                                                                              c++){f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>-1:n.find(e,this,null,[i]).length),d[e]&&d.push(f)
        }d.length&&g.push({elem:i,handlers:d})
        }}}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g
    },fix:function(a){if(a[n.expando]){return a
    }var b,c,e,f=a.type,g=a,h=this.fixHooks[f];
        h||(this.fixHooks[f]=h=ma.test(f)?this.mouseHooks:la.test(f)?this.keyHooks:{}),e=h.props?this.props.concat(h.props):this.props,a=new n.Event(g),b=e.length;
        while(b--){c=e[b],a[c]=g[c]
        }return a.target||(a.target=g.srcElement||d),3===a.target.nodeType&&(a.target=a.target.parentNode),a.metaKey=!!a.metaKey,h.filter?h.filter(a,g):a
    },props:"altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a
    }},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,e,f,g=b.button,h=b.fromElement;
        return null==a.pageX&&null!=b.clientX&&(e=a.target.ownerDocument||d,f=e.documentElement,c=e.body,a.pageX=b.clientX+(f&&f.scrollLeft||c&&c.scrollLeft||0)-(f&&f.clientLeft||c&&c.clientLeft||0),a.pageY=b.clientY+(f&&f.scrollTop||c&&c.scrollTop||0)-(f&&f.clientTop||c&&c.clientTop||0)),!a.relatedTarget&&h&&(a.relatedTarget=h===a.target?b.toElement:h),a.which||void 0===g||(a.which=1&g?1:2&g?3:4&g?2:0),a
    }},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==ra()&&this.focus){try{return this.focus(),!1
    }catch(a){}}},delegateType:"focusin"},blur:{trigger:function(){return this===ra()&&this.blur?(this.blur(),!1):void 0
    },delegateType:"focusout"},click:{trigger:function(){return n.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):void 0
    },_default:function(a){return n.nodeName(a.target,"a")
    }},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)
    }}},simulate:function(a,b,c){var d=n.extend(new n.Event,c,{type:a,isSimulated:!0});
        n.event.trigger(d,null,b),d.isDefaultPrevented()&&c.preventDefault()
    }},n.removeEvent=d.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)
    }:function(a,b,c){var d="on"+b;
        a.detachEvent&&("undefined"==typeof a[d]&&(a[d]=null),a.detachEvent(d,c))
    },n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?pa:qa):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void (this[n.expando]=!0)):new n.Event(a,b)
    },n.Event.prototype={constructor:n.Event,isDefaultPrevented:qa,isPropagationStopped:qa,isImmediatePropagationStopped:qa,preventDefault:function(){var a=this.originalEvent;
        this.isDefaultPrevented=pa,a&&(a.preventDefault?a.preventDefault():a.returnValue=!1)
    },stopPropagation:function(){var a=this.originalEvent;
        this.isPropagationStopped=pa,a&&!this.isSimulated&&(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)
    },stopImmediatePropagation:function(){var a=this.originalEvent;
        this.isImmediatePropagationStopped=pa,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()
    }},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;
        return(!e||e!==d&&!n.contains(d,e))&&(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c
    }}
    }),l.submit||(n.event.special.submit={setup:function(){return n.nodeName(this,"form")?!1:void n.event.add(this,"click._submit keypress._submit",function(a){var b=a.target,c=n.nodeName(b,"input")||n.nodeName(b,"button")?n.prop(b,"form"):void 0;
        c&&!n._data(c,"submit")&&(n.event.add(c,"submit._submit",function(a){a._submitBubble=!0
        }),n._data(c,"submit",!0))
    })
    },postDispatch:function(a){a._submitBubble&&(delete a._submitBubble,this.parentNode&&!a.isTrigger&&n.event.simulate("submit",this.parentNode,a))
    },teardown:function(){return n.nodeName(this,"form")?!1:void n.event.remove(this,"._submit")
    }}),l.change||(n.event.special.change={setup:function(){return ka.test(this.nodeName)?(("checkbox"===this.type||"radio"===this.type)&&(n.event.add(this,"propertychange._change",function(a){"checked"===a.originalEvent.propertyName&&(this._justChanged=!0)
    }),n.event.add(this,"click._change",function(a){this._justChanged&&!a.isTrigger&&(this._justChanged=!1),n.event.simulate("change",this,a)
    })),!1):void n.event.add(this,"beforeactivate._change",function(a){var b=a.target;
        ka.test(b.nodeName)&&!n._data(b,"change")&&(n.event.add(b,"change._change",function(a){!this.parentNode||a.isSimulated||a.isTrigger||n.event.simulate("change",this.parentNode,a)
        }),n._data(b,"change",!0))
    })
    },handle:function(a){var b=a.target;
        return this!==b||a.isSimulated||a.isTrigger||"radio"!==b.type&&"checkbox"!==b.type?a.handleObj.handler.apply(this,arguments):void 0
    },teardown:function(){return n.event.remove(this,"._change"),!ka.test(this.nodeName)
    }}),l.focusin||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a))
    };
        n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=n._data(d,b);
            e||d.addEventListener(a,c,!0),n._data(d,b,(e||0)+1)
        },teardown:function(){var d=this.ownerDocument||this,e=n._data(d,b)-1;
            e?n._data(d,b,e):(d.removeEventListener(a,c,!0),n._removeData(d,b))
        }}
    }),n.fn.extend({on:function(a,b,c,d){return sa(this,a,b,c,d)
    },one:function(a,b,c,d){return sa(this,a,b,c,d,1)
    },off:function(a,b,c){var d,e;
        if(a&&a.preventDefault&&a.handleObj){return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this
        }if("object"==typeof a){for(e in a){this.off(e,b,a[e])
        }return this
        }return(b===!1||"function"==typeof b)&&(c=b,b=void 0),c===!1&&(c=qa),this.each(function(){n.event.remove(this,a,c,b)
        })
    },trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)
    })
    },triggerHandler:function(a,b){var c=this[0];
        return c?n.event.trigger(a,b,c,!0):void 0
    }});
    var ta=/ jQuery\d+="(?:null|\d+)"/g,ua=new RegExp("<(?:"+ba+")[\\s/>]","i"),va=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,wa=/<script|<style|<link/i,xa=/checked\s*(?:[^=]|=\s*.checked.)/i,ya=/^true\/(.*)/,za=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,Aa=ca(d),Ba=Aa.appendChild(d.createElement("div"));
    function Ca(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a
    }function Da(a){return a.type=(null!==n.find.attr(a,"type"))+"/"+a.type,a
    }function Ea(a){var b=ya.exec(a.type);
        return b?a.type=b[1]:a.removeAttribute("type"),a
    }function Fa(a,b){if(1===b.nodeType&&n.hasData(a)){var c,d,e,f=n._data(a),g=n._data(b,f),h=f.events;
        if(h){delete g.handle,g.events={};
            for(c in h){for(d=0,e=h[c].length;
                            e>d;
                            d++){n.event.add(b,c,h[c][d])
            }}}g.data&&(g.data=n.extend({},g.data))
    }}function Ga(a,b){var c,d,e;
        if(1===b.nodeType){if(c=b.nodeName.toLowerCase(),!l.noCloneEvent&&b[n.expando]){e=n._data(b);
            for(d in e.events){n.removeEvent(b,d,e.handle)
            }b.removeAttribute(n.expando)
        }"script"===c&&b.text!==a.text?(Da(b).text=a.text,Ea(b)):"object"===c?(b.parentNode&&(b.outerHTML=a.outerHTML),l.html5Clone&&a.innerHTML&&!n.trim(b.innerHTML)&&(b.innerHTML=a.innerHTML)):"input"===c&&Z.test(a.type)?(b.defaultChecked=b.checked=a.checked,b.value!==a.value&&(b.value=a.value)):"option"===c?b.defaultSelected=b.selected=a.defaultSelected:("input"===c||"textarea"===c)&&(b.defaultValue=a.defaultValue)
        }}function Ha(a,b,c,d){b=f.apply([],b);
        var e,g,h,i,j,k,m=0,o=a.length,p=o-1,q=b[0],r=n.isFunction(q);
        if(r||o>1&&"string"==typeof q&&!l.checkClone&&xa.test(q)){return a.each(function(e){var f=a.eq(e);
            r&&(b[0]=q.call(this,e,f.html())),Ha(f,b,c,d)
        })
        }if(o&&(k=ja(b,a[0].ownerDocument,!1,a,d),e=k.firstChild,1===k.childNodes.length&&(k=e),e||d)){for(i=n.map(ea(k,"script"),Da),h=i.length;
                                                                                                           o>m;
                                                                                                           m++){g=k,m!==p&&(g=n.clone(g,!0,!0),h&&n.merge(i,ea(g,"script"))),c.call(a[m],g,m)
        }if(h){for(j=i[i.length-1].ownerDocument,n.map(i,Ea),m=0;
                   h>m;
                   m++){g=i[m],_.test(g.type||"")&&!n._data(g,"globalEval")&&n.contains(j,g)&&(g.src?n._evalUrl&&n._evalUrl(g.src):n.globalEval((g.text||g.textContent||g.innerHTML||"").replace(za,"")))
        }}k=e=null
        }return a
    }function Ia(a,b,c){for(var d,e=b?n.filter(b,a):a,f=0;
                            null!=(d=e[f]);
                            f++){c||1!==d.nodeType||n.cleanData(ea(d)),d.parentNode&&(c&&n.contains(d.ownerDocument,d)&&fa(ea(d,"script")),d.parentNode.removeChild(d))
    }return a
    }n.extend({htmlPrefilter:function(a){return a.replace(va,"<$1></$2>")
    },clone:function(a,b,c){var d,e,f,g,h,i=n.contains(a.ownerDocument,a);
        if(l.html5Clone||n.isXMLDoc(a)||!ua.test("<"+a.nodeName+">")?f=a.cloneNode(!0):(Ba.innerHTML=a.outerHTML,Ba.removeChild(f=Ba.firstChild)),!(l.noCloneEvent&&l.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a))){for(d=ea(f),h=ea(a),g=0;
                                                                                                                                                                                                                                           null!=(e=h[g]);
                                                                                                                                                                                                                                           ++g){d[g]&&Ga(e,d[g])
        }}if(b){if(c){for(h=h||ea(a),d=d||ea(f),g=0;
                          null!=(e=h[g]);
                          g++){Fa(e,d[g])
        }}else{Fa(a,f)
        }}return d=ea(f,"script"),d.length>0&&fa(d,!i&&ea(a,"script")),d=h=e=null,f
    },cleanData:function(a,b){for(var d,e,f,g,h=0,i=n.expando,j=n.cache,k=l.attributes,m=n.event.special;
                                  null!=(d=a[h]);
                                  h++){if((b||M(d))&&(f=d[i],g=f&&j[f])){if(g.events){for(e in g.events){m[e]?n.event.remove(d,e):n.removeEvent(d,e,g.handle)
    }}j[f]&&(delete j[f],k||"undefined"==typeof d.removeAttribute?d[i]=void 0:d.removeAttribute(i),c.push(f))
    }}}}),n.fn.extend({domManip:Ha,detach:function(a){return Ia(this,a,!0)
    },remove:function(a){return Ia(this,a)
    },text:function(a){return Y(this,function(a){return void 0===a?n.text(this):this.empty().append((this[0]&&this[0].ownerDocument||d).createTextNode(a))
    },null,a,arguments.length)
    },append:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);
        b.appendChild(a)
    }})
    },prepend:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);
        b.insertBefore(a,b.firstChild)
    }})
    },before:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)
    })
    },after:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)
    })
    },empty:function(){for(var a,b=0;
                           null!=(a=this[b]);
                           b++){1===a.nodeType&&n.cleanData(ea(a,!1));
        while(a.firstChild){a.removeChild(a.firstChild)
        }a.options&&n.nodeName(a,"select")&&(a.options.length=0)
    }return this
    },clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)
    })
    },html:function(a){return Y(this,function(a){var b=this[0]||{},c=0,d=this.length;
        if(void 0===a){return 1===b.nodeType?b.innerHTML.replace(ta,""):void 0
        }if("string"==typeof a&&!wa.test(a)&&(l.htmlSerialize||!ua.test(a))&&(l.leadingWhitespace||!aa.test(a))&&!da[($.exec(a)||["",""])[1].toLowerCase()]){a=n.htmlPrefilter(a);
            try{for(;
                d>c;
                c++){b=this[c]||{},1===b.nodeType&&(n.cleanData(ea(b,!1)),b.innerHTML=a)
            }b=0
            }catch(e){}}b&&this.empty().append(a)
    },null,a,arguments.length)
    },replaceWith:function(){var a=[];
        return Ha(this,arguments,function(b){var c=this.parentNode;
            n.inArray(this,a)<0&&(n.cleanData(ea(this)),c&&c.replaceChild(b,this))
        },a)
    }}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=0,e=[],f=n(a),h=f.length-1;
                                                                                                                                                                h>=d;
                                                                                                                                                                d++){c=d===h?this:this.clone(!0),n(f[d])[b](c),g.apply(e,c.get())
    }return this.pushStack(e)
    }
    });
    var Ja,Ka={HTML:"block",BODY:"block"};
    function La(a,b){var c=n(b.createElement(a)).appendTo(b.body),d=n.css(c[0],"display");
        return c.detach(),d
    }function Ma(a){var b=d,c=Ka[a];
        return c||(c=La(a,b),"none"!==c&&c||(Ja=(Ja||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=(Ja[0].contentWindow||Ja[0].contentDocument).document,b.write(),b.close(),c=La(a,b),Ja.detach()),Ka[a]=c),c
    }var Na=/^margin/,Oa=new RegExp("^("+T+")(?!px)[a-z%]+$","i"),Pa=function(a,b,c,d){var e,f,g={};
        for(f in b){g[f]=a.style[f],a.style[f]=b[f]
        }e=c.apply(a,d||[]);
        for(f in b){a.style[f]=g[f]
        }return e
    },Qa=d.documentElement;
    !function(){var b,c,e,f,g,h,i=d.createElement("div"),j=d.createElement("div");
        if(j.style){j.style.cssText="float:left;opacity:.5",l.opacity="0.5"===j.style.opacity,l.cssFloat=!!j.style.cssFloat,j.style.backgroundClip="content-box",j.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===j.style.backgroundClip,i=d.createElement("div"),i.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",j.innerHTML="",i.appendChild(j),l.boxSizing=""===j.style.boxSizing||""===j.style.MozBoxSizing||""===j.style.WebkitBoxSizing,n.extend(l,{reliableHiddenOffsets:function(){return null==b&&k(),f
        },boxSizingReliable:function(){return null==b&&k(),e
        },pixelMarginRight:function(){return null==b&&k(),c
        },pixelPosition:function(){return null==b&&k(),b
        },reliableMarginRight:function(){return null==b&&k(),g
        },reliableMarginLeft:function(){return null==b&&k(),h
        }});
            function k(){var k,l,m=d.documentElement;
                m.appendChild(i),j.style.cssText="-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",b=e=h=!1,c=g=!0,a.getComputedStyle&&(l=a.getComputedStyle(j),b="1%"!==(l||{}).top,h="2px"===(l||{}).marginLeft,e="4px"===(l||{width:"4px"}).width,j.style.marginRight="50%",c="4px"===(l||{marginRight:"4px"}).marginRight,k=j.appendChild(d.createElement("div")),k.style.cssText=j.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",k.style.marginRight=k.style.width="0",j.style.width="1px",g=!parseFloat((a.getComputedStyle(k)||{}).marginRight),j.removeChild(k)),j.style.display="none",f=0===j.getClientRects().length,f&&(j.style.display="",j.innerHTML="<table><tr><td></td><td>t</td></tr></table>",k=j.getElementsByTagName("td"),k[0].style.cssText="margin:0;border:0;padding:0;display:none",f=0===k[0].offsetHeight,f&&(k[0].style.display="",k[1].style.display="none",f=0===k[0].offsetHeight)),m.removeChild(i)
            }}}();
    var Ra,Sa,Ta=/^(top|right|bottom|left)$/;
    a.getComputedStyle?(Ra=function(b){var c=b.ownerDocument.defaultView;
        return c.opener||(c=a),c.getComputedStyle(b)
    },Sa=function(a,b,c){var d,e,f,g,h=a.style;
        return c=c||Ra(a),g=c?c.getPropertyValue(b)||c[b]:void 0,c&&(""!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),!l.pixelMarginRight()&&Oa.test(g)&&Na.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f)),void 0===g?g:g+""
    }):Qa.currentStyle&&(Ra=function(a){return a.currentStyle
    },Sa=function(a,b,c){var d,e,f,g,h=a.style;
        return c=c||Ra(a),g=c?c[b]:void 0,null==g&&h&&h[b]&&(g=h[b]),Oa.test(g)&&!Ta.test(b)&&(d=h.left,e=a.runtimeStyle,f=e&&e.left,f&&(e.left=a.currentStyle.left),h.left="fontSize"===b?"1em":g,g=h.pixelLeft+"px",h.left=d,f&&(e.left=f)),void 0===g?g:g+""||"auto"
    });
    function Ua(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)
    }}
    }var Va=/alpha\([^)]*\)/i,Wa=/opacity\s*=\s*([^)]*)/i,Xa=/^(none|table(?!-c[ea]).+)/,Ya=new RegExp("^("+T+")(.*)$","i"),Za={position:"absolute",visibility:"hidden",display:"block"},$a={letterSpacing:"0",fontWeight:"400"},_a=["Webkit","O","Moz","ms"],ab=d.createElement("div").style;
    function bb(a){if(a in ab){return a
    }var b=a.charAt(0).toUpperCase()+a.slice(1),c=_a.length;
        while(c--){if(a=_a[c]+b,a in ab){return a
        }}}function cb(a,b){for(var c,d,e,f=[],g=0,h=a.length;
                                h>g;
                                g++){d=a[g],d.style&&(f[g]=n._data(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&W(d)&&(f[g]=n._data(d,"olddisplay",Ma(d.nodeName)))):(e=W(d),(c&&"none"!==c||!e)&&n._data(d,"olddisplay",e?c:n.css(d,"display"))))
    }for(g=0;
         h>g;
         g++){d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"))
    }return a
    }function db(a,b,c){var d=Ya.exec(b);
        return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b
    }function eb(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;
                                4>f;
                                f+=2){"margin"===c&&(g+=n.css(a,c+V[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+V[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+V[f]+"Width",!0,e))):(g+=n.css(a,"padding"+V[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+V[f]+"Width",!0,e)))
    }return g
    }function fb(b,c,e){var f=!0,g="width"===c?b.offsetWidth:b.offsetHeight,h=Ra(b),i=l.boxSizing&&"border-box"===n.css(b,"boxSizing",!1,h);
        if(d.msFullscreenElement&&a.top!==a&&b.getClientRects().length&&(g=Math.round(100*b.getBoundingClientRect()[c])),0>=g||null==g){if(g=Sa(b,c,h),(0>g||null==g)&&(g=b.style[c]),Oa.test(g)){return g
        }f=i&&(l.boxSizingReliable()||g===b.style[c]),g=parseFloat(g)||0
        }return g+eb(b,c,e||(i?"border":"content"),f,h)+"px"
    }n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Sa(a,"opacity");
        return""===c?"1":c
    }}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":l.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;
        if(b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],void 0===c){return g&&"get" in g&&void 0!==(e=g.get(a,!1,d))?e:i[b]
        }if(f=typeof c,"string"===f&&(e=U.exec(c))&&e[1]&&(c=X(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(n.cssNumber[h]?"":"px")),l.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),!(g&&"set" in g&&void 0===(c=g.set(a,c,d))))){try{i[b]=c
        }catch(j){}}}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);
        return b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],g&&"get" in g&&(f=g.get(a,!0,c)),void 0===f&&(f=Sa(a,b,d)),"normal"===f&&b in $a&&(f=$a[b]),""===c||c?(e=parseFloat(f),c===!0||isFinite(e)?e||0:f):f
    }}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?Xa.test(n.css(a,"display"))&&0===a.offsetWidth?Pa(a,Za,function(){return fb(a,b,d)
    }):fb(a,b,d):void 0
    },set:function(a,c,d){var e=d&&Ra(a);
        return db(a,c,d?eb(a,b,d,l.boxSizing&&"border-box"===n.css(a,"boxSizing",!1,e),e):0)
    }}
    }),l.opacity||(n.cssHooks.opacity={get:function(a,b){return Wa.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?0.01*parseFloat(RegExp.$1)+"":b?"1":""
    },set:function(a,b){var c=a.style,d=a.currentStyle,e=n.isNumeric(b)?"alpha(opacity="+100*b+")":"",f=d&&d.filter||c.filter||"";
        c.zoom=1,(b>=1||""===b)&&""===n.trim(f.replace(Va,""))&&c.removeAttribute&&(c.removeAttribute("filter"),""===b||d&&!d.filter)||(c.filter=Va.test(f)?f.replace(Va,e):f+" "+e)
    }}),n.cssHooks.marginRight=Ua(l.reliableMarginRight,function(a,b){return b?Pa(a,{display:"inline-block"},Sa,[a,"marginRight"]):void 0
    }),n.cssHooks.marginLeft=Ua(l.reliableMarginLeft,function(a,b){return b?(parseFloat(Sa(a,"marginLeft"))||(n.contains(a.ownerDocument,a)?a.getBoundingClientRect().left-Pa(a,{marginLeft:0},function(){return a.getBoundingClientRect().left
    }):0))+"px":void 0
    }),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];
                                                                                                          4>d;
                                                                                                          d++){e[a+V[d]+b]=f[d]||f[d-2]||f[0]
    }return e
    }},Na.test(a)||(n.cssHooks[a+b].set=db)
    }),n.fn.extend({css:function(a,b){return Y(this,function(a,b,c){var d,e,f={},g=0;
        if(n.isArray(b)){for(d=Ra(a),e=b.length;
                             e>g;
                             g++){f[b[g]]=n.css(a,b[g],!1,d)
        }return f
        }return void 0!==c?n.style(a,b,c):n.css(a,b)
    },a,b,arguments.length>1)
    },show:function(){return cb(this,!0)
    },hide:function(){return cb(this)
    },toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){W(this)?n(this).show():n(this).hide()
    })
    }});
    function gb(a,b,c,d,e){return new gb.prototype.init(a,b,c,d,e)
    }n.Tween=gb,gb.prototype={constructor:gb,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||n.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")
    },cur:function(){var a=gb.propHooks[this.prop];
        return a&&a.get?a.get(this):gb.propHooks._default.get(this)
    },run:function(a){var b,c=gb.propHooks[this.prop];
        return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):gb.propHooks._default.set(this),this
    }},gb.prototype.init.prototype=gb.prototype,gb.propHooks={_default:{get:function(a){var b;
        return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)
    },set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[n.cssProps[a.prop]]&&!n.cssHooks[a.prop]?a.elem[a.prop]=a.now:n.style(a.elem,a.prop,a.now+a.unit)
    }}},gb.propHooks.scrollTop=gb.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)
    }},n.easing={linear:function(a){return a
    },swing:function(a){return 0.5-Math.cos(a*Math.PI)/2
    },_default:"swing"},n.fx=gb.prototype.init,n.fx.step={};
    var hb,ib,jb=/^(?:toggle|show|hide)$/,kb=/queueHooks$/;
    function lb(){return a.setTimeout(function(){hb=void 0
    }),hb=n.now()
    }function mb(a,b){var c,d={height:a},e=0;
        for(b=b?1:0;
            4>e;
            e+=2-b){c=V[e],d["margin"+c]=d["padding"+c]=a
        }return b&&(d.opacity=d.width=a),d
    }function nb(a,b,c){for(var d,e=(qb.tweeners[b]||[]).concat(qb.tweeners["*"]),f=0,g=e.length;
                            g>f;
                            f++){if(d=e[f].call(c,b,a)){return d
    }}}function ob(a,b,c){var d,e,f,g,h,i,j,k,m=this,o={},p=a.style,q=a.nodeType&&W(a),r=n._data(a,"fxshow");
        c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()
        }),h.unqueued++,m.always(function(){m.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()
        })
        })),1===a.nodeType&&("height" in b||"width" in b)&&(c.overflow=[p.overflow,p.overflowX,p.overflowY],j=n.css(a,"display"),k="none"===j?n._data(a,"olddisplay")||Ma(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(l.inlineBlockNeedsLayout&&"inline"!==Ma(a.nodeName)?p.zoom=1:p.display="inline-block")),c.overflow&&(p.overflow="hidden",l.shrinkWrapBlocks()||m.always(function(){p.overflow=c.overflow[0],p.overflowX=c.overflow[1],p.overflowY=c.overflow[2]
        }));
        for(d in b){if(e=b[d],jb.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(q?"hide":"show")){if("show"!==e||!r||void 0===r[d]){continue
        }q=!0
        }o[d]=r&&r[d]||n.style(a,d)
        }else{j=void 0
        }}if(n.isEmptyObject(o)){"inline"===("none"===j?Ma(a.nodeName):j)&&(p.display=j)
        }else{r?"hidden" in r&&(q=r.hidden):r=n._data(a,"fxshow",{}),f&&(r.hidden=!q),q?n(a).show():m.done(function(){n(a).hide()
        }),m.done(function(){var b;
            n._removeData(a,"fxshow");
            for(b in o){n.style(a,b,o[b])
            }});
            for(d in o){g=nb(q?r[d]:0,d,m),d in r||(r[d]=g.start,q&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))
            }}}function pb(a,b){var c,d,e,f,g;
        for(c in a){if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand" in g){f=g.expand(f),delete a[d];
            for(c in f){c in a||(a[c]=f[c],b[c]=e)
            }}else{b[d]=e
        }}}function qb(a,b,c){var d,e,f=0,g=qb.prefilters.length,h=n.Deferred().always(function(){delete i.elem
    }),i=function(){if(e){return !1
    }for(var b=hb||lb(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;
         i>g;
         g++){j.tweens[g].run(f)
    }return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)
    },j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{},easing:n.easing._default},c),originalProperties:b,originalOptions:c,startTime:hb||lb(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);
        return j.tweens.push(d),d
    },stop:function(b){var c=0,d=b?j.tweens.length:0;
        if(e){return this
        }for(e=!0;
             d>c;
             c++){j.tweens[c].run(1)
        }return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this
    }}),k=j.props;
        for(pb(k,j.opts.specialEasing);
            g>f;
            f++){if(d=qb.prefilters[f].call(j,a,k,j.opts)){return n.isFunction(d.stop)&&(n._queueHooks(j.elem,j.opts.queue).stop=n.proxy(d.stop,d)),d
        }}return n.map(k,nb,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)
    }n.Animation=n.extend(qb,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);
        return X(c.elem,a,U.exec(b),c),c
    }]},tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.match(G);
        for(var c,d=0,e=a.length;
            e>d;
            d++){c=a[d],qb.tweeners[c]=qb.tweeners[c]||[],qb.tweeners[c].unshift(b)
        }},prefilters:[ob],prefilter:function(a,b){b?qb.prefilters.unshift(a):qb.prefilters.push(a)
    }}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};
        return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,(null==d.queue||d.queue===!0)&&(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)
        },d
    },n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(W).css("opacity",0).show().end().animate({opacity:b},a,c,d)
    },animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=qb(this,n.extend({},a),f);
        (e||n._data(this,"finish"))&&b.stop(!0)
    };
        return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)
    },stop:function(a,b,c){var d=function(a){var b=a.stop;
        delete a.stop,b(c)
    };
        return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=n._data(this);
            if(e){g[e]&&g[e].stop&&d(g[e])
            }else{for(e in g){g[e]&&g[e].stop&&kb.test(e)&&d(g[e])
            }}for(e=f.length;
                  e--;
            ){f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1))
            }(b||!c)&&n.dequeue(this,a)
        })
    },finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=n._data(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;
        for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;
            b--;
        ){f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1))
        }for(b=0;
             g>b;
             b++){d[b]&&d[b].finish&&d[b].finish.call(this)
        }delete c.finish
    })
    }}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];
        n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(mb(b,!0),a,d,e)
        }
    }),n.each({slideDown:mb("show"),slideUp:mb("hide"),slideToggle:mb("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)
    }
    }),n.timers=[],n.fx.tick=function(){var a,b=n.timers,c=0;
        for(hb=n.now();
            c<b.length;
            c++){a=b[c],a()||b[c]!==a||b.splice(c--,1)
        }b.length||n.fx.stop(),hb=void 0
    },n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()
    },n.fx.interval=13,n.fx.start=function(){ib||(ib=a.setInterval(n.fx.tick,n.fx.interval))
    },n.fx.stop=function(){a.clearInterval(ib),ib=null
    },n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(b,c){return b=n.fx?n.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);
        d.stop=function(){a.clearTimeout(e)
        }
    })
    },function(){var a,b=d.createElement("input"),c=d.createElement("div"),e=d.createElement("select"),f=e.appendChild(d.createElement("option"));
        c=d.createElement("div"),c.setAttribute("className","t"),c.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",a=c.getElementsByTagName("a")[0],b.setAttribute("type","checkbox"),c.appendChild(b),a=c.getElementsByTagName("a")[0],a.style.cssText="top:1px",l.getSetAttribute="t"!==c.className,l.style=/top/.test(a.getAttribute("style")),l.hrefNormalized="/a"===a.getAttribute("href"),l.checkOn=!!b.value,l.optSelected=f.selected,l.enctype=!!d.createElement("form").enctype,e.disabled=!0,l.optDisabled=!f.disabled,b=d.createElement("input"),b.setAttribute("value",""),l.input=""===b.getAttribute("value"),b.value="t",b.setAttribute("type","radio"),l.radioValue="t"===b.value
    }();
    var rb=/\r/g;
    n.fn.extend({val:function(a){var b,c,d,e=this[0];
        if(arguments.length){return d=n.isFunction(a),this.each(function(c){var e;
            1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""
            })),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set" in b&&void 0!==b.set(this,e,"value")||(this.value=e))
        })
        }if(e){return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get" in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(rb,""):null==c?"":c)
        }}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");
        return null!=b?b:n.trim(n.text(a))
    }},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;
                                   h>i;
                                   i++){if(c=d[i],(c.selected||i===e)&&(l.optDisabled?!c.disabled:null===c.getAttribute("disabled"))&&(!c.parentNode.disabled||!n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f){return b
    }g.push(b)
    }}return g
    },set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;
        while(g--){if(d=e[g],n.inArray(n.valHooks.option.get(d),f)>=0){try{d.selected=c=!0
        }catch(h){d.scrollHeight
        }}else{d.selected=!1
        }}return c||(a.selectedIndex=-1),e
    }}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>-1:void 0
    }},l.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value
    })
    });
    var sb,tb,ub=n.expr.attrHandle,vb=/^(?:checked|selected)$/i,wb=l.getSetAttribute,xb=l.input;
    n.fn.extend({attr:function(a,b){return Y(this,n.attr,a,b,arguments.length>1)
    },removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)
    })
    }}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;
        if(3!==f&&8!==f&&2!==f){return"undefined"==typeof a.getAttribute?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),e=n.attrHooks[b]||(n.expr.match.bool.test(b)?tb:sb)),void 0!==c?null===c?void n.removeAttr(a,b):e&&"set" in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get" in e&&null!==(d=e.get(a,b))?d:(d=n.find.attr(a,b),null==d?void 0:d))
        }},attrHooks:{type:{set:function(a,b){if(!l.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;
        return a.setAttribute("type",b),c&&(a.value=c),b
    }}}},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(G);
        if(f&&1===a.nodeType){while(c=f[e++]){d=n.propFix[c]||c,n.expr.match.bool.test(c)?xb&&wb||!vb.test(c)?a[d]=!1:a[n.camelCase("default-"+c)]=a[d]=!1:n.attr(a,c,""),a.removeAttribute(wb?c:d)
        }}}}),tb={set:function(a,b,c){return b===!1?n.removeAttr(a,c):xb&&wb||!vb.test(c)?a.setAttribute(!wb&&n.propFix[c]||c,c):a[n.camelCase("default-"+c)]=a[c]=!0,c
    }},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=ub[b]||n.find.attr;
        xb&&wb||!vb.test(b)?ub[b]=function(a,b,d){var e,f;
            return d||(f=ub[b],ub[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,ub[b]=f),e
        }:ub[b]=function(a,b,c){return c?void 0:a[n.camelCase("default-"+b)]?b.toLowerCase():null
        }
    }),xb&&wb||(n.attrHooks.value={set:function(a,b,c){return n.nodeName(a,"input")?void (a.defaultValue=b):sb&&sb.set(a,b,c)
    }}),wb||(sb={set:function(a,b,c){var d=a.getAttributeNode(c);
        return d||a.setAttributeNode(d=a.ownerDocument.createAttribute(c)),d.value=b+="","value"===c||b===a.getAttribute(c)?b:void 0
    }},ub.id=ub.name=ub.coords=function(a,b,c){var d;
        return c?void 0:(d=a.getAttributeNode(b))&&""!==d.value?d.value:null
    },n.valHooks.button={get:function(a,b){var c=a.getAttributeNode(b);
        return c&&c.specified?c.value:void 0
    },set:sb.set},n.attrHooks.contenteditable={set:function(a,b,c){sb.set(a,""===b?!1:b,c)
    }},n.each(["width","height"],function(a,b){n.attrHooks[b]={set:function(a,c){return""===c?(a.setAttribute(b,"auto"),c):void 0
    }}
    })),l.style||(n.attrHooks.style={get:function(a){return a.style.cssText||void 0
    },set:function(a,b){return a.style.cssText=b+""
    }});
    var yb=/^(?:input|select|textarea|button|object)$/i,zb=/^(?:a|area)$/i;
    n.fn.extend({prop:function(a,b){return Y(this,n.prop,a,b,arguments.length>1)
    },removeProp:function(a){return a=n.propFix[a]||a,this.each(function(){try{this[a]=void 0,delete this[a]
    }catch(b){}})
    }}),n.extend({prop:function(a,b,c){var d,e,f=a.nodeType;
        if(3!==f&&8!==f&&2!==f){return 1===f&&n.isXMLDoc(a)||(b=n.propFix[b]||b,e=n.propHooks[b]),void 0!==c?e&&"set" in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get" in e&&null!==(d=e.get(a,b))?d:a[b]
        }},propHooks:{tabIndex:{get:function(a){var b=n.find.attr(a,"tabindex");
        return b?parseInt(b,10):yb.test(a.nodeName)||zb.test(a.nodeName)&&a.href?0:-1
    }}},propFix:{"for":"htmlFor","class":"className"}}),l.hrefNormalized||n.each(["href","src"],function(a,b){n.propHooks[b]={get:function(a){return a.getAttribute(b,4)
    }}
    }),l.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;
        return b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex),null
    }}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this
    }),l.enctype||(n.propFix.enctype="encoding");
    var Ab=/[\t\r\n\f]/g;
    function Bb(a){return n.attr(a,"class")||""
    }n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;
        if(n.isFunction(a)){return this.each(function(b){n(this).addClass(a.call(this,b,Bb(this)))
        })
        }if("string"==typeof a&&a){b=a.match(G)||[];
            while(c=this[i++]){if(e=Bb(c),d=1===c.nodeType&&(" "+e+" ").replace(Ab," ")){g=0;
                while(f=b[g++]){d.indexOf(" "+f+" ")<0&&(d+=f+" ")
                }h=n.trim(d),e!==h&&n.attr(c,"class",h)
            }}}return this
    },removeClass:function(a){var b,c,d,e,f,g,h,i=0;
        if(n.isFunction(a)){return this.each(function(b){n(this).removeClass(a.call(this,b,Bb(this)))
        })
        }if(!arguments.length){return this.attr("class","")
        }if("string"==typeof a&&a){b=a.match(G)||[];
            while(c=this[i++]){if(e=Bb(c),d=1===c.nodeType&&(" "+e+" ").replace(Ab," ")){g=0;
                while(f=b[g++]){while(d.indexOf(" "+f+" ")>-1){d=d.replace(" "+f+" "," ")
                }}h=n.trim(d),e!==h&&n.attr(c,"class",h)
            }}}return this
    },toggleClass:function(a,b){var c=typeof a;
        return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):n.isFunction(a)?this.each(function(c){n(this).toggleClass(a.call(this,c,Bb(this),b),b)
        }):this.each(function(){var b,d,e,f;
            if("string"===c){d=0,e=n(this),f=a.match(G)||[];
                while(b=f[d++]){e.hasClass(b)?e.removeClass(b):e.addClass(b)
                }}else{(void 0===a||"boolean"===c)&&(b=Bb(this),b&&n._data(this,"__className__",b),n.attr(this,"class",b||a===!1?"":n._data(this,"__className__")||""))
            }})
    },hasClass:function(a){var b,c,d=0;
        b=" "+a+" ";
        while(c=this[d++]){if(1===c.nodeType&&(" "+Bb(c)+" ").replace(Ab," ").indexOf(b)>-1){return !0
        }}return !1
    }}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)
    }
    }),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)
    }});
    var Cb=a.location,Db=n.now(),Eb=/\?/,Fb=/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    n.parseJSON=function(b){if(a.JSON&&a.JSON.parse){return a.JSON.parse(b+"")
    }var c,d=null,e=n.trim(b+"");
        return e&&!n.trim(e.replace(Fb,function(a,b,e,f){return c&&b&&(d=0),0===d?a:(c=e||b,d+=!f-!e,"")
        }))?Function("return "+e)():n.error("Invalid JSON: "+b)
    },n.parseXML=function(b){var c,d;
        if(!b||"string"!=typeof b){return null
        }try{a.DOMParser?(d=new a.DOMParser,c=d.parseFromString(b,"text/xml")):(c=new a.ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(b))
        }catch(e){c=void 0
        }return c&&c.documentElement&&!c.getElementsByTagName("parsererror").length||n.error("Invalid XML: "+b),c
    };
    var Gb=/#.*$/,Hb=/([?&])_=[^&]*/,Ib=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Jb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Kb=/^(?:GET|HEAD)$/,Lb=/^\/\//,Mb=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,Nb={},Ob={},Pb="*/".concat("*"),Qb=Cb.href,Rb=Mb.exec(Qb.toLowerCase())||[];
    function Sb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");
        var d,e=0,f=b.toLowerCase().match(G)||[];
        if(n.isFunction(c)){while(d=f[e++]){"+"===d.charAt(0)?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)
        }}}
    }function Tb(a,b,c,d){var e={},f=a===Ob;
        function g(h){var i;
            return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);
                return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)
            }),i
        }return g(b.dataTypes[0])||!e["*"]&&g("*")
    }function Ub(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};
        for(d in b){void 0!==b[d]&&((e[d]?a:c||(c={}))[d]=b[d])
        }return c&&n.extend(!0,a,c),a
    }function Vb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;
        while("*"===i[0]){i.shift(),void 0===e&&(e=a.mimeType||b.getResponseHeader("Content-Type"))
        }if(e){for(g in h){if(h[g]&&h[g].test(e)){i.unshift(g);
            break
        }}}if(i[0] in c){f=i[0]
        }else{for(g in c){if(!i[0]||a.converters[g+" "+i[0]]){f=g;
            break
        }d||(d=g)
        }f=f||d
        }return f?(f!==i[0]&&i.unshift(f),c[f]):void 0
    }function Wb(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();
        if(k[1]){for(g in a.converters){j[g.toLowerCase()]=a.converters[g]
        }}f=k.shift();
        while(f){if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift()){if("*"===f){f=i
        }else{if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g){for(e in j){if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));
            break
        }}}if(g!==!0){if(g&&a["throws"]){b=g(b)
        }else{try{b=g(b)
        }catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}
        }}}}}}}return{state:"success",data:b}
    }n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Qb,type:"GET",isLocal:Jb.test(Rb[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Pb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Ub(Ub(a,n.ajaxSettings),b):Ub(n.ajaxSettings,a)
    },ajaxPrefilter:Sb(Nb),ajaxTransport:Sb(Ob),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};
        var d,e,f,g,h,i,j,k,l=n.ajaxSetup({},c),m=l.context||l,o=l.context&&(m.nodeType||m.jquery)?n(m):n.event,p=n.Deferred(),q=n.Callbacks("once memory"),r=l.statusCode||{},s={},t={},u=0,v="canceled",w={readyState:0,getResponseHeader:function(a){var b;
            if(2===u){if(!k){k={};
                while(b=Ib.exec(g)){k[b[1].toLowerCase()]=b[2]
                }}b=k[a.toLowerCase()]
            }return null==b?null:b
        },getAllResponseHeaders:function(){return 2===u?g:null
        },setRequestHeader:function(a,b){var c=a.toLowerCase();
            return u||(a=t[c]=t[c]||a,s[a]=b),this
        },overrideMimeType:function(a){return u||(l.mimeType=a),this
        },statusCode:function(a){var b;
            if(a){if(2>u){for(b in a){r[b]=[r[b],a[b]]
            }}else{w.always(a[w.status])
            }}return this
        },abort:function(a){var b=a||v;
            return j&&j.abort(b),y(0,b),this
        }};
        if(p.promise(w).complete=q.add,w.success=w.done,w.error=w.fail,l.url=((b||l.url||Qb)+"").replace(Gb,"").replace(Lb,Rb[1]+"//"),l.type=c.method||c.type||l.method||l.type,l.dataTypes=n.trim(l.dataType||"*").toLowerCase().match(G)||[""],null==l.crossDomain&&(d=Mb.exec(l.url.toLowerCase()),l.crossDomain=!(!d||d[1]===Rb[1]&&d[2]===Rb[2]&&(d[3]||("http:"===d[1]?"80":"443"))===(Rb[3]||("http:"===Rb[1]?"80":"443")))),l.data&&l.processData&&"string"!=typeof l.data&&(l.data=n.param(l.data,l.traditional)),Tb(Nb,l,c,w),2===u){return w
        }i=n.event&&l.global,i&&0===n.active++&&n.event.trigger("ajaxStart"),l.type=l.type.toUpperCase(),l.hasContent=!Kb.test(l.type),f=l.url,l.hasContent||(l.data&&(f=l.url+=(Eb.test(f)?"&":"?")+l.data,delete l.data),l.cache===!1&&(l.url=Hb.test(f)?f.replace(Hb,"$1_="+Db++):f+(Eb.test(f)?"&":"?")+"_="+Db++)),l.ifModified&&(n.lastModified[f]&&w.setRequestHeader("If-Modified-Since",n.lastModified[f]),n.etag[f]&&w.setRequestHeader("If-None-Match",n.etag[f])),(l.data&&l.hasContent&&l.contentType!==!1||c.contentType)&&w.setRequestHeader("Content-Type",l.contentType),w.setRequestHeader("Accept",l.dataTypes[0]&&l.accepts[l.dataTypes[0]]?l.accepts[l.dataTypes[0]]+("*"!==l.dataTypes[0]?", "+Pb+"; q=0.01":""):l.accepts["*"]);
        for(e in l.headers){w.setRequestHeader(e,l.headers[e])
        }if(l.beforeSend&&(l.beforeSend.call(m,w,l)===!1||2===u)){return w.abort()
        }v="abort";
        for(e in {success:1,error:1,complete:1}){w[e](l[e])
        }if(j=Tb(Ob,l,c,w)){if(w.readyState=1,i&&o.trigger("ajaxSend",[w,l]),2===u){return w
        }l.async&&l.timeout>0&&(h=a.setTimeout(function(){w.abort("timeout")
        },l.timeout));
            try{u=1,j.send(s,y)
            }catch(x){if(!(2>u)){throw x
            }y(-1,x)
            }}else{y(-1,"No Transport")
        }function y(b,c,d,e){var k,s,t,v,x,y=c;
            2!==u&&(u=2,h&&a.clearTimeout(h),j=void 0,g=e||"",w.readyState=b>0?4:0,k=b>=200&&300>b||304===b,d&&(v=Vb(l,w,d)),v=Wb(l,v,w,k),k?(l.ifModified&&(x=w.getResponseHeader("Last-Modified"),x&&(n.lastModified[f]=x),x=w.getResponseHeader("etag"),x&&(n.etag[f]=x)),204===b||"HEAD"===l.type?y="nocontent":304===b?y="notmodified":(y=v.state,s=v.data,t=v.error,k=!t)):(t=y,(b||!y)&&(y="error",0>b&&(b=0))),w.status=b,w.statusText=(c||y)+"",k?p.resolveWith(m,[s,y,w]):p.rejectWith(m,[w,y,t]),w.statusCode(r),r=void 0,i&&o.trigger(k?"ajaxSuccess":"ajaxError",[w,l,k?s:t]),q.fireWith(m,[w,y]),i&&(o.trigger("ajaxComplete",[w,l]),--n.active||n.event.trigger("ajaxStop")))
        }return w
    },getJSON:function(a,b,c){return n.get(a,b,c,"json")
    },getScript:function(a,b){return n.get(a,void 0,b,"script")
    }}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax(n.extend({url:a,type:b,dataType:e,data:c,success:d},n.isPlainObject(a)&&a))
    }
    }),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,"throws":!0})
    },n.fn.extend({wrapAll:function(a){if(n.isFunction(a)){return this.each(function(b){n(this).wrapAll(a.call(this,b))
    })
    }if(this[0]){var b=n(a,this[0].ownerDocument).eq(0).clone(!0);
        this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;
            while(a.firstChild&&1===a.firstChild.nodeType){a=a.firstChild
            }return a
        }).append(this)
    }return this
    },wrapInner:function(a){return n.isFunction(a)?this.each(function(b){n(this).wrapInner(a.call(this,b))
    }):this.each(function(){var b=n(this),c=b.contents();
        c.length?c.wrapAll(a):b.append(a)
    })
    },wrap:function(a){var b=n.isFunction(a);
        return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)
        })
    },unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)
    }).end()
    }});
    function Xb(a){return a.style&&a.style.display||n.css(a,"display")
    }function Yb(a){while(a&&1===a.nodeType){if("none"===Xb(a)||"hidden"===a.type){return !0
    }a=a.parentNode
    }return !1
    }n.expr.filters.hidden=function(a){return l.reliableHiddenOffsets()?a.offsetWidth<=0&&a.offsetHeight<=0&&!a.getClientRects().length:Yb(a)
    },n.expr.filters.visible=function(a){return !n.expr.filters.hidden(a)
    };
    var Zb=/%20/g,$b=/\[\]$/,_b=/\r?\n/g,ac=/^(?:submit|button|image|reset|file)$/i,bc=/^(?:input|select|textarea|keygen)/i;
    function cc(a,b,c,d){var e;
        if(n.isArray(b)){n.each(b,function(b,e){c||$b.test(a)?d(a,e):cc(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)
        })
        }else{if(c||"object"!==n.type(b)){d(a,b)
        }else{for(e in b){cc(a+"["+e+"]",b[e],c,d)
        }}}}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)
    };
        if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a)){n.each(a,function(){e(this.name,this.value)
        })
        }else{for(c in a){cc(c,a[c],b,e)
        }}return d.join("&").replace(Zb,"+")
    },n.fn.extend({serialize:function(){return n.param(this.serializeArray())
    },serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");
        return a?n.makeArray(a):this
    }).filter(function(){var a=this.type;
        return this.name&&!n(this).is(":disabled")&&bc.test(this.nodeName)&&!ac.test(a)&&(this.checked||!Z.test(a))
    }).map(function(a,b){var c=n(this).val();
        return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(_b,"\r\n")}
        }):{name:b.name,value:c.replace(_b,"\r\n")}
    }).get()
    }}),n.ajaxSettings.xhr=void 0!==a.ActiveXObject?function(){return this.isLocal?hc():d.documentMode>8?gc():/^(get|post|head|put|delete|options)$/i.test(this.type)&&gc()||hc()
    }:gc;
    var dc=0,ec={},fc=n.ajaxSettings.xhr();
    a.attachEvent&&a.attachEvent("onunload",function(){for(var a in ec){ec[a](void 0,!0)
    }}),l.cors=!!fc&&"withCredentials" in fc,fc=l.ajax=!!fc,fc&&n.ajaxTransport(function(b){if(!b.crossDomain||l.cors){var c;
        return{send:function(d,e){var f,g=b.xhr(),h=++dc;
            if(g.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields){for(f in b.xhrFields){g[f]=b.xhrFields[f]
            }}b.mimeType&&g.overrideMimeType&&g.overrideMimeType(b.mimeType),b.crossDomain||d["X-Requested-With"]||(d["X-Requested-With"]="XMLHttpRequest");
            for(f in d){void 0!==d[f]&&g.setRequestHeader(f,d[f]+"")
            }g.send(b.hasContent&&b.data||null),c=function(a,d){var f,i,j;
                if(c&&(d||4===g.readyState)){if(delete ec[h],c=void 0,g.onreadystatechange=n.noop,d){4!==g.readyState&&g.abort()
                }else{j={},f=g.status,"string"==typeof g.responseText&&(j.text=g.responseText);
                    try{i=g.statusText
                    }catch(k){i=""
                    }f||!b.isLocal||b.crossDomain?1223===f&&(f=204):f=j.text?200:404
                }}j&&e(f,i,j,g.getAllResponseHeaders())
            },b.async?4===g.readyState?a.setTimeout(c):g.onreadystatechange=ec[h]=c:c()
        },abort:function(){c&&c(void 0,!0)
        }}
    }});
    function gc(){try{return new a.XMLHttpRequest
    }catch(b){}}function hc(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")
    }catch(b){}}n.ajaxPrefilter(function(a){a.crossDomain&&(a.contents.script=!1)
    }),n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return n.globalEval(a),a
    }}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)
    }),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c=d.head||n("head")[0]||d.documentElement;
        return{send:function(e,f){b=d.createElement("script"),b.async=!0,a.scriptCharset&&(b.charset=a.scriptCharset),b.src=a.url,b.onload=b.onreadystatechange=function(a,c){(c||!b.readyState||/loaded|complete/.test(b.readyState))&&(b.onload=b.onreadystatechange=null,b.parentNode&&b.parentNode.removeChild(b),b=null,c||f(200,"success"))
        },c.insertBefore(b,c.firstChild)
        },abort:function(){b&&b.onload(void 0,!0)
        }}
    }});
    var ic=[],jc=/(=)\?(?=&|$)|\?\?/;
    n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=ic.pop()||n.expando+"_"+Db++;
        return this[a]=!0,a
    }}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(jc.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&jc.test(b.data)&&"data");
        return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(jc,"$1"+e):b.jsonp!==!1&&(b.url+=(Eb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]
        },b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments
        },d.always(function(){void 0===f?n(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,ic.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0
        }),"script"):void 0
    }),l.createHTMLDocument=function(){if(!d.implementation.createHTMLDocument){return !1
    }var a=d.implementation.createHTMLDocument("");
        return a.body.innerHTML="<form></form><form></form>",2===a.body.childNodes.length
    }(),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a){return null
    }"boolean"==typeof b&&(c=b,b=!1),b=b||(l.createHTMLDocument?d.implementation.createHTMLDocument(""):d);
        var e=x.exec(a),f=!c&&[];
        return e?[b.createElement(e[1])]:(e=ja([a],b,f),f&&f.length&&n(f).remove(),n.merge([],e.childNodes))
    };
    var kc=n.fn.load;
    n.fn.load=function(a,b,c){if("string"!=typeof a&&kc){return kc.apply(this,arguments)
    }var d,e,f,g=this,h=a.indexOf(" ");
        return h>-1&&(d=n.trim(a.slice(h,a.length)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)
        }).always(c&&function(a,b){g.each(function(){c.apply(g,f||[a.responseText,b,a])
        })
        }),this
    },n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)
    }
    }),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem
    }).length
    };
    function lc(a){return n.isWindow(a)?a:9===a.nodeType?a.defaultView||a.parentWindow:!1
    }n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};
        "static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&n.inArray("auto",[f,i])>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,n.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using" in b?b.using.call(a,m):l.css(m)
    }},n.fn.extend({offset:function(a){if(arguments.length){return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)
    })
    }var b,c,d={top:0,left:0},e=this[0],f=e&&e.ownerDocument;
        if(f){return b=f.documentElement,n.contains(b,e)?("undefined"!=typeof e.getBoundingClientRect&&(d=e.getBoundingClientRect()),c=lc(f),{top:d.top+(c.pageYOffset||b.scrollTop)-(b.clientTop||0),left:d.left+(c.pageXOffset||b.scrollLeft)-(b.clientLeft||0)}):d
        }},position:function(){if(this[0]){var a,b,c={top:0,left:0},d=this[0];
        return"fixed"===n.css(d,"position")?b=d.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(c=a.offset()),c.top+=n.css(a[0],"borderTopWidth",!0)-a.scrollTop(),c.left+=n.css(a[0],"borderLeftWidth",!0)-a.scrollLeft()),{top:b.top-c.top-n.css(d,"marginTop",!0),left:b.left-c.left-n.css(d,"marginLeft",!0)}
    }},offsetParent:function(){return this.map(function(){var a=this.offsetParent;
        while(a&&!n.nodeName(a,"html")&&"static"===n.css(a,"position")){a=a.offsetParent
        }return a||Qa
    })
    }}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c=/Y/.test(b);
        n.fn[a]=function(d){return Y(this,function(a,d,e){var f=lc(a);
            return void 0===e?f?b in f?f[b]:f.document.documentElement[d]:a[d]:void (f?f.scrollTo(c?n(f).scrollLeft():e,c?e:n(f).scrollTop()):a[d]=e)
        },a,d,arguments.length,null)
        }
    }),n.each(["top","left"],function(a,b){n.cssHooks[b]=Ua(l.pixelPosition,function(a,c){return c?(c=Sa(a,b),Oa.test(c)?n(a).position()[b]+"px":c):void 0
    })
    }),n.each({Height:"height",Width:"width"},function(a,b){n.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");
        return Y(this,function(b,c,d){var e;
            return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)
        },b,f?d:void 0,f,null)
    }
    })
    }),n.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)
    },unbind:function(a,b){return this.off(a,null,b)
    },delegate:function(a,b,c,d){return this.on(b,a,c,d)
    },undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)
    }}),n.fn.size=function(){return this.length
    },n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n
    });
    var mc=a.jQuery,nc=a.$;
    return n.noConflict=function(b){return a.$===n&&(a.$=nc),b&&a.jQuery===n&&(a.jQuery=mc),n
    },b||(a.jQuery=a.$=n),n
});
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof module&&module.exports?module.exports=a(require("jquery")):a(jQuery)
}(function(b){b.extend(b.fn,{validate:function(f){if(!this.length){return void (f&&f.debug&&window.console&&console.warn("Nothing selected, can't validate, returning nothing."))
}var d=b.data(this[0],"validator");
    return d?d:(this.attr("novalidate","novalidate"),d=new b.validator(f,this[0]),b.data(this[0],"validator",d),d.settings.onsubmit&&(this.on("click.validate",":submit",function(g){d.settings.submitHandler&&(d.submitButton=g.target),b(this).hasClass("cancel")&&(d.cancelSubmit=!0),void 0!==b(this).attr("formnovalidate")&&(d.cancelSubmit=!0)
    }),this.on("submit.validate",function(h){function g(){var e,j;
        return d.settings.submitHandler?(d.submitButton&&(e=b("<input type='hidden'/>").attr("name",d.submitButton.name).val(b(d.submitButton).val()).appendTo(d.currentForm)),j=d.settings.submitHandler.call(d,d.currentForm,h),d.submitButton&&e.remove(),void 0!==j?j:!1):!0
    }return d.settings.debug&&h.preventDefault(),d.cancelSubmit?(d.cancelSubmit=!1,g()):d.form()?d.pendingRequest?(d.formSubmitted=!0,!1):g():(d.focusInvalid(),!1)
    })),d)
},valid:function(){var g,d,f;
    return b(this[0]).is("form")?g=this.validate().form():(f=[],g=!0,d=b(this[0].form).validate(),this.each(function(){g=d.element(this)&&g,g||(f=f.concat(d.errorList))
    }),d.errorList=f),g
},rules:function(p,k){if(this.length){var t,d,g,q,f,j,m=this[0];
    if(p){switch(t=b.data(m.form,"validator").settings,d=t.rules,g=b.validator.staticRules(m),p){case"add":b.extend(g,b.validator.normalizeRule(k)),delete g.messages,d[m.name]=g,k.messages&&(t.messages[m.name]=b.extend(t.messages[m.name],k.messages));
        break;
        case"remove":return k?(j={},b.each(k.split(/\s/),function(l,h){j[h]=g[h],delete g[h],"required"===h&&b(m).removeAttr("aria-required")
        }),j):(delete d[m.name],g)
    }}return q=b.validator.normalizeRules(b.extend({},b.validator.classRules(m),b.validator.attributeRules(m),b.validator.dataRules(m),b.validator.staticRules(m)),m),q.required&&(f=q.required,delete q.required,q=b.extend({required:f},q),b(m).attr("aria-required","true")),q.remote&&(f=q.remote,delete q.remote,q=b.extend(q,{remote:f})),q
}}}),b.extend(b.expr[":"],{blank:function(d){return !b.trim(""+b(d).val())
},filled:function(f){var d=b(f).val();
    return null!==d&&!!b.trim(""+d)
},unchecked:function(d){return !b(d).prop("checked")
}}),b.validator=function(f,d){this.settings=b.extend(!0,{},b.validator.defaults,f),this.currentForm=d,this.init()
},b.validator.format=function(f,d){return 1===arguments.length?function(){var e=b.makeArray(arguments);
    return e.unshift(f),b.validator.format.apply(this,e)
}:void 0===d?f:(arguments.length>2&&d.constructor!==Array&&(d=b.makeArray(arguments).slice(1)),d.constructor!==Array&&(d=[d]),b.each(d,function(g,e){f=f.replace(new RegExp("\\{"+g+"\\}","g"),function(){return e
})
}),f)
},b.extend(b.validator,{defaults:{messages:{},groups:{},rules:{},errorClass:"error",pendingClass:"pending",validClass:"valid",errorElement:"label",focusCleanup:!1,focusInvalid:!0,errorContainer:b([]),errorLabelContainer:b([]),onsubmit:!0,ignore:":hidden",ignoreTitle:!1,onfocusin:function(d){this.lastActive=d,this.settings.focusCleanup&&(this.settings.unhighlight&&this.settings.unhighlight.call(this,d,this.settings.errorClass,this.settings.validClass),this.hideThese(this.errorsFor(d)))
},onfocusout:function(d){this.checkable(d)||!(d.name in this.submitted)&&this.optional(d)||this.element(d)
},onkeyup:function(g,d){var f=[16,17,18,20,35,36,37,38,39,40,45,144,225];
    9===d.which&&""===this.elementValue(g)||-1!==b.inArray(d.keyCode,f)||(g.name in this.submitted||g.name in this.invalid)&&this.element(g)
},onclick:function(d){d.name in this.submitted?this.element(d):d.parentNode.name in this.submitted&&this.element(d.parentNode)
},highlight:function(g,d,f){"radio"===g.type?this.findByName(g.name).addClass(d).removeClass(f):b(g).addClass(d).removeClass(f)
},unhighlight:function(g,d,f){"radio"===g.type?this.findByName(g.name).removeClass(d).addClass(f):b(g).removeClass(d).addClass(f)
}},setDefaults:function(d){b.extend(b.validator.defaults,d)
},messages:{required:"This field is required.",remote:"Please fix this field.",email:"Please enter a valid email address.",url:"Please enter a valid URL.",date:"Please enter a valid date.",dateISO:"Please enter a valid date ( ISO ).",number:"Please enter a valid number.",digits:"Please enter only digits.",equalTo:"Please enter the same value again.",maxlength:b.validator.format("Please enter no more than {0} characters."),minlength:b.validator.format("Please enter at least {0} characters."),rangelength:b.validator.format("Please enter a value between {0} and {1} characters long."),range:b.validator.format("Please enter a value between {0} and {1}."),max:b.validator.format("Please enter a value less than or equal to {0}."),min:b.validator.format("Please enter a value greater than or equal to {0}."),step:b.validator.format("Please enter a multiple of {0}.")},autoCreateRanges:!1,prototype:{init:function(){function g(l){var h=b.data(this.form,"validator"),j="on"+l.type.replace(/^validate/,""),k=h.settings;
    k[j]&&!b(this).is(k.ignore)&&k[j].call(h,this,l)
}this.labelContainer=b(this.settings.errorLabelContainer),this.errorContext=this.labelContainer.length&&this.labelContainer||b(this.currentForm),this.containers=b(this.settings.errorContainer).add(this.settings.errorLabelContainer),this.submitted={},this.valueCache={},this.pendingRequest=0,this.pending={},this.invalid={},this.reset();
    var d,f=this.groups={};
    b.each(this.settings.groups,function(j,h){"string"==typeof h&&(h=h.split(/\s/)),b.each(h,function(k,e){f[e]=j
    })
    }),d=this.settings.rules,b.each(d,function(j,h){d[j]=b.validator.normalizeRule(h)
    }),b(this.currentForm).on("focusin.validate focusout.validate keyup.validate",":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable]",g).on("click.validate","select, option, [type='radio'], [type='checkbox']",g),this.settings.invalidHandler&&b(this.currentForm).on("invalid-form.validate",this.settings.invalidHandler),b(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required","true")
},form:function(){return this.checkForm(),b.extend(this.submitted,this.errorMap),this.invalid=b.extend({},this.errorMap),this.valid()||b(this.currentForm).triggerHandler("invalid-form",[this]),this.showErrors(),this.valid()
},checkForm:function(){this.prepareForm();
    for(var d=0,f=this.currentElements=this.elements();
        f[d];
        d++){this.check(f[d])
    }return this.valid()
},element:function(j){var f,g,h=this.clean(j),l=this.validationTargetFor(h),d=this,k=!0;
    return void 0===l?delete this.invalid[h.name]:(this.prepareElement(l),this.currentElements=b(l),g=this.groups[l.name],g&&b.each(this.groups,function(m,n){n===g&&m!==l.name&&(h=d.validationTargetFor(d.clean(d.findByName(m))),h&&h.name in d.invalid&&(d.currentElements.push(h),k=k&&d.check(h)))
    }),f=this.check(l)!==!1,k=k&&f,f?this.invalid[l.name]=!1:this.invalid[l.name]=!0,this.numberOfInvalids()||(this.toHide=this.toHide.add(this.containers)),this.showErrors(),b(j).attr("aria-invalid",!f)),k
},showErrors:function(f){if(f){var d=this;
    b.extend(this.errorMap,f),this.errorList=b.map(this.errorMap,function(g,h){return{message:g,element:d.findByName(h)[0]}
    }),this.successList=b.grep(this.successList,function(e){return !(e.name in f)
    })
}this.settings.showErrors?this.settings.showErrors.call(this,this.errorMap,this.errorList):this.defaultShowErrors()
},resetForm:function(){b.fn.resetForm&&b(this.currentForm).resetForm(),this.invalid={},this.submitted={},this.prepareForm(),this.hideErrors();
    var d=this.elements().removeData("previousValue").removeAttr("aria-invalid");
    this.resetElements(d)
},resetElements:function(d){var f;
    if(this.settings.unhighlight){for(f=0;
                                      d[f];
                                      f++){this.settings.unhighlight.call(this,d[f],this.settings.errorClass,""),this.findByName(d[f].name).removeClass(this.settings.validClass)
    }}else{d.removeClass(this.settings.errorClass).removeClass(this.settings.validClass)
    }},numberOfInvalids:function(){return this.objectLength(this.invalid)
},objectLength:function(f){var g,d=0;
    for(g in f){f[g]&&d++
    }return d
},hideErrors:function(){this.hideThese(this.toHide)
},hideThese:function(d){d.not(this.containers).text(""),this.addWrapper(d).hide()
},valid:function(){return 0===this.size()
},size:function(){return this.errorList.length
},focusInvalid:function(){if(this.settings.focusInvalid){try{b(this.findLastActive()||this.errorList.length&&this.errorList[0].element||[]).filter(":visible").focus().trigger("focusin")
}catch(d){}}},findLastActive:function(){var d=this.lastActive;
    return d&&1===b.grep(this.errorList,function(e){return e.element.name===d.name
        }).length&&d
},elements:function(){var f=this,d={};
    return b(this.currentForm).find("input, select, textarea, [contenteditable]").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function(){var e=this.name||b(this).attr("name");
        return !e&&f.settings.debug&&window.console&&console.error("%o has no name assigned",this),this.hasAttribute("contenteditable")&&(this.form=b(this).closest("form")[0]),e in d||!f.objectLength(b(this).rules())?!1:(d[e]=!0,!0)
    })
},clean:function(d){return b(d)[0]
},errors:function(){var d=this.settings.errorClass.split(" ").join(".");
    return b(this.settings.errorElement+"."+d,this.errorContext)
},resetInternals:function(){this.successList=[],this.errorList=[],this.errorMap={},this.toShow=b([]),this.toHide=b([])
},reset:function(){this.resetInternals(),this.currentElements=b([])
},prepareForm:function(){this.reset(),this.toHide=this.errors().add(this.containers)
},prepareElement:function(d){this.reset(),this.toHide=this.errorsFor(d)
},elementValue:function(h){var d,f,g=b(h),j=h.type;
    return"radio"===j||"checkbox"===j?this.findByName(h.name).filter(":checked").val():"number"===j&&"undefined"!=typeof h.validity?h.validity.badInput?"NaN":g.val():(d=h.hasAttribute("contenteditable")?g.text():g.val(),"file"===j?"C:\\fakepath\\"===d.substr(0,12)?d.substr(12):(f=d.lastIndexOf("/"),f>=0?d.substr(f+1):(f=d.lastIndexOf("\\"),f>=0?d.substr(f+1):d)):"string"==typeof d?d.replace(/\r/g,""):d)
},check:function(p){p=this.validationTargetFor(this.clean(p));
    var k,t,d,g=b(p).rules(),q=b.map(g,function(h,l){return l
    }).length,f=!1,j=this.elementValue(p);
    if("function"==typeof g.normalizer){if(j=g.normalizer.call(p,j),"string"!=typeof j){throw new TypeError("The normalizer should return a string value.")
    }delete g.normalizer
    }for(t in g){d={method:t,parameters:g[t]};
        try{if(k=b.validator.methods[t].call(this,j,p,d.parameters),"dependency-mismatch"===k&&1===q){f=!0;
            continue
        }if(f=!1,"pending"===k){return void (this.toHide=this.toHide.not(this.errorsFor(p)))
        }if(!k){return this.formatAndAdd(p,d),!1
        }}catch(m){throw this.settings.debug&&window.console&&console.log("Exception occurred when checking element "+p.id+", check the '"+d.method+"' method.",m),m instanceof TypeError&&(m.message+=".  Exception occurred when checking element "+p.id+", check the '"+d.method+"' method."),m
        }}return f?void 0:(this.objectLength(g)&&this.successList.push(p),!0)
},customDataMessage:function(f,d){return b(f).data("msg"+d.charAt(0).toUpperCase()+d.substring(1).toLowerCase())||b(f).data("msg")
},customMessage:function(f,g){var d=this.settings.messages[f];
    return d&&(d.constructor===String?d:d[g])
},findDefined:function(){for(var d=0;
                             d<arguments.length;
                             d++){if(void 0!==arguments[d]){return arguments[d]
}}},defaultMessage:function(h,d){var f=this.findDefined(this.customMessage(h.name,d.method),this.customDataMessage(h,d.method),!this.settings.ignoreTitle&&h.title||void 0,b.validator.messages[d.method],"<strong>Warning: No message defined for "+h.name+"</strong>"),g=/\$?\{(\d+)\}/g;
    return"function"==typeof f?f=f.call(this,d.parameters,h):g.test(f)&&(f=b.validator.format(f.replace(g,"{$1}"),d.parameters)),f
},formatAndAdd:function(f,g){var d=this.defaultMessage(f,g);
    this.errorList.push({message:d,element:f,method:g.method}),this.errorMap[f.name]=d,this.submitted[f.name]=d
},addWrapper:function(d){return this.settings.wrapper&&(d=d.add(d.parent(this.settings.wrapper))),d
},defaultShowErrors:function(){var f,g,d;
    for(f=0;
        this.errorList[f];
        f++){d=this.errorList[f],this.settings.highlight&&this.settings.highlight.call(this,d.element,this.settings.errorClass,this.settings.validClass),this.showLabel(d.element,d.message)
    }if(this.errorList.length&&(this.toShow=this.toShow.add(this.containers)),this.settings.success){for(f=0;
                                                                                                         this.successList[f];
                                                                                                         f++){this.showLabel(this.successList[f])
    }}if(this.settings.unhighlight){for(f=0,g=this.validElements();
                                        g[f];
                                        f++){this.settings.unhighlight.call(this,g[f],this.settings.errorClass,this.settings.validClass)
    }}this.toHide=this.toHide.not(this.toShow),this.hideErrors(),this.addWrapper(this.toShow).show()
},validElements:function(){return this.currentElements.not(this.invalidElements())
},invalidElements:function(){return b(this.errorList).map(function(){return this.element
})
},showLabel:function(p,k){var t,d,g,q,f=this.errorsFor(p),j=this.idOrName(p),m=b(p).attr("aria-describedby");
    f.length?(f.removeClass(this.settings.validClass).addClass(this.settings.errorClass),f.html(k)):(f=b("<"+this.settings.errorElement+">").attr("id",j+"-error").addClass(this.settings.errorClass).html(k||""),t=f,this.settings.wrapper&&(t=f.hide().show().wrap("<"+this.settings.wrapper+"/>").parent()),this.labelContainer.length?this.labelContainer.append(t):this.settings.errorPlacement?this.settings.errorPlacement(t,b(p)):t.insertAfter(p),f.is("label")?f.attr("for",j):0===f.parents("label[for='"+this.escapeCssMeta(j)+"']").length&&(g=f.attr("id"),m?m.match(new RegExp("\\b"+this.escapeCssMeta(g)+"\\b"))||(m+=" "+g):m=g,b(p).attr("aria-describedby",m),d=this.groups[p.name],d&&(q=this,b.each(q.groups,function(l,h){h===d&&b("[name='"+q.escapeCssMeta(l)+"']",q.currentForm).attr("aria-describedby",f.attr("id"))
    })))),!k&&this.settings.success&&(f.text(""),"string"==typeof this.settings.success?f.addClass(this.settings.success):this.settings.success(f,p)),this.toShow=this.toShow.add(f)
},errorsFor:function(h){var d=this.escapeCssMeta(this.idOrName(h)),f=b(h).attr("aria-describedby"),g="label[for='"+d+"'], label[for='"+d+"'] *";
    return f&&(g=g+", #"+this.escapeCssMeta(f).replace(/\s+/g,", #")),this.errors().filter(g)
},escapeCssMeta:function(d){return d.replace(/([\\!"#$%&'()*+,.\/:;<=>?@\[\]^`{|}~])/g,"\\$1")
},idOrName:function(d){return this.groups[d.name]||(this.checkable(d)?d.name:d.id||d.name)
},validationTargetFor:function(d){return this.checkable(d)&&(d=this.findByName(d.name)),b(d).not(this.settings.ignore)[0]
},checkable:function(d){return/radio|checkbox/i.test(d.type)
},findByName:function(d){return b(this.currentForm).find("[name='"+this.escapeCssMeta(d)+"']")
},getLength:function(f,d){switch(d.nodeName.toLowerCase()){case"select":return b("option:selected",d).length;
    case"input":if(this.checkable(d)){return this.findByName(d.name).filter(":checked").length
    }}return f.length
},depend:function(d,f){return this.dependTypes[typeof d]?this.dependTypes[typeof d](d,f):!0
},dependTypes:{"boolean":function(d){return d
},string:function(f,d){return !!b(f,d.form).length
},"function":function(d,f){return d(f)
}},optional:function(f){var d=this.elementValue(f);
    return !b.validator.methods.required.call(this,d,f)&&"dependency-mismatch"
},startRequest:function(d){this.pending[d.name]||(this.pendingRequest++,b(d).addClass(this.settings.pendingClass),this.pending[d.name]=!0)
},stopRequest:function(f,d){this.pendingRequest--,this.pendingRequest<0&&(this.pendingRequest=0),delete this.pending[f.name],b(f).removeClass(this.settings.pendingClass),d&&0===this.pendingRequest&&this.formSubmitted&&this.form()?(b(this.currentForm).submit(),this.formSubmitted=!1):!d&&0===this.pendingRequest&&this.formSubmitted&&(b(this.currentForm).triggerHandler("invalid-form",[this]),this.formSubmitted=!1)
},previousValue:function(f,d){return b.data(f,"previousValue")||b.data(f,"previousValue",{old:null,valid:!0,message:this.defaultMessage(f,{method:d})})
},destroy:function(){this.resetForm(),b(this.currentForm).off(".validate").removeData("validator").find(".validate-equalTo-blur").off(".validate-equalTo").removeClass("validate-equalTo-blur")
}},classRuleSettings:{required:{required:!0},email:{email:!0},url:{url:!0},date:{date:!0},dateISO:{dateISO:!0},number:{number:!0},digits:{digits:!0},creditcard:{creditcard:!0}},addClassRules:function(f,d){f.constructor===String?this.classRuleSettings[f]=d:b.extend(this.classRuleSettings,f)
},classRules:function(g){var d={},f=b(g).attr("class");
    return f&&b.each(f.split(" "),function(){this in b.validator.classRuleSettings&&b.extend(d,b.validator.classRuleSettings[this])
    }),d
},normalizeAttributeRule:function(f,h,d,g){/min|max|step/.test(d)&&(null===h||/number|range|text/.test(h))&&(g=Number(g),isNaN(g)&&(g=void 0)),g||0===g?f[d]=g:h===d&&"range"!==h&&(f[d]=!0)
},attributeRules:function(j){var f,g,h={},k=b(j),d=j.getAttribute("type");
    for(f in b.validator.methods){"required"===f?(g=j.getAttribute(f),""===g&&(g=!0),g=!!g):g=k.attr(f),this.normalizeAttributeRule(h,d,f,g)
    }return h.maxlength&&/-1|2147483647|524288/.test(h.maxlength)&&delete h.maxlength,h
},dataRules:function(j){var f,g,h={},k=b(j),d=j.getAttribute("type");
    for(f in b.validator.methods){g=k.data("rule"+f.charAt(0).toUpperCase()+f.substring(1).toLowerCase()),this.normalizeAttributeRule(h,d,f,g)
    }return h
},staticRules:function(g){var d={},f=b.data(g.form,"validator");
    return f.settings.rules&&(d=b.validator.normalizeRule(f.settings.rules[g.name])||{}),d
},normalizeRules:function(f,d){return b.each(f,function(e,g){if(g===!1){return void delete f[e]
}if(g.param||g.depends){var h=!0;
    switch(typeof g.depends){case"string":h=!!b(g.depends,d.form).length;
        break;
        case"function":h=g.depends.call(d,d)
    }h?f[e]=void 0!==g.param?g.param:!0:(b.data(d.form,"validator").resetElements(b(d)),delete f[e])
}}),b.each(f,function(e,g){f[e]=b.isFunction(g)&&"normalizer"!==e?g(d):g
}),b.each(["minlength","maxlength"],function(){f[this]&&(f[this]=Number(f[this]))
}),b.each(["rangelength","range"],function(){var e;
    f[this]&&(b.isArray(f[this])?f[this]=[Number(f[this][0]),Number(f[this][1])]:"string"==typeof f[this]&&(e=f[this].replace(/[\[\]]/g,"").split(/[\s,]+/),f[this]=[Number(e[0]),Number(e[1])]))
}),b.validator.autoCreateRanges&&(null!=f.min&&null!=f.max&&(f.range=[f.min,f.max],delete f.min,delete f.max),null!=f.minlength&&null!=f.maxlength&&(f.rangelength=[f.minlength,f.maxlength],delete f.minlength,delete f.maxlength)),f
},normalizeRule:function(f){if("string"==typeof f){var d={};
    b.each(f.split(/\s/),function(){d[this]=!0
    }),f=d
}return f
},addMethod:function(g,d,f){b.validator.methods[g]=d,b.validator.messages[g]=void 0!==f?f:b.validator.messages[g],d.length<3&&b.validator.addClassRules(g,b.validator.normalizeRule(g))
},methods:{required:function(h,d,f){if(!this.depend(f,d)){return"dependency-mismatch"
}if("select"===d.nodeName.toLowerCase()){var g=b(d).val();
    return g&&g.length>0
}return this.checkable(d)?this.getLength(h,d)>0:h.length>0
},email:function(d,f){return this.optional(f)||/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i.test(d)
},url:function(d,f){return this.optional(f)||/^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i.test(d)
},date:function(d,f){return this.optional(f)||!/Invalid|NaN/.test(new Date(d).toString())
},dateISO:function(d,f){return this.optional(f)||/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(d)
},number:function(d,f){return this.optional(f)||/^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(d)
},digits:function(d,f){return this.optional(f)||/^\d+$/.test(d)
},minlength:function(h,d,f){var g=b.isArray(h)?h.length:this.getLength(h,d);
    return this.optional(d)||g>=f
},maxlength:function(h,d,f){var g=b.isArray(h)?h.length:this.getLength(h,d);
    return this.optional(d)||f>=g
},rangelength:function(h,d,f){var g=b.isArray(h)?h.length:this.getLength(h,d);
    return this.optional(d)||g>=f[0]&&g<=f[1]
},min:function(f,g,d){return this.optional(g)||f>=d
},max:function(f,g,d){return this.optional(g)||d>=f
},range:function(f,g,d){return this.optional(g)||f>=d[0]&&f<=d[1]
},step:function(k,g,h){var j=b(g).attr("type"),p="Step attribute on input type "+j+" is not supported.",f=["text","number","range"],m=new RegExp("\\b"+j+"\\b"),d=j&&!m.test(f.join());
    if(d){throw new Error(p)
    }return this.optional(g)||k%h===0
},equalTo:function(h,d,f){var g=b(f);
    return this.settings.onfocusout&&g.not(".validate-equalTo-blur").length&&g.addClass("validate-equalTo-blur").on("blur.validate-equalTo",function(){b(d).valid()
    }),h===g.val()
},remote:function(k,g,h,j){if(this.optional(g)){return"dependency-mismatch"
}j="string"==typeof j&&j||"remote";
    var p,f,m,d=this.previousValue(g,j);
    return this.settings.messages[g.name]||(this.settings.messages[g.name]={}),d.originalMessage=d.originalMessage||this.settings.messages[g.name][j],this.settings.messages[g.name][j]=d.message,h="string"==typeof h&&{url:h}||h,m=b.param(b.extend({data:k},h.data)),d.old===m?d.valid:(d.old=m,p=this,this.startRequest(g),f={},f[g.name]=k,b.ajax(b.extend(!0,{mode:"abort",port:"validate"+g.name,dataType:"json",data:f,context:p.currentForm,success:function(l){var q,e,r,n=l===!0||"true"===l;
        p.settings.messages[g.name][j]=d.originalMessage,n?(r=p.formSubmitted,p.resetInternals(),p.toHide=p.errorsFor(g),p.formSubmitted=r,p.successList.push(g),p.invalid[g.name]=!1,p.showErrors()):(q={},e=l||p.defaultMessage(g,{method:j,parameters:k}),q[g.name]=d.message=e,p.invalid[g.name]=!0,p.showErrors(q)),d.valid=n,p.stopRequest(g,n)
    }},h)),"pending")
}}});
    var c,a={};
    b.ajaxPrefilter?b.ajaxPrefilter(function(d,h,f){var g=d.port;
        "abort"===d.mode&&(a[g]&&a[g].abort(),a[g]=f)
    }):(c=b.ajax,b.ajax=function(d){var e=("mode" in d?d:b.ajaxSettings).mode,f=("port" in d?d:b.ajaxSettings).port;
        return"abort"===e?(a[f]&&a[f].abort(),a[f]=c.apply(this,arguments),a[f]):c.apply(this,arguments)
    })
});
jQuery.validator.addMethod("phoneUS",function(a,b){a=a.replace(/\s+/g,"");
    return this.optional(b)||a.length>9&&a.match(/^[0-9]{1,100}$/)
},"Please specify a valid phone number");
/*!
 * Bootstrap v3.3.6 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under the MIT license
 */
;
if(typeof jQuery==="undefined"){throw new Error("Bootstrap's JavaScript requires jQuery")
}+function(b){var a=b.fn.jquery.split(" ")[0].split(".");
    if((a[0]<2&&a[1]<9)||(a[0]==1&&a[1]==9&&a[2]<1)||(a[0]>2)){throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3")
    }}(jQuery);
+function(b){function a(){var e=document.createElement("bootstrap");
    var d={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};
    for(var c in d){if(e.style[c]!==undefined){return{end:d[c]}
    }}return false
}b.fn.emulateTransitionEnd=function(e){var d=false;
    var c=this;
    b(this).one("bsTransitionEnd",function(){d=true
    });
    var f=function(){if(!d){b(c).trigger(b.support.transition.end)
    }};
    setTimeout(f,e);
    return this
};
    b(function(){b.support.transition=a();
        if(!b.support.transition){return
        }b.event.special.bsTransitionEnd={bindType:b.support.transition.end,delegateType:b.support.transition.end,handle:function(c){if(b(c.target).is(this)){return c.handleObj.handler.apply(this,arguments)
        }}}
    })
}(jQuery);
+function(e){var d='[data-dismiss="alert"]';
    var b=function(f){e(f).on("click",d,this.close)
    };
    b.VERSION="3.3.6";
    b.TRANSITION_DURATION=150;
    b.prototype.close=function(k){var j=e(this);
        var g=j.attr("data-target");
        if(!g){g=j.attr("href");
            g=g&&g.replace(/.*(?=#[^\s]*$)/,"")
        }var h=e(g);
        if(k){k.preventDefault()
        }if(!h.length){h=j.closest(".alert")
        }h.trigger(k=e.Event("close.bs.alert"));
        if(k.isDefaultPrevented()){return
        }h.removeClass("in");
        function f(){h.detach().trigger("closed.bs.alert").remove()
        }e.support.transition&&h.hasClass("fade")?h.one("bsTransitionEnd",f).emulateTransitionEnd(b.TRANSITION_DURATION):f()
    };
    function c(f){return this.each(function(){var h=e(this);
        var g=h.data("bs.alert");
        if(!g){h.data("bs.alert",(g=new b(this)))
        }if(typeof f=="string"){g[f].call(h)
        }})
    }var a=e.fn.alert;
    e.fn.alert=c;
    e.fn.alert.Constructor=b;
    e.fn.alert.noConflict=function(){e.fn.alert=a;
        return this
    };
    e(document).on("click.bs.alert.data-api",d,b.prototype.close)
}(jQuery);
+function(d){var b=function(f,e){this.$element=d(f);
    this.options=d.extend({},b.DEFAULTS,e);
    this.isLoading=false
};
    b.VERSION="3.3.6";
    b.DEFAULTS={loadingText:"loading..."};
    b.prototype.setState=function(g){var j="disabled";
        var e=this.$element;
        var h=e.is("input")?"val":"html";
        var f=e.data();
        g+="Text";
        if(f.resetText==null){e.data("resetText",e[h]())
        }setTimeout(d.proxy(function(){e[h](f[g]==null?this.options[g]:f[g]);
            if(g=="loadingText"){this.isLoading=true;
                e.addClass(j).attr(j,j)
            }else{if(this.isLoading){this.isLoading=false;
                e.removeClass(j).removeAttr(j)
            }}},this),0)
    };
    b.prototype.toggle=function(){var f=true;
        var e=this.$element.closest('[data-toggle="buttons"]');
        if(e.length){var g=this.$element.find("input");
            if(g.prop("type")=="radio"){if(g.prop("checked")){f=false
            }e.find(".active").removeClass("active");
                this.$element.addClass("active")
            }else{if(g.prop("type")=="checkbox"){if((g.prop("checked"))!==this.$element.hasClass("active")){f=false
            }this.$element.toggleClass("active")
            }}g.prop("checked",this.$element.hasClass("active"));
            if(f){g.trigger("change")
            }}else{this.$element.attr("aria-pressed",!this.$element.hasClass("active"));
            this.$element.toggleClass("active")
        }};
    function c(e){return this.each(function(){var h=d(this);
        var g=h.data("bs.button");
        var f=typeof e=="object"&&e;
        if(!g){h.data("bs.button",(g=new b(this,f)))
        }if(e=="toggle"){g.toggle()
        }else{if(e){g.setState(e)
        }}})
    }var a=d.fn.button;
    d.fn.button=c;
    d.fn.button.Constructor=b;
    d.fn.button.noConflict=function(){d.fn.button=a;
        return this
    };
    d(document).on("click.bs.button.data-api",'[data-toggle^="button"]',function(g){var f=d(g.target);
        if(!f.hasClass("btn")){f=f.closest(".btn")
        }c.call(f,"toggle");
        if(!(d(g.target).is('input[type="radio"]')||d(g.target).is('input[type="checkbox"]'))){g.preventDefault()
        }}).on("focus.bs.button.data-api blur.bs.button.data-api",'[data-toggle^="button"]',function(f){d(f.target).closest(".btn").toggleClass("focus",/^focus(in)?$/.test(f.type))
    })
}(jQuery);
+function(c){var d=function(g,f){this.$element=c(g);
    this.$indicators=this.$element.find(".carousel-indicators");
    this.options=f;
    this.paused=null;
    this.sliding=null;
    this.interval=null;
    this.$active=null;
    this.$items=null;
    this.options.keyboard&&this.$element.on("keydown.bs.carousel",c.proxy(this.keydown,this));
    this.options.pause=="hover"&&!("ontouchstart" in document.documentElement)&&this.$element.on("mouseenter.bs.carousel",c.proxy(this.pause,this)).on("mouseleave.bs.carousel",c.proxy(this.cycle,this))
};
    d.VERSION="3.3.6";
    d.TRANSITION_DURATION=600;
    d.DEFAULTS={interval:5000,pause:"hover",wrap:true,keyboard:true};
    d.prototype.keydown=function(f){if(/input|textarea/i.test(f.target.tagName)){return
    }switch(f.which){case 37:this.prev();
        break;
        case 39:this.next();
            break;
        default:return
    }f.preventDefault()
    };
    d.prototype.cycle=function(f){f||(this.paused=false);
        this.interval&&clearInterval(this.interval);
        this.options.interval&&!this.paused&&(this.interval=setInterval(c.proxy(this.next,this),this.options.interval));
        return this
    };
    d.prototype.getItemIndex=function(f){this.$items=f.parent().children(".item");
        return this.$items.index(f||this.$active)
    };
    d.prototype.getItemForDirection=function(k,j){var f=this.getItemIndex(j);
        var g=(k=="prev"&&f===0)||(k=="next"&&f==(this.$items.length-1));
        if(g&&!this.options.wrap){return j
        }var l=k=="prev"?-1:1;
        var h=(f+l)%this.$items.length;
        return this.$items.eq(h)
    };
    d.prototype.to=function(h){var g=this;
        var f=this.getItemIndex(this.$active=this.$element.find(".item.active"));
        if(h>(this.$items.length-1)||h<0){return
        }if(this.sliding){return this.$element.one("slid.bs.carousel",function(){g.to(h)
        })
        }if(f==h){return this.pause().cycle()
        }return this.slide(h>f?"next":"prev",this.$items.eq(h))
    };
    d.prototype.pause=function(f){f||(this.paused=true);
        if(this.$element.find(".next, .prev").length&&c.support.transition){this.$element.trigger(c.support.transition.end);
            this.cycle(true)
        }this.interval=clearInterval(this.interval);
        return this
    };
    d.prototype.next=function(){if(this.sliding){return
    }return this.slide("next")
    };
    d.prototype.prev=function(){if(this.sliding){return
    }return this.slide("prev")
    };
    d.prototype.slide=function(n,j){var q=this.$element.find(".item.active");
        var g=j||this.getItemForDirection(n,q);
        var l=this.interval;
        var o=n=="next"?"left":"right";
        var k=this;
        if(g.hasClass("active")){return(this.sliding=false)
        }var m=g[0];
        var f=c.Event("slide.bs.carousel",{relatedTarget:m,direction:o});
        this.$element.trigger(f);
        if(f.isDefaultPrevented()){return
        }this.sliding=true;
        l&&this.pause();
        if(this.$indicators.length){this.$indicators.find(".active").removeClass("active");
            var h=c(this.$indicators.children()[this.getItemIndex(g)]);
            h&&h.addClass("active")
        }var p=c.Event("slid.bs.carousel",{relatedTarget:m,direction:o});
        if(c.support.transition&&this.$element.hasClass("slide")){g.addClass(n);
            g[0].offsetWidth;
            q.addClass(o);
            g.addClass(o);
            q.one("bsTransitionEnd",function(){g.removeClass([n,o].join(" ")).addClass("active");
                q.removeClass(["active",o].join(" "));
                k.sliding=false;
                setTimeout(function(){k.$element.trigger(p)
                },0)
            }).emulateTransitionEnd(d.TRANSITION_DURATION)
        }else{q.removeClass("active");
            g.addClass("active");
            this.sliding=false;
            this.$element.trigger(p)
        }l&&this.cycle();
        return this
    };
    function b(f){return this.each(function(){var k=c(this);
        var j=k.data("bs.carousel");
        var g=c.extend({},d.DEFAULTS,k.data(),typeof f=="object"&&f);
        var h=typeof f=="string"?f:g.slide;
        if(!j){k.data("bs.carousel",(j=new d(this,g)))
        }if(typeof f=="number"){j.to(f)
        }else{if(h){j[h]()
        }else{if(g.interval){j.pause().cycle()
        }}}})
    }var a=c.fn.carousel;
    c.fn.carousel=b;
    c.fn.carousel.Constructor=d;
    c.fn.carousel.noConflict=function(){c.fn.carousel=a;
        return this
    };
    var e=function(l){var g;
        var k=c(this);
        var f=c(k.attr("data-target")||(g=k.attr("href"))&&g.replace(/.*(?=#[^\s]+$)/,""));
        if(!f.hasClass("carousel")){return
        }var h=c.extend({},f.data(),k.data());
        var j=k.attr("data-slide-to");
        if(j){h.interval=false
        }b.call(f,h);
        if(j){f.data("bs.carousel").to(j)
        }l.preventDefault()
    };
    c(document).on("click.bs.carousel.data-api","[data-slide]",e).on("click.bs.carousel.data-api","[data-slide-to]",e);
    c(window).on("load",function(){c('[data-ride="carousel"]').each(function(){var f=c(this);
        b.call(f,f.data())
    })
    })
}(jQuery);
+function(d){var e=function(g,f){this.$element=d(g);
    this.options=d.extend({},e.DEFAULTS,f);
    this.$trigger=d('[data-toggle="collapse"][href="#'+g.id+'"],[data-toggle="collapse"][data-target="#'+g.id+'"]');
    this.transitioning=null;
    if(this.options.parent){this.$parent=this.getParent()
    }else{this.addAriaAndCollapsedClass(this.$element,this.$trigger)
    }if(this.options.toggle){this.toggle()
    }};
    e.VERSION="3.3.6";
    e.TRANSITION_DURATION=350;
    e.DEFAULTS={toggle:true};
    e.prototype.dimension=function(){var f=this.$element.hasClass("width");
        return f?"width":"height"
    };
    e.prototype.show=function(){if(this.transitioning||this.$element.hasClass("in")){return
    }var h;
        var k=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing");
        if(k&&k.length){h=k.data("bs.collapse");
            if(h&&h.transitioning){return
            }}var g=d.Event("show.bs.collapse");
        this.$element.trigger(g);
        if(g.isDefaultPrevented()){return
        }if(k&&k.length){b.call(k,"hide");
            h||k.data("bs.collapse",null)
        }var l=this.dimension();
        this.$element.removeClass("collapse").addClass("collapsing")[l](0).attr("aria-expanded",true);
        this.$trigger.removeClass("collapsed").attr("aria-expanded",true);
        this.transitioning=1;
        var f=function(){this.$element.removeClass("collapsing").addClass("collapse in")[l]("");
            this.transitioning=0;
            this.$element.trigger("shown.bs.collapse")
        };
        if(!d.support.transition){return f.call(this)
        }var j=d.camelCase(["scroll",l].join("-"));
        this.$element.one("bsTransitionEnd",d.proxy(f,this)).emulateTransitionEnd(e.TRANSITION_DURATION)[l](this.$element[0][j])
    };
    e.prototype.hide=function(){if(this.transitioning||!this.$element.hasClass("in")){return
    }var g=d.Event("hide.bs.collapse");
        this.$element.trigger(g);
        if(g.isDefaultPrevented()){return
        }var h=this.dimension();
        this.$element[h](this.$element[h]())[0].offsetHeight;
        this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",false);
        this.$trigger.addClass("collapsed").attr("aria-expanded",false);
        this.transitioning=1;
        var f=function(){this.transitioning=0;
            this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
        };
        if(!d.support.transition){return f.call(this)
        }this.$element[h](0).one("bsTransitionEnd",d.proxy(f,this)).emulateTransitionEnd(e.TRANSITION_DURATION)
    };
    e.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()
    };
    e.prototype.getParent=function(){return d(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each(d.proxy(function(h,g){var f=d(g);
        this.addAriaAndCollapsedClass(c(f),f)
    },this)).end()
    };
    e.prototype.addAriaAndCollapsedClass=function(g,f){var h=g.hasClass("in");
        g.attr("aria-expanded",h);
        f.toggleClass("collapsed",!h).attr("aria-expanded",h)
    };
    function c(f){var g;
        var h=f.attr("data-target")||(g=f.attr("href"))&&g.replace(/.*(?=#[^\s]+$)/,"");
        return d(h)
    }function b(f){return this.each(function(){var j=d(this);
        var h=j.data("bs.collapse");
        var g=d.extend({},e.DEFAULTS,j.data(),typeof f=="object"&&f);
        if(!h&&g.toggle&&/show|hide/.test(f)){g.toggle=false
        }if(!h){j.data("bs.collapse",(h=new e(this,g)))
        }if(typeof f=="string"){h[f]()
        }})
    }var a=d.fn.collapse;
    d.fn.collapse=b;
    d.fn.collapse.Constructor=e;
    d.fn.collapse.noConflict=function(){d.fn.collapse=a;
        return this
    };
    d(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(k){var j=d(this);
        if(!j.attr("data-target")){k.preventDefault()
        }var f=c(j);
        var h=f.data("bs.collapse");
        var g=h?"toggle":j.data();
        b.call(f,g)
    })
}(jQuery);
+function(h){var e=".dropdown-backdrop";
    var b='[data-toggle="dropdown"]';
    var a=function(j){h(j).on("click.bs.dropdown",this.toggle)
    };
    a.VERSION="3.3.6";
    function f(l){var j=l.attr("data-target");
        if(!j){j=l.attr("href");
            j=j&&/#[A-Za-z]/.test(j)&&j.replace(/.*(?=#[^\s]*$)/,"")
        }var k=j&&h(j);
        return k&&k.length?k:l.parent()
    }function d(j){if(j&&j.which===3){return
    }h(e).remove();
        h(b).each(function(){var m=h(this);
            var l=f(m);
            var k={relatedTarget:this};
            if(!l.hasClass("open")){return
            }if(j&&j.type=="click"&&/input|textarea/i.test(j.target.tagName)&&h.contains(l[0],j.target)){return
            }l.trigger(j=h.Event("hide.bs.dropdown",k));
            if(j.isDefaultPrevented()){return
            }m.attr("aria-expanded","false");
            l.removeClass("open").trigger(h.Event("hidden.bs.dropdown",k))
        })
    }a.prototype.toggle=function(n){var m=h(this);
        if(m.is(".disabled, :disabled")){return
        }var l=f(m);
        var k=l.hasClass("open");
        d();
        if(!k){if("ontouchstart" in document.documentElement&&!l.closest(".navbar-nav").length){h(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(h(this)).on("click",d)
        }var j={relatedTarget:this};
            l.trigger(n=h.Event("show.bs.dropdown",j));
            if(n.isDefaultPrevented()){return
            }m.trigger("focus").attr("aria-expanded","true");
            l.toggleClass("open").trigger(h.Event("shown.bs.dropdown",j))
        }return false
    };
    a.prototype.keydown=function(n){if(!/(38|40|27|32)/.test(n.which)||/input|textarea/i.test(n.target.tagName)){return
    }var m=h(this);
        n.preventDefault();
        n.stopPropagation();
        if(m.is(".disabled, :disabled")){return
        }var l=f(m);
        var k=l.hasClass("open");
        if(!k&&n.which!=27||k&&n.which==27){if(n.which==27){l.find(b).trigger("focus")
        }return m.trigger("click")
        }var o=" li:not(.disabled):visible a";
        var p=l.find(".dropdown-menu"+o);
        if(!p.length){return
        }var j=p.index(n.target);
        if(n.which==38&&j>0){j--
        }if(n.which==40&&j<p.length-1){j++
        }if(!~j){j=0
        }p.eq(j).trigger("focus")
    };
    function g(j){return this.each(function(){var l=h(this);
        var k=l.data("bs.dropdown");
        if(!k){l.data("bs.dropdown",(k=new a(this)))
        }if(typeof j=="string"){k[j].call(l)
        }})
    }var c=h.fn.dropdown;
    h.fn.dropdown=g;
    h.fn.dropdown.Constructor=a;
    h.fn.dropdown.noConflict=function(){h.fn.dropdown=c;
        return this
    };
    h(document).on("click.bs.dropdown.data-api",d).on("click.bs.dropdown.data-api",".dropdown form",function(j){j.stopPropagation()
    }).on("click.bs.dropdown.data-api",b,a.prototype.toggle).on("keydown.bs.dropdown.data-api",b,a.prototype.keydown).on("keydown.bs.dropdown.data-api",".dropdown-menu",a.prototype.keydown)
}(jQuery);
+function(d){var b=function(f,e){this.options=e;
    this.$body=d(document.body);
    this.$element=d(f);
    this.$dialog=this.$element.find(".modal-dialog");
    this.$backdrop=null;
    this.isShown=null;
    this.originalBodyPad=null;
    this.scrollbarWidth=0;
    this.ignoreBackdropClick=false;
    if(this.options.remote){this.$element.find(".modal-content").load(this.options.remote,d.proxy(function(){this.$element.trigger("loaded.bs.modal")
    },this))
    }};
    b.VERSION="3.3.6";
    b.TRANSITION_DURATION=300;
    b.BACKDROP_TRANSITION_DURATION=150;
    b.DEFAULTS={backdrop:true,keyboard:true,show:true};
    b.prototype.toggle=function(e){return this.isShown?this.hide():this.show(e)
    };
    b.prototype.show=function(h){var f=this;
        var g=d.Event("show.bs.modal",{relatedTarget:h});
        this.$element.trigger(g);
        if(this.isShown||g.isDefaultPrevented()){return
        }this.isShown=true;
        this.checkScrollbar();
        this.setScrollbar();
        this.$body.addClass("modal-open");
        this.escape();
        this.resize();
        this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',d.proxy(this.hide,this));
        this.$dialog.on("mousedown.dismiss.bs.modal",function(){f.$element.one("mouseup.dismiss.bs.modal",function(j){if(d(j.target).is(f.$element)){f.ignoreBackdropClick=true
        }})
        });
        this.backdrop(function(){var k=d.support.transition&&f.$element.hasClass("fade");
            if(!f.$element.parent().length){f.$element.appendTo(f.$body)
            }f.$element.show().scrollTop(0);
            f.adjustDialog();
            if(k){f.$element[0].offsetWidth
            }f.$element.addClass("in");
            f.enforceFocus();
            var j=d.Event("shown.bs.modal",{relatedTarget:h});
            k?f.$dialog.one("bsTransitionEnd",function(){f.$element.trigger("focus").trigger(j)
            }).emulateTransitionEnd(b.TRANSITION_DURATION):f.$element.trigger("focus").trigger(j)
        })
    };
    b.prototype.hide=function(f){if(f){f.preventDefault()
    }f=d.Event("hide.bs.modal");
        this.$element.trigger(f);
        if(!this.isShown||f.isDefaultPrevented()){return
        }this.isShown=false;
        this.escape();
        this.resize();
        d(document).off("focusin.bs.modal");
        this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal");
        this.$dialog.off("mousedown.dismiss.bs.modal");
        d.support.transition&&this.$element.hasClass("fade")?this.$element.one("bsTransitionEnd",d.proxy(this.hideModal,this)).emulateTransitionEnd(b.TRANSITION_DURATION):this.hideModal()
    };
    b.prototype.enforceFocus=function(){d(document).off("focusin.bs.modal").on("focusin.bs.modal",d.proxy(function(f){if(this.$element[0]!==f.target&&!this.$element.has(f.target).length){this.$element.trigger("focus")
    }},this))
    };
    b.prototype.escape=function(){if(this.isShown&&this.options.keyboard){this.$element.on("keydown.dismiss.bs.modal",d.proxy(function(f){f.which==27&&this.hide()
    },this))
    }else{if(!this.isShown){this.$element.off("keydown.dismiss.bs.modal")
    }}};
    b.prototype.resize=function(){if(this.isShown){d(window).on("resize.bs.modal",d.proxy(this.handleUpdate,this))
    }else{d(window).off("resize.bs.modal")
    }};
    b.prototype.hideModal=function(){var e=this;
        this.$element.hide();
        this.backdrop(function(){e.$body.removeClass("modal-open");
            e.resetAdjustments();
            e.resetScrollbar();
            e.$element.trigger("hidden.bs.modal")
        })
    };
    b.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove();
        this.$backdrop=null
    };
    b.prototype.backdrop=function(j){var h=this;
        var f=this.$element.hasClass("fade")?"fade":"";
        if(this.isShown&&this.options.backdrop){var e=d.support.transition&&f;
            this.$backdrop=d(document.createElement("div")).addClass("modal-backdrop "+f).appendTo(this.$body);
            this.$element.on("click.dismiss.bs.modal",d.proxy(function(k){if(this.ignoreBackdropClick){this.ignoreBackdropClick=false;
                return
            }if(k.target!==k.currentTarget){return
            }this.options.backdrop=="static"?this.$element[0].focus():this.hide()
            },this));
            if(e){this.$backdrop[0].offsetWidth
            }this.$backdrop.addClass("in");
            if(!j){return
            }e?this.$backdrop.one("bsTransitionEnd",j).emulateTransitionEnd(b.BACKDROP_TRANSITION_DURATION):j()
        }else{if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass("in");
            var g=function(){h.removeBackdrop();
                j&&j()
            };
            d.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one("bsTransitionEnd",g).emulateTransitionEnd(b.BACKDROP_TRANSITION_DURATION):g()
        }else{if(j){j()
        }}}};
    b.prototype.handleUpdate=function(){this.adjustDialog()
    };
    b.prototype.adjustDialog=function(){var e=this.$element[0].scrollHeight>document.documentElement.clientHeight;
        this.$element.css({paddingLeft:!this.bodyIsOverflowing&&e?this.scrollbarWidth:"",paddingRight:this.bodyIsOverflowing&&!e?this.scrollbarWidth:""})
    };
    b.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:"",paddingRight:""})
    };
    b.prototype.checkScrollbar=function(){var f=window.innerWidth;
        if(!f){var e=document.documentElement.getBoundingClientRect();
            f=e.right-Math.abs(e.left)
        }this.bodyIsOverflowing=document.body.clientWidth<f;
        this.scrollbarWidth=this.measureScrollbar()
    };
    b.prototype.setScrollbar=function(){var e=parseInt((this.$body.css("padding-right")||0),10);
        this.originalBodyPad=document.body.style.paddingRight||"";
        if(this.bodyIsOverflowing){this.$body.css("padding-right",e+this.scrollbarWidth)
        }};
    b.prototype.resetScrollbar=function(){this.$body.css("padding-right",this.originalBodyPad)
    };
    b.prototype.measureScrollbar=function(){var f=document.createElement("div");
        f.className="modal-scrollbar-measure";
        this.$body.append(f);
        var e=f.offsetWidth-f.clientWidth;
        this.$body[0].removeChild(f);
        return e
    };
    function c(e,f){return this.each(function(){var j=d(this);
        var h=j.data("bs.modal");
        var g=d.extend({},b.DEFAULTS,j.data(),typeof e=="object"&&e);
        if(!h){j.data("bs.modal",(h=new b(this,g)))
        }if(typeof e=="string"){h[e](f)
        }else{if(g.show){h.show(f)
        }}})
    }var a=d.fn.modal;
    d.fn.modal=c;
    d.fn.modal.Constructor=b;
    d.fn.modal.noConflict=function(){d.fn.modal=a;
        return this
    };
    d(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(k){var j=d(this);
        var g=j.attr("href");
        var f=d(j.attr("data-target")||(g&&g.replace(/.*(?=#[^\s]+$)/,"")));
        var h=f.data("bs.modal")?"toggle":d.extend({remote:!/#/.test(g)&&g},f.data(),j.data());
        if(j.is("a")){k.preventDefault()
        }f.one("show.bs.modal",function(e){if(e.isDefaultPrevented()){return
        }f.one("hidden.bs.modal",function(){j.is(":visible")&&j.trigger("focus")
        })
        });
        c.call(f,h,this)
    })
}(jQuery);
+function(d){var c=function(f,e){this.type=null;
    this.options=null;
    this.enabled=null;
    this.timeout=null;
    this.hoverState=null;
    this.$element=null;
    this.inState=null;
    this.init("tooltip",f,e)
};
    c.VERSION="3.3.6";
    c.TRANSITION_DURATION=150;
    c.DEFAULTS={animation:true,placement:"top",selector:false,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:false,container:false,viewport:{selector:"body",padding:0}};
    c.prototype.init=function(l,j,g){this.enabled=true;
        this.type=l;
        this.$element=d(j);
        this.options=this.getOptions(g);
        this.$viewport=this.options.viewport&&d(d.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):(this.options.viewport.selector||this.options.viewport));
        this.inState={click:false,hover:false,focus:false};
        if(this.$element[0] instanceof document.constructor&&!this.options.selector){throw new Error("`selector` option must be specified when initializing "+this.type+" on the window.document object!")
        }var k=this.options.trigger.split(" ");
        for(var h=k.length;
            h--;
        ){var f=k[h];
            if(f=="click"){this.$element.on("click."+this.type,this.options.selector,d.proxy(this.toggle,this))
            }else{if(f!="manual"){var m=f=="hover"?"mouseenter":"focusin";
                var e=f=="hover"?"mouseleave":"focusout";
                this.$element.on(m+"."+this.type,this.options.selector,d.proxy(this.enter,this));
                this.$element.on(e+"."+this.type,this.options.selector,d.proxy(this.leave,this))
            }}}this.options.selector?(this._options=d.extend({},this.options,{trigger:"manual",selector:""})):this.fixTitle()
    };
    c.prototype.getDefaults=function(){return c.DEFAULTS
    };
    c.prototype.getOptions=function(e){e=d.extend({},this.getDefaults(),this.$element.data(),e);
        if(e.delay&&typeof e.delay=="number"){e.delay={show:e.delay,hide:e.delay}
        }return e
    };
    c.prototype.getDelegateOptions=function(){var e={};
        var f=this.getDefaults();
        this._options&&d.each(this._options,function(g,h){if(f[g]!=h){e[g]=h
        }});
        return e
    };
    c.prototype.enter=function(f){var e=f instanceof this.constructor?f:d(f.currentTarget).data("bs."+this.type);
        if(!e){e=new this.constructor(f.currentTarget,this.getDelegateOptions());
            d(f.currentTarget).data("bs."+this.type,e)
        }if(f instanceof d.Event){e.inState[f.type=="focusin"?"focus":"hover"]=true
        }if(e.tip().hasClass("in")||e.hoverState=="in"){e.hoverState="in";
            return
        }clearTimeout(e.timeout);
        e.hoverState="in";
        if(!e.options.delay||!e.options.delay.show){return e.show()
        }e.timeout=setTimeout(function(){if(e.hoverState=="in"){e.show()
        }},e.options.delay.show)
    };
    c.prototype.isInStateTrue=function(){for(var e in this.inState){if(this.inState[e]){return true
    }}return false
    };
    c.prototype.leave=function(f){var e=f instanceof this.constructor?f:d(f.currentTarget).data("bs."+this.type);
        if(!e){e=new this.constructor(f.currentTarget,this.getDelegateOptions());
            d(f.currentTarget).data("bs."+this.type,e)
        }if(f instanceof d.Event){e.inState[f.type=="focusout"?"focus":"hover"]=false
        }if(e.isInStateTrue()){return
        }clearTimeout(e.timeout);
        e.hoverState="out";
        if(!e.options.delay||!e.options.delay.hide){return e.hide()
        }e.timeout=setTimeout(function(){if(e.hoverState=="out"){e.hide()
        }},e.options.delay.hide)
    };
    c.prototype.show=function(){var p=d.Event("show.bs."+this.type);
        if(this.hasContent()&&this.enabled){this.$element.trigger(p);
            var q=d.contains(this.$element[0].ownerDocument.documentElement,this.$element[0]);
            if(p.isDefaultPrevented()||!q){return
            }var o=this;
            var m=this.tip();
            var h=this.getUID(this.type);
            this.setContent();
            m.attr("id",h);
            this.$element.attr("aria-describedby",h);
            if(this.options.animation){m.addClass("fade")
            }var l=typeof this.options.placement=="function"?this.options.placement.call(this,m[0],this.$element[0]):this.options.placement;
            var t=/\s?auto?\s?/i;
            var u=t.test(l);
            if(u){l=l.replace(t,"")||"top"
            }m.detach().css({top:0,left:0,display:"block"}).addClass(l).data("bs."+this.type,this);
            this.options.container?m.appendTo(this.options.container):m.insertAfter(this.$element);
            this.$element.trigger("inserted.bs."+this.type);
            var r=this.getPosition();
            var f=m[0].offsetWidth;
            var n=m[0].offsetHeight;
            if(u){var k=l;
                var s=this.getPosition(this.$viewport);
                l=l=="bottom"&&r.bottom+n>s.bottom?"top":l=="top"&&r.top-n<s.top?"bottom":l=="right"&&r.right+f>s.width?"left":l=="left"&&r.left-f<s.left?"right":l;
                m.removeClass(k).addClass(l)
            }var j=this.getCalculatedOffset(l,r,f,n);
            this.applyPlacement(j,l);
            var g=function(){var e=o.hoverState;
                o.$element.trigger("shown.bs."+o.type);
                o.hoverState=null;
                if(e=="out"){o.leave(o)
                }};
            d.support.transition&&this.$tip.hasClass("fade")?m.one("bsTransitionEnd",g).emulateTransitionEnd(c.TRANSITION_DURATION):g()
        }};
    c.prototype.applyPlacement=function(k,l){var m=this.tip();
        var g=m[0].offsetWidth;
        var r=m[0].offsetHeight;
        var f=parseInt(m.css("margin-top"),10);
        var j=parseInt(m.css("margin-left"),10);
        if(isNaN(f)){f=0
        }if(isNaN(j)){j=0
        }k.top+=f;
        k.left+=j;
        d.offset.setOffset(m[0],d.extend({using:function(s){m.css({top:Math.round(s.top),left:Math.round(s.left)})
        }},k),0);
        m.addClass("in");
        var e=m[0].offsetWidth;
        var n=m[0].offsetHeight;
        if(l=="top"&&n!=r){k.top=k.top+r-n
        }var q=this.getViewportAdjustedDelta(l,k,e,n);
        if(q.left){k.left+=q.left
        }else{k.top+=q.top
        }var o=/top|bottom/.test(l);
        var h=o?q.left*2-g+e:q.top*2-r+n;
        var p=o?"offsetWidth":"offsetHeight";
        m.offset(k);
        this.replaceArrow(h,m[0][p],o)
    };
    c.prototype.replaceArrow=function(g,e,f){this.arrow().css(f?"left":"top",50*(1-g/e)+"%").css(f?"top":"left","")
    };
    c.prototype.setContent=function(){var f=this.tip();
        var e=this.getTitle();
        f.find(".tooltip-inner")[this.options.html?"html":"text"](e);
        f.removeClass("fade in top bottom left right")
    };
    c.prototype.hide=function(k){var g=this;
        var j=d(this.$tip);
        var h=d.Event("hide.bs."+this.type);
        function f(){if(g.hoverState!="in"){j.detach()
        }g.$element.removeAttr("aria-describedby").trigger("hidden.bs."+g.type);
            k&&k()
        }this.$element.trigger(h);
        if(h.isDefaultPrevented()){return
        }j.removeClass("in");
        d.support.transition&&j.hasClass("fade")?j.one("bsTransitionEnd",f).emulateTransitionEnd(c.TRANSITION_DURATION):f();
        this.hoverState=null;
        return this
    };
    c.prototype.fixTitle=function(){var e=this.$element;
        if(e.attr("title")||typeof e.attr("data-original-title")!="string"){e.attr("data-original-title",e.attr("title")||"").attr("title","")
        }};
    c.prototype.hasContent=function(){return this.getTitle()
    };
    c.prototype.getPosition=function(g){g=g||this.$element;
        var j=g[0];
        var f=j.tagName=="BODY";
        var h=j.getBoundingClientRect();
        if(h.width==null){h=d.extend({},h,{width:h.right-h.left,height:h.bottom-h.top})
        }var l=f?{top:0,left:0}:g.offset();
        var e={scroll:f?document.documentElement.scrollTop||document.body.scrollTop:g.scrollTop()};
        var k=f?{width:d(window).width(),height:d(window).height()}:null;
        return d.extend({},h,e,k,l)
    };
    c.prototype.getCalculatedOffset=function(e,h,f,g){return e=="bottom"?{top:h.top+h.height,left:h.left+h.width/2-f/2}:e=="top"?{top:h.top-g,left:h.left+h.width/2-f/2}:e=="left"?{top:h.top+h.height/2-g/2,left:h.left-f}:{top:h.top+h.height/2-g/2,left:h.left+h.width}
    };
    c.prototype.getViewportAdjustedDelta=function(h,l,e,k){var n={top:0,left:0};
        if(!this.$viewport){return n
        }var g=this.options.viewport&&this.options.viewport.padding||0;
        var m=this.getPosition(this.$viewport);
        if(/right|left/.test(h)){var o=l.top-g-m.scroll;
            var j=l.top+g-m.scroll+k;
            if(o<m.top){n.top=m.top-o
            }else{if(j>m.top+m.height){n.top=m.top+m.height-j
            }}}else{var p=l.left-g;
            var f=l.left+g+e;
            if(p<m.left){n.left=m.left-p
            }else{if(f>m.right){n.left=m.left+m.width-f
            }}}return n
    };
    c.prototype.getTitle=function(){var g;
        var e=this.$element;
        var f=this.options;
        g=e.attr("data-original-title")||(typeof f.title=="function"?f.title.call(e[0]):f.title);
        return g
    };
    c.prototype.getUID=function(e){do{e+=~~(Math.random()*1000000)
    }while(document.getElementById(e));
        return e
    };
    c.prototype.tip=function(){if(!this.$tip){this.$tip=d(this.options.template);
        if(this.$tip.length!=1){throw new Error(this.type+" `template` option must consist of exactly 1 top-level element!")
        }}return this.$tip
    };
    c.prototype.arrow=function(){return(this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow"))
    };
    c.prototype.enable=function(){this.enabled=true
    };
    c.prototype.disable=function(){this.enabled=false
    };
    c.prototype.toggleEnabled=function(){this.enabled=!this.enabled
    };
    c.prototype.toggle=function(g){var f=this;
        if(g){f=d(g.currentTarget).data("bs."+this.type);
            if(!f){f=new this.constructor(g.currentTarget,this.getDelegateOptions());
                d(g.currentTarget).data("bs."+this.type,f)
            }}if(g){f.inState.click=!f.inState.click;
            if(f.isInStateTrue()){f.enter(f)
            }else{f.leave(f)
            }}else{f.tip().hasClass("in")?f.leave(f):f.enter(f)
        }};
    c.prototype.destroy=function(){var e=this;
        clearTimeout(this.timeout);
        this.hide(function(){e.$element.off("."+e.type).removeData("bs."+e.type);
            if(e.$tip){e.$tip.detach()
            }e.$tip=null;
            e.$arrow=null;
            e.$viewport=null
        })
    };
    function b(e){return this.each(function(){var h=d(this);
        var g=h.data("bs.tooltip");
        var f=typeof e=="object"&&e;
        if(!g&&/destroy|hide/.test(e)){return
        }if(!g){h.data("bs.tooltip",(g=new c(this,f)))
        }if(typeof e=="string"){g[e]()
        }})
    }var a=d.fn.tooltip;
    d.fn.tooltip=b;
    d.fn.tooltip.Constructor=c;
    d.fn.tooltip.noConflict=function(){d.fn.tooltip=a;
        return this
    }
}(jQuery);
+function(d){var c=function(f,e){this.init("popover",f,e)
};
    if(!d.fn.tooltip){throw new Error("Popover requires tooltip.js")
    }c.VERSION="3.3.6";
    c.DEFAULTS=d.extend({},d.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'});
    c.prototype=d.extend({},d.fn.tooltip.Constructor.prototype);
    c.prototype.constructor=c;
    c.prototype.getDefaults=function(){return c.DEFAULTS
    };
    c.prototype.setContent=function(){var g=this.tip();
        var f=this.getTitle();
        var e=this.getContent();
        g.find(".popover-title")[this.options.html?"html":"text"](f);
        g.find(".popover-content").children().detach().end()[this.options.html?(typeof e=="string"?"html":"append"):"text"](e);
        g.removeClass("fade top bottom left right in");
        if(!g.find(".popover-title").html()){g.find(".popover-title").hide()
        }};
    c.prototype.hasContent=function(){return this.getTitle()||this.getContent()
    };
    c.prototype.getContent=function(){var e=this.$element;
        var f=this.options;
        return e.attr("data-content")||(typeof f.content=="function"?f.content.call(e[0]):f.content)
    };
    c.prototype.arrow=function(){return(this.$arrow=this.$arrow||this.tip().find(".arrow"))
    };
    function b(e){return this.each(function(){var h=d(this);
        var g=h.data("bs.popover");
        var f=typeof e=="object"&&e;
        if(!g&&/destroy|hide/.test(e)){return
        }if(!g){h.data("bs.popover",(g=new c(this,f)))
        }if(typeof e=="string"){g[e]()
        }})
    }var a=d.fn.popover;
    d.fn.popover=b;
    d.fn.popover.Constructor=c;
    d.fn.popover.noConflict=function(){d.fn.popover=a;
        return this
    }
}(jQuery);
+function(d){function c(f,e){this.$body=d(document.body);
    this.$scrollElement=d(f).is(document.body)?d(window):d(f);
    this.options=d.extend({},c.DEFAULTS,e);
    this.selector=(this.options.target||"")+" .nav li > a";
    this.offsets=[];
    this.targets=[];
    this.activeTarget=null;
    this.scrollHeight=0;
    this.$scrollElement.on("scroll.bs.scrollspy",d.proxy(this.process,this));
    this.refresh();
    this.process()
}c.VERSION="3.3.6";
    c.DEFAULTS={offset:10};
    c.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)
    };
    c.prototype.refresh=function(){var g=this;
        var e="offset";
        var f=0;
        this.offsets=[];
        this.targets=[];
        this.scrollHeight=this.getScrollHeight();
        if(!d.isWindow(this.$scrollElement[0])){e="position";
            f=this.$scrollElement.scrollTop()
        }this.$body.find(this.selector).map(function(){var j=d(this);
            var h=j.data("target")||j.attr("href");
            var k=/^#./.test(h)&&d(h);
            return(k&&k.length&&k.is(":visible")&&[[k[e]().top+f,h]])||null
        }).sort(function(j,h){return j[0]-h[0]
        }).each(function(){g.offsets.push(this[0]);
            g.targets.push(this[1])
        })
    };
    c.prototype.process=function(){var k=this.$scrollElement.scrollTop()+this.options.offset;
        var g=this.getScrollHeight();
        var j=this.options.offset+g-this.$scrollElement.height();
        var h=this.offsets;
        var e=this.targets;
        var l=this.activeTarget;
        var f;
        if(this.scrollHeight!=g){this.refresh()
        }if(k>=j){return l!=(f=e[e.length-1])&&this.activate(f)
        }if(l&&k<h[0]){this.activeTarget=null;
            return this.clear()
        }for(f=h.length;
             f--;
        ){l!=e[f]&&k>=h[f]&&(h[f+1]===undefined||k<h[f+1])&&this.activate(e[f])
        }};
    c.prototype.activate=function(g){this.activeTarget=g;
        this.clear();
        var e=this.selector+'[data-target="'+g+'"],'+this.selector+'[href="'+g+'"]';
        var f=d(e).parents("li").addClass("active");
        if(f.parent(".dropdown-menu").length){f=f.closest("li.dropdown").addClass("active")
        }f.trigger("activate.bs.scrollspy")
    };
    c.prototype.clear=function(){d(this.selector).parentsUntil(this.options.target,".active").removeClass("active")
    };
    function b(e){return this.each(function(){var h=d(this);
        var g=h.data("bs.scrollspy");
        var f=typeof e=="object"&&e;
        if(!g){h.data("bs.scrollspy",(g=new c(this,f)))
        }if(typeof e=="string"){g[e]()
        }})
    }var a=d.fn.scrollspy;
    d.fn.scrollspy=b;
    d.fn.scrollspy.Constructor=c;
    d.fn.scrollspy.noConflict=function(){d.fn.scrollspy=a;
        return this
    };
    d(window).on("load.bs.scrollspy.data-api",function(){d('[data-spy="scroll"]').each(function(){var e=d(this);
        b.call(e,e.data())
    })
    })
}(jQuery);
+function(d){var b=function(f){this.element=d(f)
};
    b.VERSION="3.3.6";
    b.TRANSITION_DURATION=150;
    b.prototype.show=function(){var m=this.element;
        var h=m.closest("ul:not(.dropdown-menu)");
        var g=m.data("target");
        if(!g){g=m.attr("href");
            g=g&&g.replace(/.*(?=#[^\s]*$)/,"")
        }if(m.parent("li").hasClass("active")){return
        }var k=h.find(".active:last a");
        var l=d.Event("hide.bs.tab",{relatedTarget:m[0]});
        var j=d.Event("show.bs.tab",{relatedTarget:k[0]});
        k.trigger(l);
        m.trigger(j);
        if(j.isDefaultPrevented()||l.isDefaultPrevented()){return
        }var f=d(g);
        this.activate(m.closest("li"),h);
        this.activate(f,f.parent(),function(){k.trigger({type:"hidden.bs.tab",relatedTarget:m[0]});
            m.trigger({type:"shown.bs.tab",relatedTarget:k[0]})
        })
    };
    b.prototype.activate=function(h,g,l){var f=g.find("> .active");
        var k=l&&d.support.transition&&(f.length&&f.hasClass("fade")||!!g.find("> .fade").length);
        function j(){f.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",false);
            h.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",true);
            if(k){h[0].offsetWidth;
                h.addClass("in")
            }else{h.removeClass("fade")
            }if(h.parent(".dropdown-menu").length){h.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",true)
            }l&&l()
        }f.length&&k?f.one("bsTransitionEnd",j).emulateTransitionEnd(b.TRANSITION_DURATION):j();
        f.removeClass("in")
    };
    function c(f){return this.each(function(){var h=d(this);
        var g=h.data("bs.tab");
        if(!g){h.data("bs.tab",(g=new b(this)))
        }if(typeof f=="string"){g[f]()
        }})
    }var a=d.fn.tab;
    d.fn.tab=c;
    d.fn.tab.Constructor=b;
    d.fn.tab.noConflict=function(){d.fn.tab=a;
        return this
    };
    var e=function(f){f.preventDefault();
        c.call(d(this),"show")
    };
    d(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',e).on("click.bs.tab.data-api",'[data-toggle="pill"]',e)
}(jQuery);
+function(d){var c=function(f,e){this.options=d.extend({},c.DEFAULTS,e);
    this.$target=d(this.options.target).on("scroll.bs.affix.data-api",d.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",d.proxy(this.checkPositionWithEventLoop,this));
    this.$element=d(f);
    this.affixed=null;
    this.unpin=null;
    this.pinnedOffset=null;
    this.checkPosition()
};
    c.VERSION="3.3.6";
    c.RESET="affix affix-top affix-bottom";
    c.DEFAULTS={offset:0,target:window};
    c.prototype.getState=function(o,n,f,g){var e=this.$target.scrollTop();
        var j=this.$element.offset();
        var k=this.$target.height();
        if(f!=null&&this.affixed=="top"){return e<f?"top":false
        }if(this.affixed=="bottom"){if(f!=null){return(e+this.unpin<=j.top)?false:"bottom"
        }return(e+k<=o-g)?false:"bottom"
        }var h=this.affixed==null;
        var m=h?e:j.top;
        var l=h?k:n;
        if(f!=null&&e<=f){return"top"
        }if(g!=null&&(m+l>=o-g)){return"bottom"
        }return false
    };
    c.prototype.getPinnedOffset=function(){if(this.pinnedOffset){return this.pinnedOffset
    }this.$element.removeClass(c.RESET).addClass("affix");
        var f=this.$target.scrollTop();
        var e=this.$element.offset();
        return(this.pinnedOffset=e.top-f)
    };
    c.prototype.checkPositionWithEventLoop=function(){setTimeout(d.proxy(this.checkPosition,this),1)
    };
    c.prototype.checkPosition=function(){if(!this.$element.is(":visible")){return
    }var f=this.$element.height();
        var m=this.options.offset;
        var k=m.top;
        var h=m.bottom;
        var j=Math.max(d(document).height(),d(document.body).height());
        if(typeof m!="object"){h=k=m
        }if(typeof k=="function"){k=m.top(this.$element)
        }if(typeof h=="function"){h=m.bottom(this.$element)
        }var g=this.getState(j,f,k,h);
        if(this.affixed!=g){if(this.unpin!=null){this.$element.css("top","")
        }var n="affix"+(g?"-"+g:"");
            var l=d.Event(n+".bs.affix");
            this.$element.trigger(l);
            if(l.isDefaultPrevented()){return
            }this.affixed=g;
            this.unpin=g=="bottom"?this.getPinnedOffset():null;
            this.$element.removeClass(c.RESET).addClass(n).trigger(n.replace("affix","affixed")+".bs.affix")
        }if(g=="bottom"){this.$element.offset({top:j-f-h})
        }};
    function b(e){return this.each(function(){var h=d(this);
        var g=h.data("bs.affix");
        var f=typeof e=="object"&&e;
        if(!g){h.data("bs.affix",(g=new c(this,f)))
        }if(typeof e=="string"){g[e]()
        }})
    }var a=d.fn.affix;
    d.fn.affix=b;
    d.fn.affix.Constructor=c;
    d.fn.affix.noConflict=function(){d.fn.affix=a;
        return this
    };
    d(window).on("load",function(){d('[data-spy="affix"]').each(function(){var f=d(this);
        var e=f.data();
        e.offset=e.offset||{};
        if(e.offsetBottom!=null){e.offset.bottom=e.offsetBottom
        }if(e.offsetTop!=null){e.offset.top=e.offsetTop
        }b.call(f,e)
    })
    })
}(jQuery);
!function(b){"function"==typeof define&&define.amd?define(["jquery"],b):"undefined"!=typeof exports?module.exports=b(require("jquery")):b(jQuery)
}(function(d){var c=window.Slick||{};
    c=function(){function e(j,h){var b,g=this;
        g.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:d(j),appendDots:d(j),arrows:!0,asNavFor:null,prevArrow:'<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',nextArrow:'<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',autoplay:!1,autoplaySpeed:3000,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(k,f){return'<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">'+(f+1)+"</button>"
        },dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:0.35,fade:!1,focusOnSelect:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,useTransform:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1000},g.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},d.extend(g,g.initials),g.activeBreakpoint=null,g.animType=null,g.animProp=null,g.breakpoints=[],g.breakpointSettings=[],g.cssTransitions=!1,g.hidden="hidden",g.paused=!1,g.positionProp=null,g.respondTo=null,g.rowCount=1,g.shouldClick=!0,g.$slider=d(j),g.$slidesCache=null,g.transformType=null,g.transitionType=null,g.visibilityChange="visibilitychange",g.windowWidth=0,g.windowTimer=null,b=d(j).data("slick")||{},g.options=d.extend({},g.defaults,b,h),g.currentSlide=g.options.initialSlide,g.originalSettings=g.options,"undefined"!=typeof document.mozHidden?(g.hidden="mozHidden",g.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(g.hidden="webkitHidden",g.visibilityChange="webkitvisibilitychange"),g.autoPlay=d.proxy(g.autoPlay,g),g.autoPlayClear=d.proxy(g.autoPlayClear,g),g.changeSlide=d.proxy(g.changeSlide,g),g.clickHandler=d.proxy(g.clickHandler,g),g.selectHandler=d.proxy(g.selectHandler,g),g.setPosition=d.proxy(g.setPosition,g),g.swipeHandler=d.proxy(g.swipeHandler,g),g.dragHandler=d.proxy(g.dragHandler,g),g.keyHandler=d.proxy(g.keyHandler,g),g.autoPlayIterator=d.proxy(g.autoPlayIterator,g),g.instanceUid=a++,g.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,g.registerBreakpoints(),g.init(!0),g.checkResponsive(!0)
    }var a=0;
        return e
    }(),c.prototype.addSlide=c.prototype.slickAdd=function(a,h,g){var f=this;
        if("boolean"==typeof h){g=h,h=null
        }else{if(0>h||h>=f.slideCount){return !1
        }}f.unload(),"number"==typeof h?0===h&&0===f.$slides.length?d(a).appendTo(f.$slideTrack):g?d(a).insertBefore(f.$slides.eq(h)):d(a).insertAfter(f.$slides.eq(h)):g===!0?d(a).prependTo(f.$slideTrack):d(a).appendTo(f.$slideTrack),f.$slides=f.$slideTrack.children(this.options.slide),f.$slideTrack.children(this.options.slide).detach(),f.$slideTrack.append(f.$slides),f.$slides.each(function(e,j){d(j).attr("data-slick-index",e)
        }),f.$slidesCache=f.$slides,f.reinit()
    },c.prototype.animateHeight=function(){var f=this;
        if(1===f.options.slidesToShow&&f.options.adaptiveHeight===!0&&f.options.vertical===!1){var e=f.$slides.eq(f.currentSlide).outerHeight(!0);
            f.$list.animate({height:e},f.options.speed)
        }},c.prototype.animateSlide=function(a,h){var g={},f=this;
        f.animateHeight(),f.options.rtl===!0&&f.options.vertical===!1&&(a=-a),f.transformsEnabled===!1?f.options.vertical===!1?f.$slideTrack.animate({left:a},f.options.speed,f.options.easing,h):f.$slideTrack.animate({top:a},f.options.speed,f.options.easing,h):f.cssTransitions===!1?(f.options.rtl===!0&&(f.currentLeft=-f.currentLeft),d({animStart:f.currentLeft}).animate({animStart:a},{duration:f.options.speed,easing:f.options.easing,step:function(b){b=Math.ceil(b),f.options.vertical===!1?(g[f.animType]="translate("+b+"px, 0px)",f.$slideTrack.css(g)):(g[f.animType]="translate(0px,"+b+"px)",f.$slideTrack.css(g))
        },complete:function(){h&&h.call()
        }})):(f.applyTransition(),a=Math.ceil(a),f.options.vertical===!1?g[f.animType]="translate3d("+a+"px, 0px, 0px)":g[f.animType]="translate3d(0px,"+a+"px, 0px)",f.$slideTrack.css(g),h&&setTimeout(function(){f.disableTransition(),h.call()
        },f.options.speed))
    },c.prototype.asNavFor=function(a){var f=this,e=f.options.asNavFor;
        e&&null!==e&&(e=d(e).not(f.$slider)),null!==e&&"object"==typeof e&&e.each(function(){var b=d(this).slick("getSlick");
            b.unslicked||b.slideHandler(a,!0)
        })
    },c.prototype.applyTransition=function(f){var e=this,g={};
        e.options.fade===!1?g[e.transitionType]=e.transformType+" "+e.options.speed+"ms "+e.options.cssEase:g[e.transitionType]="opacity "+e.options.speed+"ms "+e.options.cssEase,e.options.fade===!1?e.$slideTrack.css(g):e.$slides.eq(f).css(g)
    },c.prototype.autoPlay=function(){var b=this;
        b.autoPlayTimer&&clearInterval(b.autoPlayTimer),b.slideCount>b.options.slidesToShow&&b.paused!==!0&&(b.autoPlayTimer=setInterval(b.autoPlayIterator,b.options.autoplaySpeed))
    },c.prototype.autoPlayClear=function(){var b=this;
        b.autoPlayTimer&&clearInterval(b.autoPlayTimer)
    },c.prototype.autoPlayIterator=function(){var b=this;
        b.options.infinite===!1?1===b.direction?(b.currentSlide+1===b.slideCount-1&&(b.direction=0),b.slideHandler(b.currentSlide+b.options.slidesToScroll)):(b.currentSlide-1===0&&(b.direction=1),b.slideHandler(b.currentSlide-b.options.slidesToScroll)):b.slideHandler(b.currentSlide+b.options.slidesToScroll)
    },c.prototype.buildArrows=function(){var a=this;
        a.options.arrows===!0&&(a.$prevArrow=d(a.options.prevArrow).addClass("slick-arrow"),a.$nextArrow=d(a.options.nextArrow).addClass("slick-arrow"),a.slideCount>a.options.slidesToShow?(a.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),a.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),a.htmlExpr.test(a.options.prevArrow)&&a.$prevArrow.prependTo(a.options.appendArrows),a.htmlExpr.test(a.options.nextArrow)&&a.$nextArrow.appendTo(a.options.appendArrows),a.options.infinite!==!0&&a.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true")):a.$prevArrow.add(a.$nextArrow).addClass("slick-hidden").attr({"aria-disabled":"true",tabindex:"-1"}))
    },c.prototype.buildDots=function(){var f,e,a=this;
        if(a.options.dots===!0&&a.slideCount>a.options.slidesToShow){for(e='<ul class="'+a.options.dotsClass+'">',f=0;
                                                                         f<=a.getDotCount();
                                                                         f+=1){e+="<li>"+a.options.customPaging.call(this,a,f)+"</li>"
        }e+="</ul>",a.$dots=d(e).appendTo(a.options.appendDots),a.$dots.find("li").first().addClass("slick-active").attr("aria-hidden","false")
        }},c.prototype.buildOut=function(){var a=this;
        a.$slides=a.$slider.children(a.options.slide+":not(.slick-cloned)").addClass("slick-slide"),a.slideCount=a.$slides.length,a.$slides.each(function(e,f){d(f).attr("data-slick-index",e).data("originalStyling",d(f).attr("style")||"")
        }),a.$slider.addClass("slick-slider"),a.$slideTrack=0===a.slideCount?d('<div class="slick-track"/>').appendTo(a.$slider):a.$slides.wrapAll('<div class="slick-track"/>').parent(),a.$list=a.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),a.$slideTrack.css("opacity",0),(a.options.centerMode===!0||a.options.swipeToSlide===!0)&&(a.options.slidesToScroll=1),d("img[data-lazy]",a.$slider).not("[src]").addClass("slick-loading"),a.setupInfinite(),a.buildArrows(),a.buildDots(),a.updateDots(),a.setSlideClasses("number"==typeof a.currentSlide?a.currentSlide:0),a.options.draggable===!0&&a.$list.addClass("draggable")
    },c.prototype.buildRows=function(){var u,t,s,r,q,p,o,v=this;
        if(r=document.createDocumentFragment(),p=v.$slider.children(),v.options.rows>1){for(o=v.options.slidesPerRow*v.options.rows,q=Math.ceil(p.length/o),u=0;
                                                                                            q>u;
                                                                                            u++){var n=document.createElement("div");
            for(t=0;
                t<v.options.rows;
                t++){var m=document.createElement("div");
                for(s=0;
                    s<v.options.slidesPerRow;
                    s++){var l=u*o+(t*v.options.slidesPerRow+s);
                    p.get(l)&&m.appendChild(p.get(l))
                }n.appendChild(m)
            }r.appendChild(n)
        }v.$slider.html(r),v.$slider.children().children().children().css({width:100/v.options.slidesPerRow+"%",display:"inline-block"})
        }},c.prototype.checkResponsive=function(r,q){var o,n,m,p=this,l=!1,k=p.$slider.width(),a=window.innerWidth||d(window).width();
        if("window"===p.respondTo?m=a:"slider"===p.respondTo?m=k:"min"===p.respondTo&&(m=Math.min(a,k)),p.options.responsive&&p.options.responsive.length&&null!==p.options.responsive){n=null;
            for(o in p.breakpoints){p.breakpoints.hasOwnProperty(o)&&(p.originalSettings.mobileFirst===!1?m<p.breakpoints[o]&&(n=p.breakpoints[o]):m>p.breakpoints[o]&&(n=p.breakpoints[o]))
            }null!==n?null!==p.activeBreakpoint?(n!==p.activeBreakpoint||q)&&(p.activeBreakpoint=n,"unslick"===p.breakpointSettings[n]?p.unslick(n):(p.options=d.extend({},p.originalSettings,p.breakpointSettings[n]),r===!0&&(p.currentSlide=p.options.initialSlide),p.refresh(r)),l=n):(p.activeBreakpoint=n,"unslick"===p.breakpointSettings[n]?p.unslick(n):(p.options=d.extend({},p.originalSettings,p.breakpointSettings[n]),r===!0&&(p.currentSlide=p.options.initialSlide),p.refresh(r)),l=n):null!==p.activeBreakpoint&&(p.activeBreakpoint=null,p.options=p.originalSettings,r===!0&&(p.currentSlide=p.options.initialSlide),p.refresh(r),l=n),r||l===!1||p.$slider.trigger("breakpoint",[p,l])
        }},c.prototype.changeSlide=function(a,p){var m,l,k,o=this,n=d(a.target);
        switch(n.is("a")&&a.preventDefault(),n.is("li")||(n=n.closest("li")),k=o.slideCount%o.options.slidesToScroll!==0,m=k?0:(o.slideCount-o.currentSlide)%o.options.slidesToScroll,a.data.message){case"previous":l=0===m?o.options.slidesToScroll:o.options.slidesToShow-m,o.slideCount>o.options.slidesToShow&&o.slideHandler(o.currentSlide-l,!1,p);
            break;
            case"next":l=0===m?o.options.slidesToScroll:m,o.slideCount>o.options.slidesToShow&&o.slideHandler(o.currentSlide+l,!1,p);
                break;
            case"index":var j=0===a.data.index?0:a.data.index||n.index()*o.options.slidesToScroll;
                o.slideHandler(o.checkNavigable(j),!1,p),n.children().trigger("focus");
                break;
            default:return
        }},c.prototype.checkNavigable=function(g){var k,j,f=this;
        if(k=f.getNavigableIndexes(),j=0,g>k[k.length-1]){g=k[k.length-1]
        }else{for(var h in k){if(g<k[h]){g=j;
            break
        }j=k[h]
        }}return g
    },c.prototype.cleanUpEvents=function(){var a=this;
        a.options.dots&&null!==a.$dots&&(d("li",a.$dots).off("click.slick",a.changeSlide),a.options.pauseOnDotsHover===!0&&a.options.autoplay===!0&&d("li",a.$dots).off("mouseenter.slick",d.proxy(a.setPaused,a,!0)).off("mouseleave.slick",d.proxy(a.setPaused,a,!1))),a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow&&a.$prevArrow.off("click.slick",a.changeSlide),a.$nextArrow&&a.$nextArrow.off("click.slick",a.changeSlide)),a.$list.off("touchstart.slick mousedown.slick",a.swipeHandler),a.$list.off("touchmove.slick mousemove.slick",a.swipeHandler),a.$list.off("touchend.slick mouseup.slick",a.swipeHandler),a.$list.off("touchcancel.slick mouseleave.slick",a.swipeHandler),a.$list.off("click.slick",a.clickHandler),d(document).off(a.visibilityChange,a.visibility),a.$list.off("mouseenter.slick",d.proxy(a.setPaused,a,!0)),a.$list.off("mouseleave.slick",d.proxy(a.setPaused,a,!1)),a.options.accessibility===!0&&a.$list.off("keydown.slick",a.keyHandler),a.options.focusOnSelect===!0&&d(a.$slideTrack).children().off("click.slick",a.selectHandler),d(window).off("orientationchange.slick.slick-"+a.instanceUid,a.orientationChange),d(window).off("resize.slick.slick-"+a.instanceUid,a.resize),d("[draggable!=true]",a.$slideTrack).off("dragstart",a.preventDefault),d(window).off("load.slick.slick-"+a.instanceUid,a.setPosition),d(document).off("ready.slick.slick-"+a.instanceUid,a.setPosition)
    },c.prototype.cleanUpRows=function(){var e,f=this;
        f.options.rows>1&&(e=f.$slides.children().children(),e.removeAttr("style"),f.$slider.html(e))
    },c.prototype.clickHandler=function(f){var e=this;
        e.shouldClick===!1&&(f.stopImmediatePropagation(),f.stopPropagation(),f.preventDefault())
    },c.prototype.destroy=function(a){var e=this;
        e.autoPlayClear(),e.touchObject={},e.cleanUpEvents(),d(".slick-cloned",e.$slider).detach(),e.$dots&&e.$dots.remove(),e.$prevArrow&&e.$prevArrow.length&&(e.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.remove()),e.$nextArrow&&e.$nextArrow.length&&(e.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.remove()),e.$slides&&(e.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){d(this).attr("style",d(this).data("originalStyling"))
        }),e.$slideTrack.children(this.options.slide).detach(),e.$slideTrack.detach(),e.$list.detach(),e.$slider.append(e.$slides)),e.cleanUpRows(),e.$slider.removeClass("slick-slider"),e.$slider.removeClass("slick-initialized"),e.unslicked=!0,a||e.$slider.trigger("destroy",[e])
    },c.prototype.disableTransition=function(f){var e=this,g={};
        g[e.transitionType]="",e.options.fade===!1?e.$slideTrack.css(g):e.$slides.eq(f).css(g)
    },c.prototype.fadeSlide=function(f,e){var g=this;
        g.cssTransitions===!1?(g.$slides.eq(f).css({zIndex:g.options.zIndex}),g.$slides.eq(f).animate({opacity:1},g.options.speed,g.options.easing,e)):(g.applyTransition(f),g.$slides.eq(f).css({opacity:1,zIndex:g.options.zIndex}),e&&setTimeout(function(){g.disableTransition(f),e.call()
        },g.options.speed))
    },c.prototype.fadeSlideOut=function(f){var e=this;
        e.cssTransitions===!1?e.$slides.eq(f).animate({opacity:0,zIndex:e.options.zIndex-2},e.options.speed,e.options.easing):(e.applyTransition(f),e.$slides.eq(f).css({opacity:0,zIndex:e.options.zIndex-2}))
    },c.prototype.filterSlides=c.prototype.slickFilter=function(f){var e=this;
        null!==f&&(e.$slidesCache=e.$slides,e.unload(),e.$slideTrack.children(this.options.slide).detach(),e.$slidesCache.filter(f).appendTo(e.$slideTrack),e.reinit())
    },c.prototype.getCurrent=c.prototype.slickCurrentSlide=function(){var b=this;
        return b.currentSlide
    },c.prototype.getDotCount=function(){var f=this,e=0,h=0,g=0;
        if(f.options.infinite===!0){for(;
            e<f.slideCount;
        ){++g,e=h+f.options.slidesToScroll,h+=f.options.slidesToScroll<=f.options.slidesToShow?f.options.slidesToScroll:f.options.slidesToShow
        }}else{if(f.options.centerMode===!0){g=f.slideCount
        }else{for(;
            e<f.slideCount;
        ){++g,e=h+f.options.slidesToScroll,h+=f.options.slidesToScroll<=f.options.slidesToShow?f.options.slidesToScroll:f.options.slidesToShow
        }}}return g-1
    },c.prototype.getLeft=function(h){var m,l,j,g=this,k=0;
        return g.slideOffset=0,l=g.$slides.first().outerHeight(!0),g.options.infinite===!0?(g.slideCount>g.options.slidesToShow&&(g.slideOffset=g.slideWidth*g.options.slidesToShow*-1,k=l*g.options.slidesToShow*-1),g.slideCount%g.options.slidesToScroll!==0&&h+g.options.slidesToScroll>g.slideCount&&g.slideCount>g.options.slidesToShow&&(h>g.slideCount?(g.slideOffset=(g.options.slidesToShow-(h-g.slideCount))*g.slideWidth*-1,k=(g.options.slidesToShow-(h-g.slideCount))*l*-1):(g.slideOffset=g.slideCount%g.options.slidesToScroll*g.slideWidth*-1,k=g.slideCount%g.options.slidesToScroll*l*-1))):h+g.options.slidesToShow>g.slideCount&&(g.slideOffset=(h+g.options.slidesToShow-g.slideCount)*g.slideWidth,k=(h+g.options.slidesToShow-g.slideCount)*l),g.slideCount<=g.options.slidesToShow&&(g.slideOffset=0,k=0),g.options.centerMode===!0&&g.options.infinite===!0?g.slideOffset+=g.slideWidth*Math.floor(g.options.slidesToShow/2)-g.slideWidth:g.options.centerMode===!0&&(g.slideOffset=0,g.slideOffset+=g.slideWidth*Math.floor(g.options.slidesToShow/2)),m=g.options.vertical===!1?h*g.slideWidth*-1+g.slideOffset:h*l*-1+k,g.options.variableWidth===!0&&(j=g.slideCount<=g.options.slidesToShow||g.options.infinite===!1?g.$slideTrack.children(".slick-slide").eq(h):g.$slideTrack.children(".slick-slide").eq(h+g.options.slidesToShow),m=g.options.rtl===!0?j[0]?-1*(g.$slideTrack.width()-j[0].offsetLeft-j.width()):0:j[0]?-1*j[0].offsetLeft:0,g.options.centerMode===!0&&(j=g.slideCount<=g.options.slidesToShow||g.options.infinite===!1?g.$slideTrack.children(".slick-slide").eq(h):g.$slideTrack.children(".slick-slide").eq(h+g.options.slidesToShow+1),m=g.options.rtl===!0?j[0]?-1*(g.$slideTrack.width()-j[0].offsetLeft-j.width()):0:j[0]?-1*j[0].offsetLeft:0,m+=(g.$list.width()-j.outerWidth())/2)),m
    },c.prototype.getOption=c.prototype.slickGetOption=function(f){var e=this;
        return e.options[f]
    },c.prototype.getNavigableIndexes=function(){var h,g=this,f=0,k=0,j=[];
        for(g.options.infinite===!1?h=g.slideCount:(f=-1*g.options.slidesToScroll,k=-1*g.options.slidesToScroll,h=2*g.slideCount);
            h>f;
        ){j.push(f),f=k+g.options.slidesToScroll,k+=g.options.slidesToScroll<=g.options.slidesToShow?g.options.slidesToScroll:g.options.slidesToShow
        }return j
    },c.prototype.getSlick=function(){return this
    },c.prototype.getSlideCount=function(){var h,g,f,a=this;
        return f=a.options.centerMode===!0?a.slideWidth*Math.floor(a.options.slidesToShow/2):0,a.options.swipeToSlide===!0?(a.$slideTrack.find(".slick-slide").each(function(e,b){return b.offsetLeft-f+d(b).outerWidth()/2>-1*a.swipeLeft?(g=b,!1):void 0
        }),h=Math.abs(d(g).attr("data-slick-index")-a.currentSlide)||1):a.options.slidesToScroll
    },c.prototype.goTo=c.prototype.slickGoTo=function(f,e){var g=this;
        g.changeSlide({data:{message:"index",index:parseInt(f)}},e)
    },c.prototype.init=function(a){var e=this;
        d(e.$slider).hasClass("slick-initialized")||(d(e.$slider).addClass("slick-initialized"),e.buildRows(),e.buildOut(),e.setProps(),e.startLoad(),e.loadSlider(),e.initializeEvents(),e.updateArrows(),e.updateDots()),a&&e.$slider.trigger("init",[e]),e.options.accessibility===!0&&e.initADA()
    },c.prototype.initArrowEvents=function(){var b=this;
        b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow.on("click.slick",{message:"previous"},b.changeSlide),b.$nextArrow.on("click.slick",{message:"next"},b.changeSlide))
    },c.prototype.initDotEvents=function(){var a=this;
        a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&d("li",a.$dots).on("click.slick",{message:"index"},a.changeSlide),a.options.dots===!0&&a.options.pauseOnDotsHover===!0&&a.options.autoplay===!0&&d("li",a.$dots).on("mouseenter.slick",d.proxy(a.setPaused,a,!0)).on("mouseleave.slick",d.proxy(a.setPaused,a,!1))
    },c.prototype.initializeEvents=function(){var a=this;
        a.initArrowEvents(),a.initDotEvents(),a.$list.on("touchstart.slick mousedown.slick",{action:"start"},a.swipeHandler),a.$list.on("touchmove.slick mousemove.slick",{action:"move"},a.swipeHandler),a.$list.on("touchend.slick mouseup.slick",{action:"end"},a.swipeHandler),a.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},a.swipeHandler),a.$list.on("click.slick",a.clickHandler),d(document).on(a.visibilityChange,d.proxy(a.visibility,a)),a.$list.on("mouseenter.slick",d.proxy(a.setPaused,a,!0)),a.$list.on("mouseleave.slick",d.proxy(a.setPaused,a,!1)),a.options.accessibility===!0&&a.$list.on("keydown.slick",a.keyHandler),a.options.focusOnSelect===!0&&d(a.$slideTrack).children().on("click.slick",a.selectHandler),d(window).on("orientationchange.slick.slick-"+a.instanceUid,d.proxy(a.orientationChange,a)),d(window).on("resize.slick.slick-"+a.instanceUid,d.proxy(a.resize,a)),d("[draggable!=true]",a.$slideTrack).on("dragstart",a.preventDefault),d(window).on("load.slick.slick-"+a.instanceUid,a.setPosition),d(document).on("ready.slick.slick-"+a.instanceUid,a.setPosition)
    },c.prototype.initUI=function(){var b=this;
        b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow.show(),b.$nextArrow.show()),b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&b.$dots.show(),b.options.autoplay===!0&&b.autoPlay()
    },c.prototype.keyHandler=function(f){var e=this;
        f.target.tagName.match("TEXTAREA|INPUT|SELECT")||(37===f.keyCode&&e.options.accessibility===!0?e.changeSlide({data:{message:"previous"}}):39===f.keyCode&&e.options.accessibility===!0&&e.changeSlide({data:{message:"next"}}))
    },c.prototype.lazyLoad=function(){function h(e){d("img[data-lazy]",e).each(function(){var f=d(this),n=d(this).attr("data-lazy"),g=document.createElement("img");
        g.onload=function(){f.animate({opacity:0},100,function(){f.attr("src",n).animate({opacity:1},200,function(){f.removeAttr("data-lazy").removeClass("slick-loading")
        })
        })
        },g.src=n
    })
    }var m,l,k,j,a=this;
        a.options.centerMode===!0?a.options.infinite===!0?(k=a.currentSlide+(a.options.slidesToShow/2+1),j=k+a.options.slidesToShow+2):(k=Math.max(0,a.currentSlide-(a.options.slidesToShow/2+1)),j=2+(a.options.slidesToShow/2+1)+a.currentSlide):(k=a.options.infinite?a.options.slidesToShow+a.currentSlide:a.currentSlide,j=k+a.options.slidesToShow,a.options.fade===!0&&(k>0&&k--,j<=a.slideCount&&j++)),m=a.$slider.find(".slick-slide").slice(k,j),h(m),a.slideCount<=a.options.slidesToShow?(l=a.$slider.find(".slick-slide"),h(l)):a.currentSlide>=a.slideCount-a.options.slidesToShow?(l=a.$slider.find(".slick-cloned").slice(0,a.options.slidesToShow),h(l)):0===a.currentSlide&&(l=a.$slider.find(".slick-cloned").slice(-1*a.options.slidesToShow),h(l))
    },c.prototype.loadSlider=function(){var b=this;
        b.setPosition(),b.$slideTrack.css({opacity:1}),b.$slider.removeClass("slick-loading"),b.initUI(),"progressive"===b.options.lazyLoad&&b.progressiveLazyLoad()
    },c.prototype.next=c.prototype.slickNext=function(){var b=this;
        b.changeSlide({data:{message:"next"}})
    },c.prototype.orientationChange=function(){var b=this;
        b.checkResponsive(),b.setPosition()
    },c.prototype.pause=c.prototype.slickPause=function(){var b=this;
        b.autoPlayClear(),b.paused=!0
    },c.prototype.play=c.prototype.slickPlay=function(){var b=this;
        b.paused=!1,b.autoPlay()
    },c.prototype.postSlide=function(f){var e=this;
        e.$slider.trigger("afterChange",[e,f]),e.animating=!1,e.setPosition(),e.swipeLeft=null,e.options.autoplay===!0&&e.paused===!1&&e.autoPlay(),e.options.accessibility===!0&&e.initADA()
    },c.prototype.prev=c.prototype.slickPrev=function(){var b=this;
        b.changeSlide({data:{message:"previous"}})
    },c.prototype.preventDefault=function(b){b.preventDefault()
    },c.prototype.progressiveLazyLoad=function(){var f,e,a=this;
        f=d("img[data-lazy]",a.$slider).length,f>0&&(e=d("img[data-lazy]",a.$slider).first(),e.attr("src",null),e.attr("src",e.attr("data-lazy")).removeClass("slick-loading").load(function(){e.removeAttr("data-lazy"),a.progressiveLazyLoad(),a.options.adaptiveHeight===!0&&a.setPosition()
        }).error(function(){e.removeAttr("data-lazy"),a.progressiveLazyLoad()
        }))
    },c.prototype.refresh=function(a){var g,f,h=this;
        f=h.slideCount-h.options.slidesToShow,h.options.infinite||(h.slideCount<=h.options.slidesToShow?h.currentSlide=0:h.currentSlide>f&&(h.currentSlide=f)),g=h.currentSlide,h.destroy(!0),d.extend(h,h.initials,{currentSlide:g}),h.init(),a||h.changeSlide({data:{message:"index",index:g}},!1)
    },c.prototype.registerBreakpoints=function(){var k,j,h,a=this,g=a.options.responsive||null;
        if("array"===d.type(g)&&g.length){a.respondTo=a.options.respondTo||"window";
            for(k in g){if(h=a.breakpoints.length-1,j=g[k].breakpoint,g.hasOwnProperty(k)){for(;
                h>=0;
            ){a.breakpoints[h]&&a.breakpoints[h]===j&&a.breakpoints.splice(h,1),h--
            }a.breakpoints.push(j),a.breakpointSettings[j]=g[k].settings
            }}a.breakpoints.sort(function(b,e){return a.options.mobileFirst?b-e:e-b
            })
        }},c.prototype.reinit=function(){var a=this;
        a.$slides=a.$slideTrack.children(a.options.slide).addClass("slick-slide"),a.slideCount=a.$slides.length,a.currentSlide>=a.slideCount&&0!==a.currentSlide&&(a.currentSlide=a.currentSlide-a.options.slidesToScroll),a.slideCount<=a.options.slidesToShow&&(a.currentSlide=0),a.registerBreakpoints(),a.setProps(),a.setupInfinite(),a.buildArrows(),a.updateArrows(),a.initArrowEvents(),a.buildDots(),a.updateDots(),a.initDotEvents(),a.checkResponsive(!1,!0),a.options.focusOnSelect===!0&&d(a.$slideTrack).children().on("click.slick",a.selectHandler),a.setSlideClasses(0),a.setPosition(),a.$slider.trigger("reInit",[a]),a.options.autoplay===!0&&a.focusHandler()
    },c.prototype.resize=function(){var a=this;
        d(window).width()!==a.windowWidth&&(clearTimeout(a.windowDelay),a.windowDelay=window.setTimeout(function(){a.windowWidth=d(window).width(),a.checkResponsive(),a.unslicked||a.setPosition()
        },50))
    },c.prototype.removeSlide=c.prototype.slickRemove=function(f,e,h){var g=this;
        return"boolean"==typeof f?(e=f,f=e===!0?0:g.slideCount-1):f=e===!0?--f:f,g.slideCount<1||0>f||f>g.slideCount-1?!1:(g.unload(),h===!0?g.$slideTrack.children().remove():g.$slideTrack.children(this.options.slide).eq(f).remove(),g.$slides=g.$slideTrack.children(this.options.slide),g.$slideTrack.children(this.options.slide).detach(),g.$slideTrack.append(g.$slides),g.$slidesCache=g.$slides,void g.reinit())
    },c.prototype.setCSS=function(g){var j,h,f=this,k={};
        f.options.rtl===!0&&(g=-g),j="left"==f.positionProp?Math.ceil(g)+"px":"0px",h="top"==f.positionProp?Math.ceil(g)+"px":"0px",k[f.positionProp]=g,f.transformsEnabled===!1?f.$slideTrack.css(k):(k={},f.cssTransitions===!1?(k[f.animType]="translate("+j+", "+h+")",f.$slideTrack.css(k)):(k[f.animType]="translate3d("+j+", "+h+", 0px)",f.$slideTrack.css(k)))
    },c.prototype.setDimensions=function(){var f=this;
        f.options.vertical===!1?f.options.centerMode===!0&&f.$list.css({padding:"0px "+f.options.centerPadding}):(f.$list.height(f.$slides.first().outerHeight(!0)*f.options.slidesToShow),f.options.centerMode===!0&&f.$list.css({padding:f.options.centerPadding+" 0px"})),f.listWidth=f.$list.width(),f.listHeight=f.$list.height(),f.options.vertical===!1&&f.options.variableWidth===!1?(f.slideWidth=Math.ceil(f.listWidth/f.options.slidesToShow),f.$slideTrack.width(Math.ceil(f.slideWidth*f.$slideTrack.children(".slick-slide").length))):f.options.variableWidth===!0?f.$slideTrack.width(5000*f.slideCount):(f.slideWidth=Math.ceil(f.listWidth),f.$slideTrack.height(Math.ceil(f.$slides.first().outerHeight(!0)*f.$slideTrack.children(".slick-slide").length)));
        var e=f.$slides.first().outerWidth(!0)-f.$slides.first().width();
        f.options.variableWidth===!1&&f.$slideTrack.children(".slick-slide").width(f.slideWidth-e)
    },c.prototype.setFade=function(){var e,a=this;
        a.$slides.each(function(f,b){e=a.slideWidth*f*-1,a.options.rtl===!0?d(b).css({position:"relative",right:e,top:0,zIndex:a.options.zIndex-2,opacity:0}):d(b).css({position:"relative",left:e,top:0,zIndex:a.options.zIndex-2,opacity:0})
        }),a.$slides.eq(a.currentSlide).css({zIndex:a.options.zIndex-1,opacity:1})
    },c.prototype.setHeight=function(){var f=this;
        if(1===f.options.slidesToShow&&f.options.adaptiveHeight===!0&&f.options.vertical===!1){var e=f.$slides.eq(f.currentSlide).outerHeight(!0);
            f.$list.css("height",e)
        }},c.prototype.setOption=c.prototype.slickSetOption=function(a,m,l){var j,h,k=this;
        if("responsive"===a&&"array"===d.type(m)){for(h in m){if("array"!==d.type(k.options.responsive)){k.options.responsive=[m[h]]
        }else{for(j=k.options.responsive.length-1;
                  j>=0;
        ){k.options.responsive[j].breakpoint===m[h].breakpoint&&k.options.responsive.splice(j,1),j--
        }k.options.responsive.push(m[h])
        }}}else{k.options[a]=m
        }l===!0&&(k.unload(),k.reinit())
    },c.prototype.setPosition=function(){var b=this;
        b.setDimensions(),b.setHeight(),b.options.fade===!1?b.setCSS(b.getLeft(b.currentSlide)):b.setFade(),b.$slider.trigger("setPosition",[b])
    },c.prototype.setProps=function(){var f=this,e=document.body.style;
        f.positionProp=f.options.vertical===!0?"top":"left","top"===f.positionProp?f.$slider.addClass("slick-vertical"):f.$slider.removeClass("slick-vertical"),(void 0!==e.WebkitTransition||void 0!==e.MozTransition||void 0!==e.msTransition)&&f.options.useCSS===!0&&(f.cssTransitions=!0),f.options.fade&&("number"==typeof f.options.zIndex?f.options.zIndex<3&&(f.options.zIndex=3):f.options.zIndex=f.defaults.zIndex),void 0!==e.OTransform&&(f.animType="OTransform",f.transformType="-o-transform",f.transitionType="OTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(f.animType=!1)),void 0!==e.MozTransform&&(f.animType="MozTransform",f.transformType="-moz-transform",f.transitionType="MozTransition",void 0===e.perspectiveProperty&&void 0===e.MozPerspective&&(f.animType=!1)),void 0!==e.webkitTransform&&(f.animType="webkitTransform",f.transformType="-webkit-transform",f.transitionType="webkitTransition",void 0===e.perspectiveProperty&&void 0===e.webkitPerspective&&(f.animType=!1)),void 0!==e.msTransform&&(f.animType="msTransform",f.transformType="-ms-transform",f.transitionType="msTransition",void 0===e.msTransform&&(f.animType=!1)),void 0!==e.transform&&f.animType!==!1&&(f.animType="transform",f.transformType="transform",f.transitionType="transition"),f.transformsEnabled=f.options.useTransform&&null!==f.animType&&f.animType!==!1
    },c.prototype.setSlideClasses=function(h){var m,l,k,j,g=this;
        l=g.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true"),g.$slides.eq(h).addClass("slick-current"),g.options.centerMode===!0?(m=Math.floor(g.options.slidesToShow/2),g.options.infinite===!0&&(h>=m&&h<=g.slideCount-1-m?g.$slides.slice(h-m,h+m+1).addClass("slick-active").attr("aria-hidden","false"):(k=g.options.slidesToShow+h,l.slice(k-m+1,k+m+2).addClass("slick-active").attr("aria-hidden","false")),0===h?l.eq(l.length-1-g.options.slidesToShow).addClass("slick-center"):h===g.slideCount-1&&l.eq(g.options.slidesToShow).addClass("slick-center")),g.$slides.eq(h).addClass("slick-center")):h>=0&&h<=g.slideCount-g.options.slidesToShow?g.$slides.slice(h,h+g.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):l.length<=g.options.slidesToShow?l.addClass("slick-active").attr("aria-hidden","false"):(j=g.slideCount%g.options.slidesToShow,k=g.options.infinite===!0?g.options.slidesToShow+h:h,g.options.slidesToShow==g.options.slidesToScroll&&g.slideCount-h<g.options.slidesToShow?l.slice(k-(g.options.slidesToShow-j),k+j).addClass("slick-active").attr("aria-hidden","false"):l.slice(k,k+g.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false")),"ondemand"===g.options.lazyLoad&&g.lazyLoad()
    },c.prototype.setupInfinite=function(){var h,g,f,a=this;
        if(a.options.fade===!0&&(a.options.centerMode=!1),a.options.infinite===!0&&a.options.fade===!1&&(g=null,a.slideCount>a.options.slidesToShow)){for(f=a.options.centerMode===!0?a.options.slidesToShow+1:a.options.slidesToShow,h=a.slideCount;
                                                                                                                                                          h>a.slideCount-f;
                                                                                                                                                          h-=1){g=h-1,d(a.$slides[g]).clone(!0).attr("id","").attr("data-slick-index",g-a.slideCount).prependTo(a.$slideTrack).addClass("slick-cloned")
        }for(h=0;
             f>h;
             h+=1){g=h,d(a.$slides[g]).clone(!0).attr("id","").attr("data-slick-index",g+a.slideCount).appendTo(a.$slideTrack).addClass("slick-cloned")
        }a.$slideTrack.find(".slick-cloned").find("[id]").each(function(){d(this).attr("id","")
        })
        }},c.prototype.setPaused=function(f){var e=this;
        e.options.autoplay===!0&&e.options.pauseOnHover===!0&&(e.paused=f,f?e.autoPlayClear():e.autoPlay())
    },c.prototype.selectHandler=function(a){var h=this,g=d(a.target).is(".slick-slide")?d(a.target):d(a.target).parents(".slick-slide"),f=parseInt(g.attr("data-slick-index"));
        return f||(f=0),h.slideCount<=h.options.slidesToShow?(h.setSlideClasses(f),void h.asNavFor(f)):void h.slideHandler(f)
    },c.prototype.slideHandler=function(r,q,p){var o,n,m,l,k=null,j=this;
        return q=q||!1,j.animating===!0&&j.options.waitForAnimate===!0||j.options.fade===!0&&j.currentSlide===r||j.slideCount<=j.options.slidesToShow?void 0:(q===!1&&j.asNavFor(r),o=r,k=j.getLeft(o),l=j.getLeft(j.currentSlide),j.currentLeft=null===j.swipeLeft?l:j.swipeLeft,j.options.infinite===!1&&j.options.centerMode===!1&&(0>r||r>j.getDotCount()*j.options.slidesToScroll)?void (j.options.fade===!1&&(o=j.currentSlide,p!==!0?j.animateSlide(l,function(){j.postSlide(o)
        }):j.postSlide(o))):j.options.infinite===!1&&j.options.centerMode===!0&&(0>r||r>j.slideCount-j.options.slidesToScroll)?void (j.options.fade===!1&&(o=j.currentSlide,p!==!0?j.animateSlide(l,function(){j.postSlide(o)
        }):j.postSlide(o))):(j.options.autoplay===!0&&clearInterval(j.autoPlayTimer),n=0>o?j.slideCount%j.options.slidesToScroll!==0?j.slideCount-j.slideCount%j.options.slidesToScroll:j.slideCount+o:o>=j.slideCount?j.slideCount%j.options.slidesToScroll!==0?0:o-j.slideCount:o,j.animating=!0,j.$slider.trigger("beforeChange",[j,j.currentSlide,n]),m=j.currentSlide,j.currentSlide=n,j.setSlideClasses(j.currentSlide),j.updateDots(),j.updateArrows(),j.options.fade===!0?(p!==!0?(j.fadeSlideOut(m),j.fadeSlide(n,function(){j.postSlide(n)
        })):j.postSlide(n),void j.animateHeight()):void (p!==!0?j.animateSlide(k,function(){j.postSlide(n)
        }):j.postSlide(n))))
    },c.prototype.startLoad=function(){var b=this;
        b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow.hide(),b.$nextArrow.hide()),b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&b.$dots.hide(),b.$slider.addClass("slick-loading")
    },c.prototype.swipeDirection=function(){var g,f,k,j,h=this;
        return g=h.touchObject.startX-h.touchObject.curX,f=h.touchObject.startY-h.touchObject.curY,k=Math.atan2(f,g),j=Math.round(180*k/Math.PI),0>j&&(j=360-Math.abs(j)),45>=j&&j>=0?h.options.rtl===!1?"left":"right":360>=j&&j>=315?h.options.rtl===!1?"left":"right":j>=135&&225>=j?h.options.rtl===!1?"right":"left":h.options.verticalSwiping===!0?j>=35&&135>=j?"left":"right":"vertical"
    },c.prototype.swipeEnd=function(f){var g,e=this;
        if(e.dragging=!1,e.shouldClick=e.touchObject.swipeLength>10?!1:!0,void 0===e.touchObject.curX){return !1
        }if(e.touchObject.edgeHit===!0&&e.$slider.trigger("edge",[e,e.swipeDirection()]),e.touchObject.swipeLength>=e.touchObject.minSwipe){switch(e.swipeDirection()){case"left":g=e.options.swipeToSlide?e.checkNavigable(e.currentSlide+e.getSlideCount()):e.currentSlide+e.getSlideCount(),e.slideHandler(g),e.currentDirection=0,e.touchObject={},e.$slider.trigger("swipe",[e,"left"]);
            break;
            case"right":g=e.options.swipeToSlide?e.checkNavigable(e.currentSlide-e.getSlideCount()):e.currentSlide-e.getSlideCount(),e.slideHandler(g),e.currentDirection=1,e.touchObject={},e.$slider.trigger("swipe",[e,"right"])
        }}else{e.touchObject.startX!==e.touchObject.curX&&(e.slideHandler(e.currentSlide),e.touchObject={})
        }},c.prototype.swipeHandler=function(f){var e=this;
        if(!(e.options.swipe===!1||"ontouchend" in document&&e.options.swipe===!1||e.options.draggable===!1&&-1!==f.type.indexOf("mouse"))){switch(e.touchObject.fingerCount=f.originalEvent&&void 0!==f.originalEvent.touches?f.originalEvent.touches.length:1,e.touchObject.minSwipe=e.listWidth/e.options.touchThreshold,e.options.verticalSwiping===!0&&(e.touchObject.minSwipe=e.listHeight/e.options.touchThreshold),f.data.action){case"start":e.swipeStart(f);
            break;
            case"move":e.swipeMove(f);
                break;
            case"end":e.swipeEnd(f)
        }}},c.prototype.swipeMove=function(k){var p,o,n,m,l,j=this;
        return l=void 0!==k.originalEvent?k.originalEvent.touches:null,!j.dragging||l&&1!==l.length?!1:(p=j.getLeft(j.currentSlide),j.touchObject.curX=void 0!==l?l[0].pageX:k.clientX,j.touchObject.curY=void 0!==l?l[0].pageY:k.clientY,j.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(j.touchObject.curX-j.touchObject.startX,2))),j.options.verticalSwiping===!0&&(j.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(j.touchObject.curY-j.touchObject.startY,2)))),o=j.swipeDirection(),"vertical"!==o?(void 0!==k.originalEvent&&j.touchObject.swipeLength>4&&k.preventDefault(),m=(j.options.rtl===!1?1:-1)*(j.touchObject.curX>j.touchObject.startX?1:-1),j.options.verticalSwiping===!0&&(m=j.touchObject.curY>j.touchObject.startY?1:-1),n=j.touchObject.swipeLength,j.touchObject.edgeHit=!1,j.options.infinite===!1&&(0===j.currentSlide&&"right"===o||j.currentSlide>=j.getDotCount()&&"left"===o)&&(n=j.touchObject.swipeLength*j.options.edgeFriction,j.touchObject.edgeHit=!0),j.options.vertical===!1?j.swipeLeft=p+n*m:j.swipeLeft=p+n*(j.$list.height()/j.listWidth)*m,j.options.verticalSwiping===!0&&(j.swipeLeft=p+n*m),j.options.fade===!0||j.options.touchMove===!1?!1:j.animating===!0?(j.swipeLeft=null,!1):void j.setCSS(j.swipeLeft)):void 0)
    },c.prototype.swipeStart=function(f){var g,e=this;
        return 1!==e.touchObject.fingerCount||e.slideCount<=e.options.slidesToShow?(e.touchObject={},!1):(void 0!==f.originalEvent&&void 0!==f.originalEvent.touches&&(g=f.originalEvent.touches[0]),e.touchObject.startX=e.touchObject.curX=void 0!==g?g.pageX:f.clientX,e.touchObject.startY=e.touchObject.curY=void 0!==g?g.pageY:f.clientY,void (e.dragging=!0))
    },c.prototype.unfilterSlides=c.prototype.slickUnfilter=function(){var b=this;
        null!==b.$slidesCache&&(b.unload(),b.$slideTrack.children(this.options.slide).detach(),b.$slidesCache.appendTo(b.$slideTrack),b.reinit())
    },c.prototype.unload=function(){var a=this;
        d(".slick-cloned",a.$slider).remove(),a.$dots&&a.$dots.remove(),a.$prevArrow&&a.htmlExpr.test(a.options.prevArrow)&&a.$prevArrow.remove(),a.$nextArrow&&a.htmlExpr.test(a.options.nextArrow)&&a.$nextArrow.remove(),a.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")
    },c.prototype.unslick=function(f){var e=this;
        e.$slider.trigger("unslick",[e,f]),e.destroy()
    },c.prototype.updateArrows=function(){var e,f=this;
        e=Math.floor(f.options.slidesToShow/2),f.options.arrows===!0&&f.slideCount>f.options.slidesToShow&&!f.options.infinite&&(f.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false"),f.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false"),0===f.currentSlide?(f.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true"),f.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false")):f.currentSlide>=f.slideCount-f.options.slidesToShow&&f.options.centerMode===!1?(f.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),f.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")):f.currentSlide>=f.slideCount-1&&f.options.centerMode===!0&&(f.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),f.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")))
    },c.prototype.updateDots=function(){var b=this;
        null!==b.$dots&&(b.$dots.find("li").removeClass("slick-active").attr("aria-hidden","true"),b.$dots.find("li").eq(Math.floor(b.currentSlide/b.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden","false"))
    },c.prototype.visibility=function(){var b=this;
        document[b.hidden]?(b.paused=!0,b.autoPlayClear()):b.options.autoplay===!0&&(b.paused=!1,b.autoPlay())
    },c.prototype.initADA=function(){var a=this;
        a.$slides.add(a.$slideTrack.find(".slick-cloned")).attr({"aria-hidden":"true",tabindex:"-1"}).find("a, input, button, select").attr({tabindex:"-1"}),a.$slideTrack.attr("role","listbox"),a.$slides.not(a.$slideTrack.find(".slick-cloned")).each(function(b){d(this).attr({role:"option","aria-describedby":"slick-slide"+a.instanceUid+b})
        }),null!==a.$dots&&a.$dots.attr("role","tablist").find("li").each(function(b){d(this).attr({role:"presentation","aria-selected":"false","aria-controls":"navigation"+a.instanceUid+b,id:"slick-slide"+a.instanceUid+b})
        }).first().attr("aria-selected","true").end().find("button").attr("role","button").end().closest("div").attr("role","toolbar"),a.activateADA()
    },c.prototype.activateADA=function(){var b=this;
        b.$slideTrack.find(".slick-active").attr({"aria-hidden":"false"}).find("a, input, button, select").attr({tabindex:"0"})
    },c.prototype.focusHandler=function(){var a=this;
        a.$slider.on("focus.slick blur.slick","*",function(e){e.stopImmediatePropagation();
            var b=d(this);
            setTimeout(function(){a.isPlay&&(b.is(":focus")?(a.autoPlayClear(),a.paused=!0):(a.paused=!1,a.autoPlay()))
            },0)
        })
    },d.fn.slick=function(){var j,h,b=this,m=arguments[0],l=Array.prototype.slice.call(arguments,1),k=b.length;
        for(j=0;
            k>j;
            j++){if("object"==typeof m||"undefined"==typeof m?b[j].slick=new c(b[j],m):h=b[j].slick[m].apply(b[j].slick,l),"undefined"!=typeof h){return h
        }}return b
    }
});
digitalData={event:[],page:{}};
/*! pace 1.0.2 */
(function(){var a1,a0,aZ,aY,aX,aW,aV,aU,aT,aS,aR,aQ,aP,aO,aN,aM,aL,aK,aJ,aI,aH,aG,aF,aE,aD,aC,aA,az,ay,ax,aw,av,au,at,ar,aq,ap,ao,an,am,al,ak,aj,ai,ah,ag,af,ae,ad,ac=[].slice,ab={}.hasOwnProperty,aa=function(f,e){function h(){this.constructor=f
}for(var g in e){ab.call(e,g)&&(f[g]=e[g])
}return h.prototype=e.prototype,f.prototype=new h,f.__super__=e.prototype,f
},aB=[].indexOf||function(e){for(var d=0,f=this.length;
                                 f>d;
                                 d++){if(d in this&&this[d]===e){return d
    }}return -1
    };
    for(aH={catchupTime:100,initialRate:0.03,minTime:250,ghostTime:100,maxProgressPerFrame:20,easeFactor:1.25,startOnPageLoad:!0,restartOnPushState:!0,restartOnRequestAfter:500,target:"body",elements:{checkInterval:100,selectors:["body"]},eventLag:{minSamples:10,sampleCount:3,lagThreshold:3},ajax:{trackMethods:["GET"],trackWebSockets:!0,ignoreURLs:[]}},ay=function(){var b;
        return null!=(b="undefined"!=typeof performance&&null!==performance&&"function"==typeof performance.now?performance.now():void 0)?b:+new Date
    },aw=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||window.msRequestAnimationFrame,aI=window.cancelAnimationFrame||window.mozCancelAnimationFrame,null==aw&&(aw=function(b){return setTimeout(b,50)
    },aI=function(b){return clearTimeout(b)
    }),au=function(e){var d,f;
        return d=ay(),(f=function(){var a;
            return a=ay()-d,a>=33?(d=ay(),e(a,function(){return aw(f)
            })):setTimeout(f,33-a)
        })()
    },av=function(){var e,d,f;
        return f=arguments[0],d=arguments[1],e=3<=arguments.length?ac.call(arguments,2):[],"function"==typeof f[d]?f[d].apply(f,e):f[d]
    },aG=function(){var j,h,o,n,m,l,k;
        for(h=arguments[0],n=2<=arguments.length?ac.call(arguments,1):[],l=0,k=n.length;
            k>l;
            l++){if(o=n[l]){for(j in o){ab.call(o,j)&&(m=o[j],null!=h[j]&&"object"==typeof h[j]&&null!=m&&"object"==typeof m?aG(h[j],m):h[j]=m)
        }}}return h
    },aL=function(h){var g,m,l,k,j;
        for(m=g=0,k=0,j=h.length;
            j>k;
            k++){l=h[k],m+=Math.abs(l),g++
        }return m/g
    },aE=function(h,g){var m,l,k;
        if(null==h&&(h="options"),null==g&&(g=!0),k=document.querySelector("[data-pace-"+h+"]")){if(m=k.getAttribute("data-pace-"+h),!g){return m
        }try{return JSON.parse(m)
        }catch(j){return l=j,"undefined"!=typeof console&&null!==console?console.error("Error parsing inline pace options",l):void 0
        }}},aV=function(){function b(){}return b.prototype.on=function(g,f,k,j){var h;
        return null==j&&(j=!1),null==this.bindings&&(this.bindings={}),null==(h=this.bindings)[g]&&(h[g]=[]),this.bindings[g].push({handler:f,ctx:k,once:j})
    },b.prototype.once=function(e,d,f){return this.on(e,d,f,!0)
    },b.prototype.off=function(g,f){var k,j,h;
        if(null!=(null!=(j=this.bindings)?j[g]:void 0)){if(null==f){return delete this.bindings[g]
        }for(k=0,h=[];
             k<this.bindings[g].length;
        ){h.push(this.bindings[g][k].handler===f?this.bindings[g].splice(k,1):k++)
        }return h
        }},b.prototype.trigger=function(){var r,q,p,o,n,m,l,k,j;
        if(p=arguments[0],r=2<=arguments.length?ac.call(arguments,1):[],null!=(l=this.bindings)?l[p]:void 0){for(n=0,j=[];
                                                                                                                 n<this.bindings[p].length;
        ){k=this.bindings[p][n],o=k.handler,q=k.ctx,m=k.once,o.apply(null!=q?q:this,r),j.push(m?this.bindings[p].splice(n,1):n++)
        }return j
        }},b
    }(),aS=window.Pace||{},window.Pace=aS,aG(aS,aV.prototype),ax=aS.options=aG({},aH,window.paceOptions,aE()),af=["ajax","document","eventLag","elements"],aj=0,ah=af.length;
        ah>aj;
        aj++){ap=af[aj],ax[ap]===!0&&(ax[ap]=aH[ap])
    }aT=function(d){function c(){return ae=c.__super__.constructor.apply(this,arguments)
    }return aa(c,d),c
    }(Error),a0=function(){function b(){this.progress=0
    }return b.prototype.getElement=function(){var c;
        if(null==this.el){if(c=document.querySelector(ax.target),!c){throw new aT
        }this.el=document.createElement("div"),this.el.className="pace pace-active",document.body.className=document.body.className.replace(/pace-done/g,""),document.body.className+=" pace-running",this.el.innerHTML='<div class="pace-progress">\n  <div class="pace-progress-inner"></div>\n</div>\n<div class="pace-activity"></div>',null!=c.firstChild?c.insertBefore(this.el,c.firstChild):c.appendChild(this.el)
        }return this.el
    },b.prototype.finish=function(){var c;
        return c=this.getElement(),c.className=c.className.replace("pace-active",""),c.className+=" pace-inactive",document.body.className=document.body.className.replace("pace-running",""),document.body.className+=" pace-done"
    },b.prototype.update=function(c){return this.progress=c,this.render()
    },b.prototype.destroy=function(){try{this.getElement().parentNode.removeChild(this.getElement())
    }catch(c){aT=c
    }return this.el=void 0
    },b.prototype.render=function(){var j,h,o,n,m,l,k;
        if(null==document.querySelector(ax.target)){return !1
        }for(j=this.getElement(),n="translate3d("+this.progress+"%, 0, 0)",k=["webkitTransform","msTransform","transform"],m=0,l=k.length;
             l>m;
             m++){h=k[m],j.children[0].style[h]=n
        }return(!this.lastRenderedProgress||this.lastRenderedProgress|0!==this.progress|0)&&(j.children[0].setAttribute("data-progress-text",""+(0|this.progress)+"%"),this.progress>=100?o="99":(o=this.progress<10?"0":"",o+=0|this.progress),j.children[0].setAttribute("data-progress",""+o)),this.lastRenderedProgress=this.progress
    },b.prototype.done=function(){return this.progress>=100
    },b
    }(),aU=function(){function b(){this.bindings={}
    }return b.prototype.trigger=function(j,h){var o,n,m,l,k;
        if(null!=this.bindings[j]){for(l=this.bindings[j],k=[],n=0,m=l.length;
                                       m>n;
                                       n++){o=l[n],k.push(o.call(this,h))
        }return k
        }},b.prototype.on=function(e,d){var f;
        return null==(f=this.bindings)[e]&&(f[e]=[]),this.bindings[e].push(d)
    },b
    }(),ak=window.XMLHttpRequest,al=window.XDomainRequest,am=window.WebSocket,aF=function(h,g){var m,l,k;
        k=[];
        for(l in g.prototype){try{k.push(null==h[l]&&"function"!=typeof g[l]?"function"==typeof Object.defineProperty?Object.defineProperty(h,l,{get:function(){return g.prototype[l]
        },configurable:!0,enumerable:!0}):h[l]=g.prototype[l]:void 0)
        }catch(j){m=j
        }}return k
    },aA=[],aS.ignore=function(){var e,d,f;
        return d=arguments[0],e=2<=arguments.length?ac.call(arguments,1):[],aA.unshift("ignore"),f=d.apply(null,e),aA.shift(),f
    },aS.track=function(){var e,d,f;
        return d=arguments[0],e=2<=arguments.length?ac.call(arguments,1):[],aA.unshift("track"),f=d.apply(null,e),aA.shift(),f
    },aq=function(d){var c;
        if(null==d&&(d="GET"),"track"===aA[0]){return"force"
        }if(!aA.length&&ax.ajax){if("socket"===d&&ax.ajax.trackWebSockets){return !0
        }if(c=d.toUpperCase(),aB.call(ax.ajax.trackMethods,c)>=0){return !0
        }}return !1
    },aR=function(d){function c(){var b,f=this;
        c.__super__.constructor.apply(this,arguments),b=function(h){var g;
            return g=h.open,h.open=function(j,a){return aq(j)&&f.trigger("request",{type:j,url:a,request:h}),g.apply(h,arguments)
            }
        },window.XMLHttpRequest=function(a){var g;
            return g=new ak(a),b(g),g
        };
        try{aF(window.XMLHttpRequest,ak)
        }catch(e){}if(null!=al){window.XDomainRequest=function(){var a;
            return a=new al,b(a),a
        };
            try{aF(window.XDomainRequest,al)
            }catch(e){}}if(null!=am&&ax.ajax.trackWebSockets){window.WebSocket=function(h,g){var j;
            return j=null!=g?new am(h,g):new am(h),aq("socket")&&f.trigger("request",{type:"socket",url:h,protocols:g,request:j}),j
        };
            try{aF(window.WebSocket,am)
            }catch(e){}}}return aa(c,d),c
    }(aU),ai=null,aD=function(){return null==ai&&(ai=new aR),ai
    },ar=function(g){var f,k,j,h;
        for(h=ax.ajax.ignoreURLs,k=0,j=h.length;
            j>k;
            k++){if(f=h[k],"string"==typeof f){if(-1!==g.indexOf(f)){return !0
        }}else{if(f.test(g)){return !0
        }}}return !1
    },aD().on("request",function(a){var m,l,k,j,h;
        return j=a.type,k=a.request,h=a.url,ar(h)?void 0:aS.running||ax.restartOnRequestAfter===!1&&"force"!==aq(j)?void 0:(l=arguments,m=ax.restartOnRequestAfter||0,"boolean"==typeof m&&(m=0),setTimeout(function(){var d,p,o,n,f,e;
            if(d="socket"===j?k.readyState<2:0<(n=k.readyState)&&4>n){for(aS.restart(),f=aS.sources,e=[],p=0,o=f.length;
                                                                          o>p;
                                                                          p++){if(ap=f[p],ap instanceof a1){ap.watch.apply(ap,l);
                break
            }e.push(void 0)
            }return e
            }},m))
    }),a1=function(){function b(){var c=this;
        this.elements=[],aD().on("request",function(){return c.watch.apply(c,arguments)
        })
    }return b.prototype.watch=function(g){var f,k,j,h;
        return j=g.type,f=g.request,h=g.url,ar(h)?void 0:(k="socket"===j?new aO(f):new aN(f),this.elements.push(k))
    },b
    }(),aN=function(){function b(k){var j,q,p,o,n,m,l=this;
        if(this.progress=0,null!=window.ProgressEvent){for(q=null,k.addEventListener("progress",function(c){return l.progress=c.lengthComputable?100*c.loaded/c.total:l.progress+(100-l.progress)/2
        },!1),m=["load","abort","timeout","error"],p=0,o=m.length;
                                                           o>p;
                                                           p++){j=m[p],k.addEventListener(j,function(){return l.progress=100
        },!1)
        }}else{n=k.onreadystatechange,k.onreadystatechange=function(){var a;
            return 0===(a=k.readyState)||4===a?l.progress=100:3===k.readyState&&(l.progress=50),"function"==typeof n?n.apply(null,arguments):void 0
        }
        }}return b
    }(),aO=function(){function b(h){var g,m,l,k,j=this;
        for(this.progress=0,k=["error","open"],m=0,l=k.length;
            l>m;
            m++){g=k[m],h.addEventListener(g,function(){return j.progress=100
        },!1)
        }}return b
    }(),aY=function(){function b(g){var e,k,j,h;
        for(null==g&&(g={}),this.elements=[],null==g.selectors&&(g.selectors=[]),h=g.selectors,k=0,j=h.length;
            j>k;
            k++){e=h[k],this.elements.push(new aX(e))
        }}return b
    }(),aX=function(){function b(c){this.selector=c,this.progress=0,this.check()
    }return b.prototype.check=function(){var c=this;
        return document.querySelector(this.selector)?this.done():setTimeout(function(){return c.check()
        },ax.elements.checkInterval)
    },b.prototype.done=function(){return this.progress=100
    },b
    }(),aZ=function(){function b(){var e,d,f=this;
        this.progress=null!=(d=this.states[document.readyState])?d:100,e=document.onreadystatechange,document.onreadystatechange=function(){return null!=f.states[document.readyState]&&(f.progress=f.states[document.readyState]),"function"==typeof e?e.apply(null,arguments):void 0
        }
    }return b.prototype.states={loading:0,interactive:50,complete:100},b
    }(),aW=function(){function b(){var h,g,m,l,k,j=this;
        this.progress=0,h=0,k=[],l=0,m=ay(),g=setInterval(function(){var a;
            return a=ay()-m-50,m=ay(),k.push(a),k.length>ax.eventLag.sampleCount&&k.shift(),h=aL(k),++l>=ax.eventLag.minSamples&&h<ax.eventLag.lagThreshold?(j.progress=100,clearInterval(g)):j.progress=100*(3/(h+3))
        },50)
    }return b
    }(),aP=function(){function b(c){this.source=c,this.last=this.sinceLastUpdate=0,this.rate=ax.initialRate,this.catchup=0,this.progress=this.lastProgress=0,null!=this.source&&(this.progress=av(this.source,"progress"))
    }return b.prototype.tick=function(e,d){var f;
        return null==d&&(d=av(this.source,"progress")),d>=100&&(this.done=!0),d===this.last?this.sinceLastUpdate+=e:(this.sinceLastUpdate&&(this.rate=(d-this.last)/this.sinceLastUpdate),this.catchup=(d-this.progress)/ax.catchupTime,this.sinceLastUpdate=0,this.last=d),d>this.progress&&(this.progress+=this.catchup*e),f=1-Math.pow(this.progress/100,ax.easeFactor),this.progress+=f*this.rate*e,this.progress=Math.min(this.lastProgress+ax.maxProgressPerFrame,this.progress),this.progress=Math.max(0,this.progress),this.progress=Math.min(100,this.progress),this.lastProgress=this.progress,this.progress
    },b
    }(),ao=null,at=null,aK=null,an=null,aM=null,aJ=null,aS.running=!1,aC=function(){return ax.restartOnPushState?aS.restart():void 0
    },null!=window.history.pushState&&(ag=window.history.pushState,window.history.pushState=function(){return aC(),ag.apply(window.history,arguments)
    }),null!=window.history.replaceState&&(ad=window.history.replaceState,window.history.replaceState=function(){return aC(),ad.apply(window.history,arguments)
    }),aQ={ajax:a1,elements:aY,document:aZ,eventLag:aW},(az=function(){var b,p,o,n,m,l,k,j;
        for(aS.sources=ao=[],l=["ajax","elements","document","eventLag"],p=0,n=l.length;
            n>p;
            p++){b=l[p],ax[b]!==!1&&ao.push(new aQ[b](ax[b]))
        }for(j=null!=(k=ax.extraSources)?k:[],o=0,m=j.length;
             m>o;
             o++){ap=j[o],ao.push(new ap(ax))
        }return aS.bar=aK=new a0,at=[],an=new aP
    })(),aS.stop=function(){return aS.trigger("stop"),aS.running=!1,aK.destroy(),aJ=!0,null!=aM&&("function"==typeof aI&&aI(aM),aM=null),az()
    },aS.restart=function(){return aS.trigger("restart"),aS.stop(),aS.start()
    },aS.go=function(){var b;
        return aS.running=!0,aK.render(),b=ay(),aJ=!1,aM=au(function(J,I){var H,G,F,E,D,C,B,A,z,y,x,s,r,m,j,a;
            for(A=100-aK.progress,G=x=0,F=!0,C=s=0,m=ao.length;
                m>s;
                C=++s){for(ap=ao[C],y=null!=at[C]?at[C]:at[C]=[],D=null!=(a=ap.elements)?a:[ap],B=r=0,j=D.length;
                           j>r;
                           B=++r){E=D[B],z=null!=y[B]?y[B]:y[B]=new aP(E),F&=z.done,z.done||(G++,x+=z.tick(J))
            }}return H=x/G,aK.update(an.tick(J,H)),aK.done()||F||aJ?(aK.update(100),aS.trigger("done"),setTimeout(function(){return aK.finish(),aS.running=!1,aS.trigger("hide")
            },Math.max(ax.ghostTime,Math.max(ax.minTime-(ay()-b),0)))):I()
        })
    },aS.start=function(d){aG(ax,d),aS.running=!0;
        try{aK.render()
        }catch(c){aT=c
        }return document.querySelector(".pace")?(aS.trigger("start"),aS.go()):setTimeout(aS.start,50)
    },"function"==typeof define&&define.amd?define(["pace"],function(){return aS
    }):"object"==typeof exports?module.exports=aS:ax.startOnPageLoad&&aS.start()
}).call(this);
jQuery(window).load(function(){if($(".related-links img").is(":visible")){$(".related-links img").each(function(b){var a=$(this).width();
    var c=$(this).height();
    $(this).parents(".related-links").find(".image-comp-caption-button").css("width",a);
    $(this).parents(".related-links").find(".image-button, .image-label-title").css("margin-top","-"+(parseInt(c)/2)+"px")
});
    $(".related-links img").attrchange({trackValues:false,callback:function(b){var a=$(this).width();
        $(this).parents(".related-links").find(".image-comp-caption-button").css("width",a)
    }})
}});
$(document).ready(function(){$(".back-to-top").on("click",function(a){a.preventDefault();
    $("html,body").animate({scrollTop:0},700)
})
});
digitalData={event:[],page:{}};
$("document").ready(function(){var a=1;
    $("#year,#month,#day").change(function(){var j=new Date();
        var g=j.getMonth()+1;
        var o=j.getDate();
        var p=(new Date).getFullYear();
        var h=$("#day").val();
        var n=$("#month").val();
        var l=$("#year").val();
        console.log(h+"/"+n+"/"+l);
        $("#visit-err").remove();
        var k="<div class='error' id='visit-err'>Great Scott! You can't visit us from the future.</div>";
        if(l>p){$(".input-wrapper-third-wrapper").append(k)
        }else{if(l==p){$("#visit-err").remove();
            if(n>g){$(".input-wrapper-third-wrapper").append(k)
            }else{if(n==g){$("#visit-err").remove();
                if(h>o){$(".input-wrapper-third-wrapper").append(k)
                }}}}}});
    $(".location-finder button").click(function(j){j.preventDefault();
        var g=parseInt($("#search").val().length);
        if(a<=1){console.log("length in block1:"+g);
            if(g<5&&g>0){console.log("counter in block1:"+a);
                var h="<div class='error store-error-message'>Zip code must be 5 numbers long.</div>";
                $(h).insertAfter(".location-finder button")
            }a++
        }else{if(a>1){console.log("length in block2:"+g);
            $(".store-error-message").remove();
            if(g<5&&g>0){console.log("counter in block2:"+a);
                var h="<div class='error store-error-message'>Zip code must be 5 numbers long.</div>";
                $(h).insertAfter(".location-finder button")
            }if(g>5){console.log("counter in block3:"+a);
                var h="<div class='error store-error-message'>Zip code must be 5 numbers long.</div>";
                $(h).insertAfter(".location-finder button")
            }}}});
    $("#search").keyup(function(){console.log("Handler for .keypress() called."+$(this).val().length);
        if($(this).val().length==5){a=1;
            $(".store-error-message").remove();
            $("#error-no-store").remove();
            clearTimeout($.data(this,"timer"));
            var g=setTimeout(e,100);
            $(".button-outline-black").text("Loading...");
            $(this).data("timer",g)
        }});
    function e(){var g="https://fdpapi.fridays.com/tgifwebservices/oauth/token?client_id=client-side&grant_type=client_credentials&client_secret=secret";
        $.ajax({type:"POST",url:g,success:function(h){authToken=h.access_token;
            console.log("authToken : "+authToken);
            c(authToken)
        }})
    }function c(g){var h=$("#search").val();
        $.ajax({type:"GET",url:"https://fdpapi.fridays.com/tgifwebservices/v2/tgif/stores?pageSize=3&query="+h,dataType:"json",beforeSend:function(j){j.setRequestHeader("Authorization","bearer "+g);
            $(".button-outline-black").text("Loading...")
        },async:false,success:function(j){console.log(j);
            $(".locations").html("");
            if(j.stores!=undefined){$.each(j.stores,function(m,o){console.log("value:"+j.stores[0].displayName);
                var k=j.stores[m].address.region.isocode;
                var n=k.split("-");
                var l="<li>";
                l+="<input type='radio' value="+j.stores[m].name+" name='storeCode' id="+j.stores[m].name+">";
                l+="<label for="+j.stores[m].name+">";
                l+="<h3 class='location-name'>"+j.stores[m].displayName+"</h3>";
                l+="<p class='location-address'>";
                l+="<span>"+j.stores[m].address.line1+"</span>";
                l+="<span>"+j.stores[m].address.town+", "+n[1]+" "+j.stores[m].address.postalCode+"</span>";
                l+="</p>";
                l+="</label>";
                l+="</li>";
                $(".locations").append(l)
            })
            }if(j.pagination.totalResults==0){$("<div class='error store-error-message' id='error-no-store'>No nearby locations found.</div>").insertAfter(".location-finder button")
            }$(".button-outline-black").text("Find Fridays")
        },error:function(j){console.log("error:");
            $(".button-outline-black").text("Find Fridays");
            $("<div class='error store-error-message'>Please check your network connectivity and try again.</div>").insertAfter(".location-finder button")
        }})
    }var f=2000;
    $("#amountLeft").html(f+" characters left");
    $("#your-comment").keyup(function(){var g=$("#your-comment").val().length;
        var h=f-g;
        $("#amountLeft").html(h+" characters left")
    });
    $("#contact-us-form input").change(function(){if($(this).val()){$(this).addClass("has-value")
    }else{$(this).removeClass("has-value")
    }});
    $("#contact-us-form #commenttype").change(function(){if($(this).val()){$(this).parents(".select-wrapper.select-half").removeClass("error")
    }});
    $(".input-wrapper .select-wrapper #statecode").show();
    $(".input-wrapper .select-wrapper #countrycode").change(function(){if($(this).val()=="USA"){$(".input-wrapper .select-wrapper #statecode").parents(".input-wrapper").show()
    }else{$(".input-wrapper .select-wrapper #statecode").parents(".input-wrapper").hide()
    }});
    $("#contact-us-form").validate({rules:{firstname:"required",lastname:"required",email:{required:true,email:true},phone:{phoneUS:true},postalcode:{required:false,minlength:5,maxlength:5},commenttype:"required",comment:"required"},errorPlacement:function(g,h){if(h.attr("name")=="commenttype"){g.insertAfter(".select-wrapper.select-half");
        $(h).parents(".select-wrapper.select-half").addClass("error");
        $(h).removeClass("error")
    }else{g.insertAfter(h)
    }},messages:{firstname:"What should we call you?",lastname:"Name on the back of your jersey?",email:{email:"Oops, that didn’t work. How about a valid email address?",required:"What’s your email address?"},phone:"Please enter a valid phone number.",commenttype:"Please select your comment type.",comment:"Please enter your comment",postalcode:{minlength:"Please enter at least 5 characters.",maxlength:"Please enter no more than 5 characters."}},submitHandler:function(g){event.preventDefault();
        customerData={customerAddress:{titleCode:$("#titlecode").val(),firstName:$("#firstname").val(),lastName:$("#lastname").val(),email:$("#email").val(),phone:$("#phone").val(),line1:$("#address-one").val(),line2:$("#address-two").val(),city:$("#city").val(),postalCode:$("#zip").val(),countryCode:$("#countrycode").val(),stateCode:$("#statecode").val()},storeCode:$("input[name=storeCode]").val(),commentType:$("#commenttype").val(),comment:$("#your-comment").val()};
        accesstokenUrl="https://fdpapi.fridays.com/tgifwebservices/oauth/token?client_id=client-side&grant_type=client_credentials&client_secret=secret";
        b(accesstokenUrl,customerData)
    }});
    function b(g,h){$.ajax({type:"POST",url:g,success:function(j){authToken1=j.access_token;
        console.log("authToken1 : "+authToken1);
        d(authToken1,h)
    }})
    }function d(g,h){$.ajax({type:"POST",url:"https://fdpapi.fridays.com/tgifwebservices/v2/tgif/contactus/send",contentType:"application/json",data:JSON.stringify(h),beforeSend:function(j){j.setRequestHeader("Authorization","bearer "+g);
        $(".submit-button").text("Loading...")
    },success:function(j){$("#contact-us-form")[0].reset();
        console.log("success: data sent");
        $(".submit-button").text("Submit");
        $("#error-success").css("display","inline-block")
    },error:function(j){console.log("error:");
        $(".submit-button").text("Submit");
        $("#submitError").css("display","inline-block")
    }})
    }});
$("#menuToggle").on("click",function(){$("body").toggleClass("mobile-menu-open")
});
jQuery(window).load(function(){$(".nav-links li a").removeClass("is-active");
    var b=window.location.pathname.split("/");
    var a=b[b.length-1];
    a=a.split(".");
    a=a[0];
    a=a.charAt(0).toUpperCase()+a.substr(1);
    var c=a.indexOf("-")!==-1?true:false;
    a=c?a.split("-")[0]:a;
    $("."+a).addClass("is-active")
});
$(document).ready(function(){var h=localStorage.getItem("local-cart");
    var e;
    var c=localStorage.getItem("oo-location-info");
    if(c!=null&&c!=""){var g=atob(c);
        var d=decodeURIComponent(g);
        var j=$.parseJSON(d);
        $(".location-nav-info .shop-name").html(j.displayName);
        $(".location-nav-info").removeClass("hidden")
    }else{$(".location-nav-info").addClass("hidden")
    }if(h!=null&&h!=""){if(h.indexOf(",")!=-1){e=h.split(",");
        var f=0;
        $.each(e,function(k,n){var m=$.parseJSON(localStorage.getItem("local-cart-"+n));
            if(m!=null){var l=parseInt(m.quantity);
                f=f+l
            }});
        $("#NavCart").find(".cart-count").html(f)
    }else{var a=localStorage.getItem("local-cart-"+h);
        var b=$.parseJSON(a);
        if(b!=null){$("#NavCart").find(".cart-count").html(b.quantity)
        }}$("#NavCart").find(".cart-nav-info").removeClass("hidden")
    }else{$("#NavCart").find(".cart-nav-info").addClass("hidden")
    }});
$(".jsHeaderNavAnalytics").on("click",function(b){var a=$(b.currentTarget),d=a.html().toLowerCase(),c="header";
    digitalData.event.push({eventInfo:{eventName:"Navigate",eventAction:"navigate",timeStamp:new Date()},attributes:{name:d,location:c}});
    _satellite.track("onNavigate")
});
$("#menuToggle").on("click",function(){digitalData.event.push({eventInfo:{eventName:"Menu Expansion",eventAction:"menuExpansion",timeStamp:new Date()}});
    _satellite.track("onMenuExpansion")
});
$(".jsHeaderNavAnalytics").on("click",function(c){var b=$(this).attr("href");
    var a=b.split("/").pop().toLowerCase();
    if(a.indexOf("menu")!=-1){digitalData.event.push({eventInfo:{eventName:"Order Online Click",eventAction:"orderOnlineClick",timeStamp:new Date()}});
        _satellite.track("onOrderOnlineClick")
    }});
$(".jsFooterNavAnalytics").on("click",function(b){var a=$(b.currentTarget),d=a.html().toLowerCase(),c="footer";
    digitalData.event.push({eventInfo:{eventName:"Navigate",eventAction:"navigate",timeStamp:new Date()},attributes:{name:d,location:c}});
    _satellite.track("onNavigate")
});
$.urlParam=function(a){var b=new RegExp("[?&]"+a+"=([^&#]*)").exec(window.location.href);
    if(b==null){return null
    }else{return b[1]||0
    }};
jQuery(window).load(function(){var a=$.urlParam("tab");
    if(a!=null){$(".tabs").each(function(){if($("#"+a).is(":visible")){$(this).find(".tab-content .tab-pane").removeClass("active");
        $(this).find("#"+a).addClass("active");
        $(this).find("li").each(function(){if($(this).data("href")==a){$(".nav.nav-tabs li").removeClass("active");
            $(this).addClass("active")
        }})
    }})
    }});
$(window).load(function(){if($(window).width()>767){var a=$(".list .subpage-third-wrapper  .subpage-third a");
    a.removeAttr("style");
    setMaxHeight(a);
    var b=$(".list .subpage-third-wrapper  .subpage-third .site-copy-centered-bw");
    b.removeAttr("style");
    setMaxHeight(b)
}});
$(window).resize(function(){var a=$(".list .subpage-third-wrapper  .subpage-third a");
    a.removeAttr("style");
    var b=$(".list .subpage-third-wrapper  .subpage-third .site-copy-centered-bw");
    b.removeAttr("style");
    if($(window).width()>767){setMaxHeight(a);
        setMaxHeight(b)
    }});
function setMaxHeight(b){for(var c=0;
                             c<b.length;
                             c+=3){var d=b.slice(c,c+3),a=Math.max(d.eq(0).outerHeight(),d.eq(1).outerHeight());
    a=Math.max(a,d.eq(2).outerHeight());
    d.css("min-height",a)
}}function viewport(){var c=window,b="inner";
    if(!("innerWidth" in window)){b="client";
        c=document.documentElement||document.body
    }return{width:c[b+"Width"],height:c[b+"Height"]}
}$(window).resize(function(){$(".col-lg-4.col-md-4.col-sm-4.col-xs-4.tile9x1").each(function(b){var a=parseInt($(this).width());
    $(".tile9x1").css({height:a+"px"});
    $(".tile9x2").css({height:(a*2)+"px"});
    $(".tile9x3").css({height:(a*3)+"px"})
});
    $(".new-layout .col-lg-6.col-md-6.col-sm-6.col-xs-12.tile9x3").each(function(b){var a=parseInt($(this).width());
        console.log(a+"::"+$(this).find(".tile-container  img").attr("src"));
        var d=$(this).parent(".new-layout");
        d.find(".tile9x3").css({height:a+"px"});
        d.find(".tile9x1half").css({height:(a/2)+"px"});
        var c=viewport().width;
        if(c>767){$(".tile-full").css({height:(a*0.75)+"px"})
        }else{$(".tile-full").css({height:a+"px"})
        }})
});
$(window).load(function(){$(".col-lg-4.col-md-4.col-sm-4.col-xs-4.tile9x1").each(function(b){var a=parseInt($(this).width());
    $(".tile9x1").css({height:a+"px"});
    $(".tile9x2").css({height:(a*2)+"px"});
    $(".tile9x3").css({height:(a*3)+"px"})
});
    $(".new-layout .col-lg-6.col-md-6.col-sm-6.col-xs-12.tile9x3").each(function(b){var a=parseInt($(this).width());
        console.log(a+"::"+$(this).find(".tile-container  img").attr("src"));
        var d=$(this).parent(".new-layout");
        d.find(".tile9x3").css({height:a+"px"});
        d.find(".tile9x1half").css({height:(a/2)+"px"});
        var c=viewport().width;
        if(c>767){$(".tile-full").css({height:(a*0.75)+"px"})
        }else{$(".tile-full").css({height:a+"px"})
        }})
});
$(".tilelayoutwrapper").find("a").on("click",function(d){digitalData.event.push({eventInfo:{eventName:"Billboard Click",eventAction:"billboardClick",timeStamp:new Date()}});
    _satellite.track("onBillboardClick");
    var b=$(d.currentTarget),a=b.attr("href")||"",c;
    if(a.indexOf(".twitter.com")>-1){c="Twitter"
    }else{if(a.indexOf(".facebook.com")>-1){c="Facebook"
    }else{if(a.indexOf(".instagram.com")>-1){c="Twitter"
    }}}if(null!=c){digitalData.event.push({eventInfo:{eventName:"Social Network Icon Click",eventAction:"socialNetworkIconClick",timeStamp:new Date()},attributes:{networkName:"Instagram"}});
        _satellite.track("onSocialNetworkClick")
    }});
$(window).load(function(){$(".tile-container img").each(function(b){var a=$(this).width();
    var e=$(this).height();
    $(this).parents(".tile-container").find(".tile-comp-caption").css("width",a);
    var d=$(this).parents(".tile-container").find(".tile-comp-caption").height();
    if(d){d=d>e?e:d;
        var c=e-d;
        $(this).parents(".tile-container").find(".tile-comp-caption").css("height",d);
        $(this).parents(".tile-container").find(".tile-title").css("margin-top","-"+(parseInt(c/2)+d+"px"));
        console.log(a+"->"+e+"->"+d+"->"+c)
    }})
});
$(".order-online-menu").on("click",function(a){digitalData.event.push({eventInfo:{eventName:"Order Online Click",eventAction:"orderOnlineClick",timeStamp:new Date()}});
    _satellite.track("onOrderOnlineClick")
});
$(window).load(function(){$(".faq-item-header").click(function(){var c=$(this).siblings(".faq-tout").hasClass("is-active");
    if(!c){a($(this).find(".jsPlus"))
    }else{b($(this).find(".jsMinus"))
    }$(this).parents(".faq-item-list").find(".faq-tout").each(function(){if(!($(this).hasClass("is-active"))){$(".jsCollapse").removeClass("is-active");
        $(".jsCollapse").siblings(".jsExpand").addClass("is-active");
        return false
    }})
});
    $(".jsExpand").click(function(){$(this).removeClass("is-active");
        $(this).siblings(".jsCollapse").addClass("is-active");
        $(".faq-item-list .jsPlus").each(function(){a(this)
        })
    });
    $(".jsCollapse").click(function(){$(this).removeClass("is-active");
        $(this).siblings(".jsExpand").addClass("is-active");
        $(".faq-item-list .jsMinus").each(function(){b(this)
        })
    });
    function a(c){$(c).removeClass("is-active");
        $(c).siblings(".jsMinus").addClass("is-active");
        $(c).parents(".faq-item-header").siblings(".faq-tout").addClass("is-active");
        $(".jsExpand").removeClass("is-active");
        $(".jsExpand").siblings(".jsCollapse").addClass("is-active")
    }function b(c){$(c).removeClass("is-active");
        $(c).siblings(".jsPlus").addClass("is-active");
        $(c).parents(".faq-item-header").siblings(".faq-tout").removeClass("is-active");
        $(".jsCollapse").removeClass("is-active");
        $(".jsCollapse").siblings(".jsExpand").addClass("is-active")
    }});
$(window).load(function(){$(".tile-carousel").slick({arrows:false,dots:true,autoplay:true,autoplaySpeed:4000,speed:330})
});
$(document).ready(function(){var a=$(".component-banner").data("imagepath");
    $(".component-banner-bk").css("background-image","url('"+a+"')");
    var b=$(".component-banner").data("gradient");
    if(b!="none"){$(".component-banner-bk").css("opacity",".5")
    }});
$(window).load(function(){var c=new Date().getFullYear().toString();
    var a=c.slice(2,4);
    for(i=0;
        i<5;
        i++){var b="<option value="+a+">"+c+"</option>";
        $(".year-value").append(b);
        a--;
        c--
    }$(".icon-close").click(function(){$("#modal").hide();
        $("body").removeClass("no-scroll")
    });
    $(".press-modal-ul .press-modal-li a").on("click",function(k){if($(this).attr("href")!=window.location.href){k.preventDefault();
        var j=$(this).parent().parent().parent(".press-modal-ul").data("nodepath");
        var g=$(this).attr("href");
        var l=j+".newsarticle.html?articlePath="+g;
        var d=$(this).parent().parent("li").find(".press-modal-content");
        var h=$(this).parent().parent("li").parent().parent(".newsarticlelist").find(".press-release-modal");
        if(d.html().length>0){h.find(".inner-content").html(d.html());
            h.show();
            $("body").addClass("no-scroll")
        }else{var f=$(this).attr("href");
            $.get(f).then(function(m){var e=$("<div />").html(m);
                h.find(".inner-content").html(e.find(".desktop-view").html());
                h.show();
                $("body").addClass("no-scroll")
            })
        }}})
});
var totalFilteredResults=0;
$(document).ready(function(){var a=$(".year-value").val();
    getResult(a);
    $(".search-button").click(function(){var b=$(".search-term").val();
        var c=$(".year-value").val();
        getResults(b,c)
    });
    $(".year-value").change(function(){var b=$(".year-value").val();
        getResult(b)
    });
    $("#viewMore").click(function(){var c="";
        var b=$(".press-releases > li").length;
        var d=0;
        if(b+5>=totalFilteredResults.length){d=totalFilteredResults.length-b;
            d=d+b;
            $("#viewMore").hide()
        }else{console.log(totalFilteredResults.length);
            d=5
        }for(i=b;
             i<d;
             i++){c=c+"<li><div class='pr-date site-copy-grey'>"+totalFilteredResults[i].formattedPublishDate+"</div><div class='pr-title press-modal-li'> <a href="+totalFilteredResults[i].link+">"+totalFilteredResults[i].description+"</a></div><div class='pr-articlecontent press-modal-content'></div></li>"
        }$(".press-releases").append(c)
    })
});
function getResult(a){$.ajax({url:"/bin/get/articles/bydate",type:"GET",dataType:"json",data:{year:a},success:function(b){$(".press-releases").empty();
    if(b.length!=0){console.log(b);
        totalFilteredResults=b;
        var c="";
        var d=totalFilteredResults.length<5?totalFilteredResults.length:5;
        for(i=0;
            i<d;
            i++){c=c+"<li><div class='pr-date site-copy-grey'>"+totalFilteredResults[i].formattedPublishDate+"</div><div class='pr-title press-modal-li'> <a href="+totalFilteredResults[i].link+">"+totalFilteredResults[i].description+"</a></div><div class='pr-articlecontent press-modal-content'></div></li>"
        }$(".press-releases").append(c);
        if(totalFilteredResults.length>5){$("#viewMore").show()
        }else{$("#viewMore").hide()
        }}else{$(".press-releases").append("<h3>No Results found</h3>")
    }},error:function(b){console.log("error")
}})
}function getResults(a,b){$.ajax({url:"/bin/get/articles/bydate",type:"GET",dataType:"json",data:{year:b,q:a},success:function(c){$(".press-releases").empty();
    if(c.length!=0){console.log(c);
        totalFilteredResults=c;
        var d="";
        var e=totalFilteredResults.length<5?totalFilteredResults.length:5;
        for(i=0;
            i<e;
            i++){d=d+"<li><div class='pr-date site-copy-grey'>"+totalFilteredResults[i].formattedPublishDate+"</div><div class='pr-title press-modal-li'> <a href="+totalFilteredResults[i].link+">"+totalFilteredResults[i].description+"</a></div><div class='pr-articlecontent press-modal-content'></div></li>"
        }$(".press-releases").append(d);
        if(totalFilteredResults.length>5){$("#viewMore").show()
        }else{$("#viewMore").hide()
        }}else{$(".press-releases").append("<h3>No Results found</h3>")
    }},error:function(c){alert("something went wrong")
}})
}$(document).ready(function(){var a=$(".franchise-hero-wrapper").data("imagepath");
    $(".franchise-hero-wrapper").css("background-image","url('"+a+"')")
});
$(window).load(function(){alignAssets()
});
$(".asset-search").on("click",function(a){loadAssets()
});
$(".asset-types").change(function(a){loadAssets()
});
$(".asset-detail").on("click",function(a){openModal($(this))
});
$(".asset-share .modal-close").on("click",function(a){closeModal()
});
$(".view-more a").on("click",function(a){a.preventDefault();
    viewMore()
});
function openModal(a){var b=a.find("img").attr("src");
    $(".asset-share .left-content").html("<img src="+b+">");
    $(".asset-share .asset-title").html(a.data("title"));
    $(".asset-share .asset-description").html(a.data("description"));
    $(".asset-share .published-date span").html(a.data("publisheddate"));
    $(".asset-share .contributor span").html(a.data("contributor"));
    $(".asset-share .asset-path").html(a.data("assetpath"));
    var c=$(".asset-share #modal");
    c.show()
}function closeModal(){$(".asset-share .left-content").html("");
    $(".asset-share .asset-title").html("");
    $(".asset-share .asset-description").html("");
    $(".asset-share .published-date span").html("");
    $(".asset-share .contributor span").html("");
    var a=$(".asset-share #modal");
    a.hide()
}function viewMore(){var b=$(".asset-types").val();
    var a=$("input[name='assetsearch']").val();
    var d=$(".asset-display ul li").size();
    var c=$(".assetsublist-rendition").data("rendition")+"?assetType="+b+"&fullText="+a+"&currentLimit="+d;
    $(".load-more").load(c,function(f,g,e){if(f.trim().length===0){$(".no-results").show();
        $(".view-more").hide();
        $(window).scrollTop($(".no-results").offset().top)
    }$(".assetsearch-result ul").append(f);
        $(window).scrollTop($(".view-more").offset().top-300);
        $(".asset-detail").on("click",function(h){openModal($(this))
        });
        $(".asset-share .modal-close").on("click",function(h){closeModal()
        })
    })
}function alignAssets(){$(".asset-display ul li").addClass("col-md-3 col-xs-6");
    $(".asset-display ul li").first().removeClass("col-md-3 col-xs-6");
    $(".asset-display ul li").first().addClass("col-md-6 col-xs-6")
}function loadAssets(){var c=$(".asset-types").val();
    var b=$("input[name='assetsearch']").val();
    var a=$(".asset-rendition").data("rendition")+"?assetType="+c+"&fullText="+b;
    $(".assetsearch-result").load(a,function(e,f,d){$(".assetsearch-result").html(e);
        alignAssets();
        $(".no-results").hide();
        $(".view-more").show();
        $(".asset-detail").on("click",function(g){openModal($(this))
        });
        $(".asset-share .modal-close").on("click",function(g){closeModal()
        });
        $(".view-more a").on("click",function(g){g.preventDefault();
            viewMore()
        });
        $(".asset-print a").on("click",function(h){h.preventDefault();
            var g=$(this).parent().parent().parent();
            printAsset(g)
        });
        $(".share-asset").hover(shareAsset,shareAsset_hide);
        $(".asset-download a").on("click",function(g){g.preventDefault();
            downloadAsset($(this))
        })
    })
}$(".asset-download a").on("click",function(a){a.preventDefault();
    downloadAsset($(this))
});
$(".asset-print a").on("click",function(b){b.preventDefault();
    var a=$(this).parent().parent().parent();
    printAsset(a)
});
$(".share-asset").hover(shareAsset,shareAsset_hide);
function downloadAsset(a){var c=a.parent().parent().parent().find(".asset-path").html();
    try{var b=[{data:{damPath:c}}];
        var f=new CQ.wcm.LicenseBox(b);
        if(f.licensable){f.on("beforesubmit",function(){CQ_dam_editorAction_download_after_license_agreement(c)
        });
            f.show()
        }else{CQ_dam_editorAction_download_after_license_agreement(c)
        }}catch(d){}}function CQ_dam_editorAction_download_after_license_agreement(c){try{var a=CQ.HTTP.externalize(CQ.HTTP.encodePath(c)+".assetdownload.zip",true);
    a=CQ.HTTP.addParameter(a,"_charset_","utf-8");
    CQ.shared.Util.open(a,null,"AssetDownloadWindow")
}catch(b){}}function printAsset(b){var a=window.open();
    a.document.write(b.html());
    a.print();
    a.close()
}function shareAsset(){$(".modal").css("visibility","visible")
}function shareAsset_hide(){$(".modal").css("visibility","hidden")
}$(".fact-sheet-download a").on("click",function(a){a.preventDefault();
    downloadFn($(this))
});
function downloadFn(a){var c=a.attr("href");
    if(c!=undefined&&c!=null){try{var b=[{data:{damPath:c}}];
        var f=new CQ.wcm.LicenseBox(b);
        if(f.licensable){f.on("beforesubmit",function(){CQ_dam_editorAction_download_after_license_agreement(c)
        });
            f.show()
        }else{CQ_dam_editorAction_download_after_license_agreement(c)
        }}catch(d){}}};