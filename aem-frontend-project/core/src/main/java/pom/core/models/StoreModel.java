package pom.core.models;

/**
 * Created by tommymiller on 1/18/17.
 */
public class StoreModel {

    String storeId;
    String addressTown;
    String addressLine1;
    String addressZip;
    Boolean canPurchase;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getAddressTown() {
        return addressTown;
    }

    public void setAddressTown(String addressTown) {
        this.addressTown = addressTown;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressZip() {
        return addressZip;
    }

    public void setAddressZip(String addressZip) {
        this.addressZip = addressZip;
    }

    public Boolean getCanPurchase() {
        return canPurchase;
    }

    public void setCanPurchase(Boolean canPurchase) {
        this.canPurchase = canPurchase;
    }
}
