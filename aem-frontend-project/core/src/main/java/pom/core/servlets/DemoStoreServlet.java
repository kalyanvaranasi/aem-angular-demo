/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package pom.core.servlets;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Servlet that writes some sample content into the response. It is mounted for
 * all resources of a specific Sling resource type. The
 * {@link org.apache.sling.api.servlets.SlingSafeMethodsServlet} shall be used for HTTP methods that are
 * idempotent. For write operations use the {@link org.apache.sling.api.servlets.SlingAllMethodsServlet}.
 */
@SuppressWarnings("serial")
@SlingServlet(generateComponent = true, description = "handles all JSON requests for Product Store Demo.", paths = "/bin/fridays-commerce/locations", methods = "GET", extensions = "json", metatype = true)

public class DemoStoreServlet extends SlingSafeMethodsServlet {
    private static final Logger log = LoggerFactory.getLogger(DemoStoreServlet.class);

    final String COMMERCE_LOCATIONS_PATH = "/etc/fridays-commerce/locations";

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Override
    protected void doGet(final SlingHttpServletRequest request,
                         final SlingHttpServletResponse response) throws ServletException, IOException {

        List<Resource> locationList = new ArrayList();

        ResourceResolver resourceResolver = null;
        resolverFactory = null;
        Resource storeResource = null;

        Map<String, Object> param;

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");


        String action = request.getParameter("storeId");

        resourceResolver = request.getResourceResolver();

        Resource resource = resourceResolver.getResource(COMMERCE_LOCATIONS_PATH);
        Iterator<Resource> it = resource.getChildren().iterator();


        if (action == null) {
            while (it.hasNext()) {
                Resource location = it.next();
                locationList.add(location);
            }

        } else {
            List<Resource> tempStoreList = new ArrayList();
            log.debug("getting list for specific store!");

            while (it.hasNext()) {
                Resource product = it.next();
                tempStoreList.add(product);
            }

            for (Resource item : tempStoreList) {
                String value = item.getValueMap().get("store_id", String.class);

                if (value.equals(action)) {
                    locationList.add(item);
                    break;
                }
            }

        }


        response.getWriter().write(createLocationResponse(locationList).toString());
    }

    private JSONObject createLocationResponse(List<Resource> resources) {
        final JSONArray jsonArray = new JSONArray();
        JSONObject locations = new JSONObject();


        for (Resource resource : resources) {
            JSONObject jsonObject = new JSONObject();

            ValueMap values = resource.adaptTo(ValueMap.class);
            String address = values.get("address_line1", String.class);
            String town = values.get("address_town", String.class);
            String zip = values.get("address_zip", String.class);
            Boolean canPurchase = values.get("canPurchase", Boolean.class);
            String storeId = values.get("store_id", String.class);



            try {
                jsonObject.put("address_line1", address);
                jsonObject.put("address_town", town);
                jsonObject.put("address_zip", zip);
                jsonObject.put("canPurchase", canPurchase);
                jsonObject.put("store_id", storeId);

                log.debug("testing {}", jsonObject.toString());

                jsonArray.put(jsonObject);

                locations.put("locations", jsonArray);



            } catch (JSONException e ) {
                log.debug("Error.");
            }

        }



        return locations;
    }
}
