/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package pom.core.servlets;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.json.jcr.JsonItemWriter;
import org.apache.sling.commons.json.jcr.JsonJcrNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pom.core.models.ProductModel;
import pom.core.models.StoreModel;
import sun.rmi.runtime.Log;


import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Servlet that writes some sample content into the response. It is mounted for
 * all resources of a specific Sling resource type. The
 * {@link SlingSafeMethodsServlet} shall be used for HTTP methods that are
 * idempotent. For write operations use the {@link SlingAllMethodsServlet}.
 */
@SuppressWarnings("serial")
@SlingServlet(generateComponent = true, description = "handles all JSON requests for Product Store Demo.", selectors="products", resourceTypes = "fridays-demo/components/structure/categoryPage", methods = "GET", extensions = "html", metatype = true)

public class DemoProductServlet extends SlingSafeMethodsServlet {
    private static final Logger log = LoggerFactory.getLogger(DemoProductServlet.class);

    final String COMMERCE_PRODUCTS_PATH = "/etc/fridays-commerce/products";

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Override
    protected void doGet(final SlingHttpServletRequest request,
                         final SlingHttpServletResponse response) throws ServletException, IOException {

        List<Resource> productList = new ArrayList();

        resolverFactory = null;
        Resource storeResource = null;

        Map<String, Object> param;


        // set the response as a JSON object.
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");


        String storeId = request.getParameter("store");

        final PrintWriter out = response.getWriter();
        final ResourceResolver resourceResolver = request.getResourceResolver();
        final Resource resource = resourceResolver.getResource(COMMERCE_PRODUCTS_PATH);
        final Node node = resource.adaptTo(Node.class);

        final Set<String> propertiesToIgnore = new HashSet<String>() {{
            add("jcr:created");
            add("jcr:createdBy");
            add("jcr:versionHistory");
            add("jcr:predecessors");
            add("jcr:baseVersion");
            add("jcr:uuid");
            add("jcr:primaryType");
        }};

        JsonItemWriter jsonWriter = new JsonItemWriter(propertiesToIgnore);



        try {


            jsonWriter.dump(node, out, 2, true);
            response.setStatus(SlingHttpServletResponse.SC_OK);

        } catch (JSONException e ) {
            log.debug("Error.");
        } catch (RepositoryException e) {
            log.debug("Repository Error.");
        }


/**
 if (action == null) {
            // return all products
        }
        else {
/**

            for (Resource item : tempProductList) {
                String[] values = item.getValueMap().get("stores", String[].class);

                for (String value : values) {
                    if (value.equals(action)) {
                        log.debug("ADDING!!! {}", value);
                        productList.add(item);
                    }
                }
            }

        } **/

     //   response.getWriter().write(createProductResponse(productList).toString());
    }

    private JSONArray createProductResponse(List<Resource> resources) {
        final JSONArray jsonArray = new JSONArray();


        for (Resource resource : resources) {
            JSONObject jsonObject = new JSONObject();

            ValueMap values = resource.adaptTo(ValueMap.class);
            String category = values.get("category", String.class);
            String title = values.get("jcr:title", String.class);
            String price = values.get("price", String.class);
            try {
                jsonObject.put("title", title);
                jsonObject.put("price", price);
                jsonObject.put("category", category);


                log.debug("testing {}", jsonObject.toString());

                jsonArray.put(jsonObject);
            } catch (JSONException e ) {
                log.debug("Error.");
            }

        }



        return jsonArray;
    }
}
